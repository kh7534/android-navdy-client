.class final Lcom/nimbusds/jose/crypto/CompositeKey;
.super Ljava/lang/Object;
.source "CompositeKey.java"


# annotations
.annotation runtime Lnet/jcip/annotations/Immutable;
.end annotation


# instance fields
.field private final encKey:Ljavax/crypto/SecretKey;

.field private final inputKey:Ljavax/crypto/SecretKey;

.field private final macKey:Ljavax/crypto/SecretKey;

.field private final truncatedMacLength:I


# direct methods
.method public constructor <init>(Ljavax/crypto/SecretKey;)V
    .locals 7
    .param p1, "inputKey"    # Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/nimbusds/jose/KeyLengthException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x18

    const/16 v4, 0x10

    const/16 v3, 0x20

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->inputKey:Ljavax/crypto/SecretKey;

    .line 64
    invoke-interface {p1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v0

    .line 66
    .local v0, "secretKeyBytes":[B
    array-length v1, v0

    if-ne v1, v3, :cond_0

    .line 70
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "HMACSHA256"

    invoke-direct {v1, v0, v6, v4, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v1, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->macKey:Ljavax/crypto/SecretKey;

    .line 71
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, v0, v4, v4, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v1, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->encKey:Ljavax/crypto/SecretKey;

    .line 72
    iput v4, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->truncatedMacLength:I

    .line 95
    :goto_0
    return-void

    .line 74
    :cond_0
    array-length v1, v0

    const/16 v2, 0x30

    if-ne v1, v2, :cond_1

    .line 78
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "HMACSHA384"

    invoke-direct {v1, v0, v6, v5, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v1, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->macKey:Ljavax/crypto/SecretKey;

    .line 79
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, v0, v5, v5, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v1, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->encKey:Ljavax/crypto/SecretKey;

    .line 80
    iput v5, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->truncatedMacLength:I

    goto :goto_0

    .line 83
    :cond_1
    array-length v1, v0

    const/16 v2, 0x40

    if-ne v1, v2, :cond_2

    .line 87
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "HMACSHA512"

    invoke-direct {v1, v0, v6, v3, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v1, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->macKey:Ljavax/crypto/SecretKey;

    .line 88
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, v0, v3, v3, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BIILjava/lang/String;)V

    iput-object v1, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->encKey:Ljavax/crypto/SecretKey;

    .line 89
    iput v3, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->truncatedMacLength:I

    goto :goto_0

    .line 93
    :cond_2
    new-instance v1, Lcom/nimbusds/jose/KeyLengthException;

    const-string v2, "Unsupported AES/CBC/PKCS5Padding/HMAC-SHA2 key length, must be 256, 384 or 512 bits"

    invoke-direct {v1, v2}, Lcom/nimbusds/jose/KeyLengthException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getAESKey()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->encKey:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method public getInputKey()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->inputKey:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method public getMACKey()Ljavax/crypto/SecretKey;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->macKey:Ljavax/crypto/SecretKey;

    return-object v0
.end method

.method public getTruncatedMACByteLength()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/nimbusds/jose/crypto/CompositeKey;->truncatedMacLength:I

    return v0
.end method
