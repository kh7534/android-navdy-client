.class public Lcom/navdy/client/app/ui/settings/SettingsUtils;
.super Ljava/lang/Object;
.source "SettingsUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/settings/SettingsUtils$SettingsLogger;
    }
.end annotation


# static fields
.field private static final FLAT_SUPPORTED:Ljava/lang/String; = "flatsupported"

.field private static final MAKE:Ljava/lang/String; = "make"

.field private static final MEDIUM_SUPPORTED:Ljava/lang/String; = "mediumsupported"

.field private static final MODEL:Ljava/lang/String; = "model"

.field private static final RECOMMENDED:Ljava/lang/String; = "recommended"

.field private static final TALL_SUPPORTED:Ljava/lang/String; = "tallsupported"

.field private static final YEAR:Ljava/lang/String; = "year"

.field private static lastMake:Ljava/lang/String;

.field private static lastModel:Ljava/lang/String;

.field private static lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

.field private static lastYear:Ljava/lang/String;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static updateSettingsUponNextConnection:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/settings/SettingsUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    .line 87
    const-string v0, ""

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMake:Ljava/lang/String;

    .line 88
    const-string v0, ""

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastYear:Ljava/lang/String;

    .line 89
    const-string v0, ""

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastModel:Ljava/lang/String;

    .line 90
    new-instance v0, Lcom/navdy/client/app/framework/models/MountInfo;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/MountInfo;-><init>()V

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    .line 112
    const/4 v0, 0x0

    sput-boolean v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->updateSettingsUponNextConnection:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 74
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendHudSettingsBasedOnSharedPrefValue()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Z)V
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 74
    invoke-static {p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUpdateSettingsUponNextConnection(Z)V

    return-void
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private static buildDisplaySpeakerPreferences(JZIZZ)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
    .locals 6
    .param p0, "serialNumber"    # J
    .param p2, "masterSound"    # Z
    .param p3, "volumeLevel"    # I
    .param p4, "feedbackSound"    # Z
    .param p5, "notificationSound"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 439
    new-instance v0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method private static buildInputPreferences(JZ)Lcom/navdy/service/library/events/preferences/InputPreferences;
    .locals 4
    .param p0, "serialNumber"    # J
    .param p2, "useGesture"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 391
    sget-object v0, Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;->DIAL_SENSITIVITY_STANDARD:Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;

    .line 392
    .local v0, "dialSensitivity":Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;
    new-instance v1, Lcom/navdy/service/library/events/preferences/InputPreferences;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/navdy/service/library/events/preferences/InputPreferences;-><init>(Ljava/lang/Long;Ljava/lang/Boolean;Lcom/navdy/service/library/events/preferences/InputPreferences$DialSensitivity;)V

    return-object v1
.end method

.method static buildNavigationPreferences(JZZZZZZZZZZZ)Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    .locals 2
    .param p0, "serialNumber"    # J
    .param p2, "calculateShortestRouteIsOn"    # Z
    .param p3, "autoRecalcIsOn"    # Z
    .param p4, "highwaysIsOn"    # Z
    .param p5, "tollRoadsIsOn"    # Z
    .param p6, "ferriesIsOn"    # Z
    .param p7, "tunnelsIsOn"    # Z
    .param p8, "unpavedRoadsIsOn"    # Z
    .param p9, "autoTrainsIsOn"    # Z
    .param p10, "spokenSpeedLimitWarningsAreOn"    # Z
    .param p11, "spokenCameraWarningsAreOn"    # Z
    .param p12, "spokenTurnByTurnIsOn"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 535
    new-instance v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;-><init>()V

    .line 536
    .local v0, "navPrefBldr":Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->serial_number:Ljava/lang/Long;

    .line 537
    if-eqz p2, :cond_0

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_SHORTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    :goto_0
    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->routingType:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    .line 538
    if-eqz p3, :cond_1

    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_AUTOMATIC:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    :goto_1
    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->rerouteForTraffic:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    .line 539
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowHighways:Ljava/lang/Boolean;

    .line 540
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTollRoads:Ljava/lang/Boolean;

    .line 541
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowFerries:Ljava/lang/Boolean;

    .line 542
    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowTunnels:Ljava/lang/Boolean;

    .line 543
    invoke-static {p8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowUnpavedRoads:Ljava/lang/Boolean;

    .line 544
    invoke-static {p9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->allowAutoTrains:Ljava/lang/Boolean;

    .line 545
    invoke-static {p10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenSpeedLimitWarnings:Ljava/lang/Boolean;

    .line 546
    invoke-static {p11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenCameraWarnings:Ljava/lang/Boolean;

    .line 547
    invoke-static {p12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->spokenTurnByTurn:Ljava/lang/Boolean;

    .line 549
    invoke-virtual {v0}, Lcom/navdy/service/library/events/preferences/NavigationPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v1

    return-object v1

    .line 537
    :cond_0
    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;->ROUTING_FASTEST:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RoutingType;

    goto :goto_0

    .line 538
    :cond_1
    sget-object v1, Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;->REROUTE_CONFIRM:Lcom/navdy/service/library/events/preferences/NavigationPreferences$RerouteForTraffic;

    goto :goto_1
.end method

.method public static carIsBlacklistedForObdData()Z
    .locals 20

    .prologue
    .line 791
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 792
    .local v2, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v17, "vehicle-make"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 793
    .local v6, "makeFromSharedPref":Ljava/lang/String;
    const-string v17, "vehicle-year"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 794
    .local v15, "yearFromSharedPref":Ljava/lang/String;
    const-string v17, "vehicle-model"

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 795
    .local v9, "modelFromSharedPref":Ljava/lang/String;
    const/4 v10, 0x0

    .line 796
    .local v10, "modelMatches":Z
    const/4 v7, 0x0

    .line 797
    .local v7, "makeMatches":Z
    const/16 v16, 0x0

    .line 799
    .local v16, "yearMatches":Z
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    invoke-static {v15}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    invoke-static {v9}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 800
    :cond_0
    sget-object v17, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v18, "No car model was found"

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 801
    const/16 v17, 0x0

    .line 864
    :goto_0
    return v17

    .line 803
    :cond_1
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 804
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    .line 805
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 808
    const/4 v12, 0x0

    .line 810
    .local v12, "reader":Landroid/util/JsonReader;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f070002

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 811
    .local v4, "is":Ljava/io/InputStream;
    new-instance v13, Landroid/util/JsonReader;

    new-instance v17, Ljava/io/InputStreamReader;

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 813
    .end local v12    # "reader":Landroid/util/JsonReader;
    .local v13, "reader":Landroid/util/JsonReader;
    :cond_2
    :try_start_1
    invoke-virtual {v13}, Landroid/util/JsonReader;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 815
    invoke-virtual {v13}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v17

    sget-object v18, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 816
    invoke-virtual {v13}, Landroid/util/JsonReader;->beginArray()V

    .line 818
    :cond_3
    invoke-virtual {v13}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v17

    sget-object v18, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 820
    invoke-virtual {v13}, Landroid/util/JsonReader;->beginObject()V

    .line 823
    :cond_4
    :goto_1
    invoke-virtual {v13}, Landroid/util/JsonReader;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 824
    invoke-virtual {v13}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v11

    .line 825
    .local v11, "name":Ljava/lang/String;
    const/16 v17, -0x1

    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    move-result v18

    sparse-switch v18, :sswitch_data_0

    :cond_5
    :goto_2
    packed-switch v17, :pswitch_data_0

    .line 845
    invoke-virtual {v13}, Landroid/util/JsonReader;->skipValue()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 857
    .end local v11    # "name":Ljava/lang/String;
    :catch_0
    move-exception v3

    move-object v12, v13

    .line 858
    .end local v4    # "is":Ljava/io/InputStream;
    .end local v13    # "reader":Landroid/util/JsonReader;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v12    # "reader":Landroid/util/JsonReader;
    :goto_3
    :try_start_2
    sget-object v17, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Exception found: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 860
    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 863
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_4
    sget-object v17, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "car is not blacklisted: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 864
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 825
    .end local v12    # "reader":Landroid/util/JsonReader;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v11    # "name":Ljava/lang/String;
    .restart local v13    # "reader":Landroid/util/JsonReader;
    :sswitch_0
    :try_start_3
    const-string v18, "model"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    const/16 v17, 0x0

    goto :goto_2

    :sswitch_1
    const-string v18, "make"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    const/16 v17, 0x1

    goto :goto_2

    :sswitch_2
    const-string v18, "year"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    const/16 v17, 0x2

    goto/16 :goto_2

    .line 827
    :pswitch_0
    invoke-virtual {v13}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v8

    .line 828
    .local v8, "model":Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 829
    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    goto/16 :goto_1

    .line 833
    .end local v8    # "model":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {v13}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v5

    .line 834
    .local v5, "make":Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 835
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    goto/16 :goto_1

    .line 839
    .end local v5    # "make":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {v13}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v14

    .line 840
    .local v14, "year":Ljava/lang/String;
    invoke-static {v14}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_4

    .line 841
    invoke-virtual {v14, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    goto/16 :goto_1

    .line 850
    .end local v11    # "name":Ljava/lang/String;
    .end local v14    # "year":Ljava/lang/String;
    :cond_6
    invoke-virtual {v13}, Landroid/util/JsonReader;->endObject()V

    .line 851
    if-eqz v7, :cond_2

    if-eqz v16, :cond_2

    if-eqz v10, :cond_2

    .line 852
    sget-object v17, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "car is blacklisted: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 853
    const/16 v17, 0x1

    .line 860
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 856
    :cond_7
    :try_start_4
    invoke-virtual {v13}, Landroid/util/JsonReader;->endArray()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 860
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v12, v13

    .line 861
    .end local v13    # "reader":Landroid/util/JsonReader;
    .restart local v12    # "reader":Landroid/util/JsonReader;
    goto/16 :goto_4

    .line 860
    .end local v4    # "is":Ljava/io/InputStream;
    :catchall_0
    move-exception v17

    :goto_5
    invoke-static {v12}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v17

    .end local v12    # "reader":Landroid/util/JsonReader;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v13    # "reader":Landroid/util/JsonReader;
    :catchall_1
    move-exception v17

    move-object v12, v13

    .end local v13    # "reader":Landroid/util/JsonReader;
    .restart local v12    # "reader":Landroid/util/JsonReader;
    goto :goto_5

    .line 857
    .end local v4    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v3

    goto/16 :goto_3

    .line 825
    nop

    :sswitch_data_0
    .sparse-switch
        0x3305ee -> :sswitch_1
        0x38883d -> :sswitch_2
        0x633fb29 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static checkCapabilities(Lcom/navdy/service/library/events/Capabilities;)V
    .locals 8
    .param p0, "capabilities"    # Lcom/navdy/service/library/events/Capabilities;

    .prologue
    .line 1099
    if-nez p0, :cond_0

    .line 1100
    sget-object v1, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "device info has no capabilities."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1113
    :goto_0
    return-void

    .line 1104
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1105
    .local v0, "sharedPrefs":Landroid/content/SharedPreferences;
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->compactUi:Ljava/lang/Boolean;

    .line 1106
    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    .line 1107
    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/navdy/service/library/events/Capabilities;->localMusicBrowser:Ljava/lang/Boolean;

    .line 1108
    invoke-virtual {v3, v4}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v3

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/navdy/service/library/events/Capabilities;->searchResultList:Ljava/lang/Boolean;

    .line 1109
    invoke-virtual {v4, v5}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/navdy/service/library/events/Capabilities;->cannedResponseToSms:Ljava/lang/Boolean;

    .line 1110
    invoke-virtual {v5, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/navdy/service/library/events/Capabilities;->customDialLongPress:Ljava/lang/Boolean;

    .line 1111
    invoke-virtual {v6, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 1105
    invoke-static/range {v0 .. v6}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->processCapabilities(Landroid/content/SharedPreferences;ZZZZZZ)V

    goto :goto_0
.end method

.method public static checkLegacyCapabilityList(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/LegacyCapability;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, "legacyCapabilityList":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/LegacyCapability;>;"
    const/4 v4, 0x0

    .line 1068
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1069
    :cond_0
    sget-object v4, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "capability list is empty"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1096
    :goto_0
    return-void

    .line 1073
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1075
    .local v0, "sharedPrefs":Landroid/content/SharedPreferences;
    const/4 v1, 0x0

    .line 1076
    .local v1, "compactCapable":Z
    const/4 v2, 0x0

    .line 1077
    .local v2, "voiceSearchCapable":Z
    const/4 v3, 0x0

    .line 1078
    .local v3, "musicCapable":Z
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/navdy/service/library/events/LegacyCapability;

    .line 1079
    .local v7, "legacyCapability":Lcom/navdy/service/library/events/LegacyCapability;
    sget-object v6, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_COMPACT_UI:Lcom/navdy/service/library/events/LegacyCapability;

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/events/LegacyCapability;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1080
    const/4 v1, 0x1

    goto :goto_1

    .line 1081
    :cond_3
    sget-object v6, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_VOICE_SEARCH:Lcom/navdy/service/library/events/LegacyCapability;

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/events/LegacyCapability;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1082
    const/4 v2, 0x1

    goto :goto_1

    .line 1083
    :cond_4
    sget-object v6, Lcom/navdy/service/library/events/LegacyCapability;->CAPABILITY_LOCAL_MUSIC_BROWSER:Lcom/navdy/service/library/events/LegacyCapability;

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/events/LegacyCapability;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1084
    const/4 v3, 0x1

    goto :goto_1

    .end local v7    # "legacyCapability":Lcom/navdy/service/library/events/LegacyCapability;
    :cond_5
    move v5, v4

    move v6, v4

    .line 1088
    invoke-static/range {v0 .. v6}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->processCapabilities(Landroid/content/SharedPreferences;ZZZZZZ)V

    goto :goto_0
.end method

.method public static checkObdSettingIfBuildVersionChanged()V
    .locals 7

    .prologue
    .line 1012
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1013
    .local v3, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v4, "last_build_version"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1015
    .local v1, "lastBuildVersion":Ljava/lang/String;
    const-string v4, "1.0"

    invoke-static {v1, v4}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1038
    :goto_0
    return-void

    .line 1018
    :cond_0
    sget-object v4, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "build version has changed. Checking obd setting"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1021
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isUsingDefaultObdScanSetting()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1022
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setDefaultObdSettingDependingOnBlacklistAsync()V

    goto :goto_0

    .line 1026
    :cond_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1027
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "user_overrided_blacklist"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 1028
    .local v2, "overrodeBlacklist":Z
    sget-object v4, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "overrodeBlacklist: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1032
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->carIsBlacklistedForObdData()Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v2, :cond_2

    .line 1033
    const-string v4, "hud_obd_on_enabled"

    sget-object v5, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v5}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1036
    :cond_2
    const-string v4, "last_build_version"

    const-string v5, "1.0"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1037
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public static getCarYearMakeModelString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 781
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 782
    .local v4, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v5, "vehicle-year"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 783
    .local v3, "carYear":Ljava/lang/String;
    const-string v5, "vehicle-make"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 784
    .local v1, "carMake":Ljava/lang/String;
    const-string v5, "vehicle-model"

    const-string v6, ""

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 785
    .local v2, "carModel":Ljava/lang/String;
    const-string v5, "%s %s %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v7, 0x1

    aput-object v1, v6, v7

    const/4 v7, 0x2

    aput-object v2, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 786
    .local v0, "carInfo":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getCustomerPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 603
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "tracker"

    const/4 v2, 0x0

    .line 604
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getDialerPackage(Landroid/content/pm/PackageManager;)Ljava/lang/String;
    .locals 4
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 623
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.CALL"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 624
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "tel:123456789"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 625
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 626
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v2, :cond_0

    .line 627
    iget-object v0, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 628
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v0, :cond_0

    .line 629
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 633
    .end local v0    # "activityInfo":Landroid/content/pm/ActivityInfo;
    :goto_0
    return-object v3

    :cond_0
    const-string v3, "com.android.dialer"

    goto :goto_0
.end method

.method public static getFeatureMode()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
    .locals 4

    .prologue
    .line 314
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "feature-mode"

    sget-object v3, Lcom/navdy/client/app/ui/settings/SettingsConstants;->FEATURE_MODE_DEFAULT:Ljava/lang/String;

    .line 315
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, "modeString":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    move-result-object v1

    return-object v1
.end method

.method public static getMountInfo()Lcom/navdy/client/app/framework/models/MountInfo;
    .locals 21
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 871
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 872
    .local v2, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v18, "vehicle-year"

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 873
    .local v16, "yearFromSharedPref":Ljava/lang/String;
    const-string v18, "vehicle-make"

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 874
    .local v6, "makeFromSharedPref":Ljava/lang/String;
    const-string v18, "vehicle-model"

    const-string v19, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 876
    .local v9, "modelFromSharedPref":Ljava/lang/String;
    sget-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastYear:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    sget-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMake:Ljava/lang/String;

    .line 877
    move-object/from16 v0, v18

    invoke-static {v0, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    sget-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastModel:Ljava/lang/String;

    .line 878
    move-object/from16 v0, v18

    invoke-static {v0, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 879
    sget-object v11, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    .line 966
    :goto_0
    return-object v11

    .line 882
    :cond_0
    const/4 v10, 0x0

    .line 883
    .local v10, "modelMatches":Z
    const/4 v7, 0x0

    .line 884
    .local v7, "makeMatches":Z
    const/16 v17, 0x0

    .line 886
    .local v17, "yearMatches":Z
    invoke-static {v6}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    invoke-static/range {v16 .. v16}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_1

    invoke-static {v9}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 887
    :cond_1
    sget-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v19, "No car model was found"

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 888
    new-instance v18, Lcom/navdy/client/app/framework/models/MountInfo;

    invoke-direct/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/MountInfo;-><init>()V

    sput-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    .line 889
    sget-object v11, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    goto :goto_0

    .line 891
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 892
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    .line 893
    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 896
    const/4 v13, 0x0

    .line 898
    .local v13, "reader":Landroid/util/JsonReader;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f070008

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 899
    .local v4, "is":Ljava/io/InputStream;
    new-instance v14, Landroid/util/JsonReader;

    new-instance v18, Ljava/io/InputStreamReader;

    move-object/from16 v0, v18

    invoke-direct {v0, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v18

    invoke-direct {v14, v0}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 901
    .end local v13    # "reader":Landroid/util/JsonReader;
    .local v14, "reader":Landroid/util/JsonReader;
    :cond_3
    :try_start_1
    invoke-virtual {v14}, Landroid/util/JsonReader;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_9

    .line 903
    invoke-virtual {v14}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v18

    sget-object v19, Landroid/util/JsonToken;->BEGIN_ARRAY:Landroid/util/JsonToken;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 904
    invoke-virtual {v14}, Landroid/util/JsonReader;->beginArray()V

    .line 906
    :cond_4
    invoke-virtual {v14}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v18

    sget-object v19, Landroid/util/JsonToken;->BEGIN_OBJECT:Landroid/util/JsonToken;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 908
    invoke-virtual {v14}, Landroid/util/JsonReader;->beginObject()V

    .line 911
    :cond_5
    new-instance v11, Lcom/navdy/client/app/framework/models/MountInfo;

    invoke-direct {v11}, Lcom/navdy/client/app/framework/models/MountInfo;-><init>()V

    .line 912
    .local v11, "mountInfo":Lcom/navdy/client/app/framework/models/MountInfo;
    :cond_6
    :goto_1
    invoke-virtual {v14}, Landroid/util/JsonReader;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_8

    .line 913
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v12

    .line 914
    .local v12, "name":Ljava/lang/String;
    const/16 v18, -0x1

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v19

    sparse-switch v19, :sswitch_data_0

    :cond_7
    :goto_2
    packed-switch v18, :pswitch_data_0

    .line 946
    invoke-virtual {v14}, Landroid/util/JsonReader;->skipValue()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 958
    .end local v11    # "mountInfo":Lcom/navdy/client/app/framework/models/MountInfo;
    .end local v12    # "name":Ljava/lang/String;
    :catch_0
    move-exception v3

    move-object v13, v14

    .line 959
    .end local v4    # "is":Ljava/io/InputStream;
    .end local v14    # "reader":Landroid/util/JsonReader;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v13    # "reader":Landroid/util/JsonReader;
    :goto_3
    :try_start_2
    sget-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Exception found: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 961
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 964
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_4
    sget-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "car is not blacklisted: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 965
    new-instance v18, Lcom/navdy/client/app/framework/models/MountInfo;

    invoke-direct/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/MountInfo;-><init>()V

    sput-object v18, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    .line 966
    sget-object v11, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    goto/16 :goto_0

    .line 914
    .end local v13    # "reader":Landroid/util/JsonReader;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v11    # "mountInfo":Lcom/navdy/client/app/framework/models/MountInfo;
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v14    # "reader":Landroid/util/JsonReader;
    :sswitch_0
    :try_start_3
    const-string v19, "model"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v18, 0x0

    goto :goto_2

    :sswitch_1
    const-string v19, "make"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v18, 0x1

    goto :goto_2

    :sswitch_2
    const-string v19, "year"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v18, 0x2

    goto/16 :goto_2

    :sswitch_3
    const-string v19, "flatsupported"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v18, 0x3

    goto/16 :goto_2

    :sswitch_4
    const-string v19, "mediumsupported"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v18, 0x4

    goto/16 :goto_2

    :sswitch_5
    const-string v19, "tallsupported"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v18, 0x5

    goto/16 :goto_2

    :sswitch_6
    const-string v19, "recommended"

    move-object/from16 v0, v19

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    const/16 v18, 0x6

    goto/16 :goto_2

    .line 916
    :pswitch_0
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v8

    .line 917
    .local v8, "model":Ljava/lang/String;
    invoke-static {v8}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 918
    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    goto/16 :goto_1

    .line 922
    .end local v8    # "model":Ljava/lang/String;
    :pswitch_1
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v5

    .line 923
    .local v5, "make":Ljava/lang/String;
    invoke-static {v5}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 924
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    goto/16 :goto_1

    .line 928
    .end local v5    # "make":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v15

    .line 929
    .local v15, "year":Ljava/lang/String;
    invoke-static {v15}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 930
    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    goto/16 :goto_1

    .line 934
    .end local v15    # "year":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v11, Lcom/navdy/client/app/framework/models/MountInfo;->shortSupported:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 961
    .end local v11    # "mountInfo":Lcom/navdy/client/app/framework/models/MountInfo;
    .end local v12    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v18

    move-object v13, v14

    .end local v4    # "is":Ljava/io/InputStream;
    .end local v14    # "reader":Landroid/util/JsonReader;
    .restart local v13    # "reader":Landroid/util/JsonReader;
    :goto_5
    invoke-static {v13}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v18

    .line 937
    .end local v13    # "reader":Landroid/util/JsonReader;
    .restart local v4    # "is":Ljava/io/InputStream;
    .restart local v11    # "mountInfo":Lcom/navdy/client/app/framework/models/MountInfo;
    .restart local v12    # "name":Ljava/lang/String;
    .restart local v14    # "reader":Landroid/util/JsonReader;
    :pswitch_4
    :try_start_4
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v11, Lcom/navdy/client/app/framework/models/MountInfo;->mediumSupported:Z

    goto/16 :goto_1

    .line 940
    :pswitch_5
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v18

    move/from16 v0, v18

    iput-boolean v0, v11, Lcom/navdy/client/app/framework/models/MountInfo;->tallSupported:Z

    goto/16 :goto_1

    .line 943
    :pswitch_6
    invoke-virtual {v14}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getMountTypeForName(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v11, Lcom/navdy/client/app/framework/models/MountInfo;->recommendedMount:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    goto/16 :goto_1

    .line 951
    .end local v12    # "name":Ljava/lang/String;
    :cond_8
    invoke-virtual {v14}, Landroid/util/JsonReader;->endObject()V

    .line 952
    if-eqz v7, :cond_3

    if-eqz v17, :cond_3

    if-eqz v10, :cond_3

    .line 953
    sput-object v11, Lcom/navdy/client/app/ui/settings/SettingsUtils;->lastMountInfo:Lcom/navdy/client/app/framework/models/MountInfo;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 961
    invoke-static {v14}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 957
    .end local v11    # "mountInfo":Lcom/navdy/client/app/framework/models/MountInfo;
    :cond_9
    :try_start_5
    invoke-virtual {v14}, Landroid/util/JsonReader;->endArray()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 961
    invoke-static {v14}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v13, v14

    .line 962
    .end local v14    # "reader":Landroid/util/JsonReader;
    .restart local v13    # "reader":Landroid/util/JsonReader;
    goto/16 :goto_4

    .line 961
    .end local v4    # "is":Ljava/io/InputStream;
    :catchall_1
    move-exception v18

    goto :goto_5

    .line 958
    :catch_1
    move-exception v3

    goto/16 :goto_3

    .line 914
    nop

    :sswitch_data_0
    .sparse-switch
        -0x3f6b2727 -> :sswitch_4
        -0x24086d5f -> :sswitch_5
        0x3305ee -> :sswitch_1
        0x38883d -> :sswitch_2
        0x633fb29 -> :sswitch_0
        0x55b4de5b -> :sswitch_6
        0x61e72d55 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static getObdBlacklistLastModifiedDate()J
    .locals 11

    .prologue
    .line 1041
    const-string v5, "0"

    .line 1042
    .local v5, "modifiedDate":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1043
    .local v3, "inputStream":Ljava/io/InputStream;
    const/4 v0, 0x0

    .line 1045
    .local v0, "bufferedReader":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f070007

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 1046
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-direct {v4, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 1047
    .local v4, "inputStreamReader":Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1048
    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .local v1, "bufferedReader":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1051
    .local v7, "stringBuilder":Ljava/lang/StringBuilder;
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "newLine":Ljava/lang/String;
    if-eqz v6, :cond_0

    .line 1052
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 1056
    .end local v6    # "newLine":Ljava/lang/String;
    .end local v7    # "stringBuilder":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 1057
    .end local v1    # "bufferedReader":Ljava/io/BufferedReader;
    .end local v4    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v8, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception found: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1059
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1060
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1063
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_2
    sget-object v8, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "the last modified date: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 1064
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    return-wide v8

    .line 1055
    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v4    # "inputStreamReader":Ljava/io/InputStreamReader;
    .restart local v6    # "newLine":Ljava/lang/String;
    .restart local v7    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v5

    .line 1059
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1060
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    move-object v0, v1

    .line 1061
    .end local v1    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferedReader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1059
    .end local v4    # "inputStreamReader":Ljava/io/InputStreamReader;
    .end local v6    # "newLine":Ljava/lang/String;
    .end local v7    # "stringBuilder":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v8

    :goto_3
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 1060
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v8

    .line 1059
    .end local v0    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v4    # "inputStreamReader":Ljava/io/InputStreamReader;
    :catchall_1
    move-exception v8

    move-object v0, v1

    .end local v1    # "bufferedReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferedReader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 1056
    .end local v4    # "inputStreamReader":Ljava/io/InputStreamReader;
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public static getPermissionsSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 609
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "permissions"

    const/4 v2, 0x0

    .line 610
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getSharedPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 597
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "settings"

    const/4 v2, 0x0

    .line 598
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getSmsPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 637
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/provider/Telephony$Sms;->getDefaultSmsPackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    .locals 4
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 254
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "unit-system"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "unitSystemPreference":Ljava/lang/String;
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 264
    :goto_0
    return-object v1

    .line 261
    :cond_0
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_METRIC:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    goto :goto_0

    .line 264
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized getUpdateSettingsUponNextConnection()Z
    .locals 2

    .prologue
    .line 119
    const-class v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/navdy/client/app/ui/settings/SettingsUtils;->updateSettingsUponNextConnection:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static incrementDriverProfileSerial()V
    .locals 8

    .prologue
    .line 155
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 156
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v1, "profile_serial_number"

    sget-object v4, Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;->SERIAL_NUM_DEFAULT:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 157
    .local v2, "serial":J
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "profile_serial_number"

    const-wide/16 v6, 0x1

    add-long/2addr v6, v2

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 158
    return-void
.end method

.method public static incrementSerialNumber(Ljava/lang/String;)J
    .locals 6
    .param p0, "serialNumKey"    # Ljava/lang/String;

    .prologue
    .line 614
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 615
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-wide/16 v4, 0x0

    invoke-interface {v2, p0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 616
    .local v0, "serialNumber":J
    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    .line 617
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 618
    return-wide v0
.end method

.method public static isBetaUser()Z
    .locals 2

    .prologue
    .line 321
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getFeatureMode()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    move-result-object v0

    sget-object v1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_BETA:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLimitingCellularData()Z
    .locals 3

    .prologue
    .line 282
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "limit_cellular_data"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static isUsingDefaultObdScanSetting()Z
    .locals 5

    .prologue
    .line 971
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 972
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v3, "hud_obd_on_enabled"

    sget-object v4, Lcom/navdy/client/app/ui/settings/SettingsConstants;->HUD_OBD_ON_DEFAULT:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 973
    .local v1, "obdScanSettingString":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-result-object v0

    .line 975
    .local v0, "obdScanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    sget-object v3, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_OFF:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 976
    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static obdScanIsOn()Z
    .locals 5

    .prologue
    .line 980
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 981
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v3, "hud_obd_on_enabled"

    sget-object v4, Lcom/navdy/client/app/ui/settings/SettingsConstants;->HUD_OBD_ON_DEFAULT:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 982
    .local v1, "obdScanSettingString":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-result-object v0

    .line 983
    .local v0, "obdScanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    sget-object v3, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v0, v3}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->equals(Ljava/lang/Object;)Z

    move-result v3

    return v3
.end method

.method public static onSharedPrefsDowngrade(II)V
    .locals 0
    .param p0, "oldVersion"    # I
    .param p1, "newVersion"    # I

    .prologue
    .line 93
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setSharedPrefsVersionToCurrent()V

    .line 94
    return-void
.end method

.method public static onSharedPrefsUpgrade(II)V
    .locals 0
    .param p0, "oldVersion"    # I
    .param p1, "newVersion"    # I

    .prologue
    .line 97
    if-gtz p0, :cond_0

    .line 98
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->convertAllClendarNameSharedPrefsToCalendarIds()V

    .line 100
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setSharedPrefsVersionToCurrent()V

    .line 101
    return-void
.end method

.method private static processCapabilities(Landroid/content/SharedPreferences;ZZZZZZ)V
    .locals 4
    .param p0, "sharedPrefs"    # Landroid/content/SharedPreferences;
    .param p1, "compactCapable"    # Z
    .param p2, "voiceSearchCapable"    # Z
    .param p3, "musicCapable"    # Z
    .param p4, "searchResultListCapable"    # Z
    .param p5, "cannedResponseToSms"    # Z
    .param p6, "customDialLongPress"    # Z

    .prologue
    .line 1122
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hud_compact_capable"

    .line 1123
    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hud_voice_search_capable"

    .line 1124
    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hud_music_capable"

    .line 1125
    invoke-interface {v1, v2, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hud_search_result_list_capable"

    .line 1126
    invoke-interface {v1, v2, p4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hud_canned_response_capable"

    .line 1127
    invoke-interface {v1, v2, p5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hud_dial_long_press_capable"

    .line 1128
    invoke-interface {v1, v2, p6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1129
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1132
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1133
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1134
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "messaging_has_seen_capable_hud"

    const/4 v3, 0x1

    .line 1135
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1136
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1138
    .end local v0    # "sharedPreferences":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method public static sendDialSettingsToTheHudBasedOnSharedPrefValue()Z
    .locals 8

    .prologue
    .line 380
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 381
    .local v4, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v5, "hud_serial_number"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 382
    .local v2, "serialNumber":J
    const-string v5, "hud_gesture"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 384
    .local v0, "gestureIsEnabled":Z
    invoke-static {v2, v3, v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->buildInputPreferences(JZ)Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v1

    .line 386
    .local v1, "inputPrefs":Lcom/navdy/service/library/events/preferences/InputPreferences;
    invoke-static {v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendInputSettingsToTheHud(Lcom/navdy/service/library/events/preferences/InputPreferences;)Z

    move-result v5

    return v5
.end method

.method private static sendDisplaySpeakerSettingsToTheHud(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)Z
    .locals 6
    .param p0, "speakerPrefs"    # Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 443
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 444
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v4

    .line 445
    .local v0, "deviceIsConnected":Z
    :goto_0
    if-nez v0, :cond_1

    .line 446
    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUpdateSettingsUponNextConnection(Z)V

    .line 454
    :goto_1
    return v3

    .end local v0    # "deviceIsConnected":Z
    :cond_0
    move v0, v3

    .line 444
    goto :goto_0

    .line 450
    .restart local v0    # "deviceIsConnected":Z
    :cond_1
    new-instance v2, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;-><init>()V

    .line 451
    .local v2, "updateBldr":Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;
    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 452
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;->serial_number:Ljava/lang/Long;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 453
    iput-object p0, v2, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    .line 454
    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v3

    goto :goto_1
.end method

.method private static sendDriverProfileSettingsToTheHud(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)Z
    .locals 6
    .param p0, "driverProfilePrefs"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 325
    if-nez p0, :cond_0

    .line 340
    :goto_0
    return v3

    .line 329
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 330
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v4

    .line 331
    .local v0, "deviceIsConnected":Z
    :goto_1
    if-nez v0, :cond_2

    .line 332
    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUpdateSettingsUponNextConnection(Z)V

    goto :goto_0

    .end local v0    # "deviceIsConnected":Z
    :cond_1
    move v0, v3

    .line 330
    goto :goto_1

    .line 336
    .restart local v0    # "deviceIsConnected":Z
    :cond_2
    new-instance v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate$Builder;-><init>()V

    .line 337
    .local v2, "updateBldr":Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate$Builder;
    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 338
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 339
    iput-object p0, v2, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .line 340
    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferencesUpdate;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v3

    goto :goto_0
.end method

.method public static sendDriverProfileToTheHudBasedOnSharedPrefValue()Z
    .locals 40
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 163
    invoke-static {}, Lcom/navdy/client/app/framework/util/SystemUtils;->ensureNotOnMainThread()V

    .line 165
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v11

    .line 166
    .local v11, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v36, "profile_serial_number"

    sget-object v37, Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;->SERIAL_NUM_DEFAULT:Ljava/lang/Long;

    invoke-virtual/range {v37 .. v37}, Ljava/lang/Long;->longValue()J

    move-result-wide v38

    move-object/from16 v0, v36

    move-wide/from16 v1, v38

    invoke-interface {v11, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v32

    .line 167
    .local v32, "serial":J
    sget-object v36, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "serial: "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-wide/from16 v1, v32

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 169
    const-string v36, "fname"

    const-string v37, ""

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 170
    .local v19, "fullName":Ljava/lang/String;
    const-string v36, "email"

    const-string v37, ""

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 171
    .local v16, "email":Ljava/lang/String;
    const-string v36, "vehicle-year"

    const-string v37, ""

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 172
    .local v10, "carYear":Ljava/lang/String;
    const-string v36, "vehicle-make"

    const-string v37, ""

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 173
    .local v8, "carMake":Ljava/lang/String;
    const-string v36, "vehicle-model"

    const-string v37, ""

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 175
    .local v9, "carModel":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v31

    .line 176
    .local v31, "settingsPrefs":Landroid/content/SharedPreferences;
    const-string v36, "hud_auto_on_enabled"

    const/16 v37, 0x1

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 177
    .local v6, "autoOnEnabled":Z
    const-string v36, "hud_ui_scaling"

    const/16 v37, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v34

    .line 179
    .local v34, "uiScalingEnabled":Z
    const-string v36, "feature-mode"

    sget-object v37, Lcom/navdy/client/app/ui/settings/SettingsConstants;->FEATURE_MODE_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 180
    .local v18, "featureModeString":Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    move-result-object v17

    .line 181
    .local v17, "featureMode":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
    sget-object v36, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "featureMode: "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 183
    if-eqz v34, :cond_1

    sget-object v14, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_COMPACT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 187
    .local v14, "displayFormat":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    :goto_0
    sget-object v29, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 189
    .local v29, "phoneName":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->getUserProfilePhoto()Landroid/graphics/Bitmap;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;->buildPhotoChecksum(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v30

    .line 191
    .local v30, "photoChecksum":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v23

    .line 192
    .local v23, "locale":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getInstance()Lcom/navdy/client/app/framework/i18n/I18nManager;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getUnitSystem()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-result-object v35

    .line 194
    .local v35, "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    const-string v36, "hud_obd_on_enabled"

    sget-object v37, Lcom/navdy/client/app/ui/settings/SettingsConstants;->HUD_OBD_ON_DEFAULT:Ljava/lang/String;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 195
    .local v25, "obdEnabled":Ljava/lang/String;
    invoke-static/range {v25 .. v25}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-result-object v28

    .line 197
    .local v28, "obdScanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getObdBlacklistLastModifiedDate()J

    move-result-wide v26

    .line 199
    .local v26, "obdBlacklistLastModified":J
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v22

    .line 201
    .local v22, "limitCellularData":Z
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v4, "additionalLocales":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v5

    .line 204
    .local v5, "appContext":Landroid/content/Context;
    sget v36, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v37, 0x18

    move/from16 v0, v36

    move/from16 v1, v37

    if-lt v0, v1, :cond_3

    .line 205
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v24

    .line 206
    .local v24, "locales":Landroid/os/LocaleList;
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_1
    invoke-virtual/range {v24 .. v24}, Landroid/os/LocaleList;->size()I

    move-result v36

    move/from16 v0, v20

    move/from16 v1, v36

    if-ge v0, v1, :cond_3

    .line 207
    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v7

    .line 208
    .local v7, "availableLocale":Ljava/util/Locale;
    if-nez v7, :cond_2

    .line 206
    :cond_0
    :goto_2
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 183
    .end local v4    # "additionalLocales":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "appContext":Landroid/content/Context;
    .end local v7    # "availableLocale":Ljava/util/Locale;
    .end local v14    # "displayFormat":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .end local v20    # "i":I
    .end local v22    # "limitCellularData":Z
    .end local v23    # "locale":Ljava/lang/String;
    .end local v24    # "locales":Landroid/os/LocaleList;
    .end local v25    # "obdEnabled":Ljava/lang/String;
    .end local v26    # "obdBlacklistLastModified":J
    .end local v28    # "obdScanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .end local v29    # "phoneName":Ljava/lang/String;
    .end local v30    # "photoChecksum":Ljava/lang/String;
    .end local v35    # "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :cond_1
    sget-object v14, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_NORMAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    goto :goto_0

    .line 211
    .restart local v4    # "additionalLocales":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "appContext":Landroid/content/Context;
    .restart local v7    # "availableLocale":Ljava/util/Locale;
    .restart local v14    # "displayFormat":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .restart local v20    # "i":I
    .restart local v22    # "limitCellularData":Z
    .restart local v23    # "locale":Ljava/lang/String;
    .restart local v24    # "locales":Landroid/os/LocaleList;
    .restart local v25    # "obdEnabled":Ljava/lang/String;
    .restart local v26    # "obdBlacklistLastModified":J
    .restart local v28    # "obdScanSetting":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .restart local v29    # "phoneName":Ljava/lang/String;
    .restart local v30    # "photoChecksum":Ljava/lang/String;
    .restart local v35    # "unitSystem":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    :cond_2
    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v21

    .line 212
    .local v21, "language":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v36

    if-nez v36, :cond_0

    .line 213
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 219
    .end local v7    # "availableLocale":Ljava/util/Locale;
    .end local v20    # "i":I
    .end local v21    # "language":Ljava/lang/String;
    .end local v24    # "locales":Landroid/os/LocaleList;
    :cond_3
    const-string v36, "dial_long_press_action"

    sget v37, Lcom/navdy/client/app/ui/settings/SettingsConstants;->DIAL_LONG_PRESS_ACTION_DEFAULT:I

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 221
    .local v13, "dialLongPressInt":I
    sget-object v36, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    invoke-virtual/range {v36 .. v36}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->getValue()I

    move-result v36

    move/from16 v0, v36

    if-ne v13, v0, :cond_4

    .line 222
    sget-object v12, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_PLACE_SEARCH:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 227
    .local v12, "dialLongPressAction":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    :goto_3
    new-instance v36, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    invoke-direct/range {v36 .. v36}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;-><init>()V

    .line 228
    invoke-static/range {v32 .. v33}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 229
    move-object/from16 v0, v36

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_name(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 230
    move-object/from16 v0, v36

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->device_name(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    const/16 v37, 0x0

    .line 231
    invoke-static/range {v37 .. v37}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->profile_is_public(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 232
    move-object/from16 v0, v36

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->photo_checksum(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 233
    move-object/from16 v0, v36

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_email(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 234
    move-object/from16 v0, v36

    invoke-virtual {v0, v8}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_make(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 235
    move-object/from16 v0, v36

    invoke-virtual {v0, v9}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_model(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 236
    move-object/from16 v0, v36

    invoke-virtual {v0, v10}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_year(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 237
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->auto_on_enabled(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 238
    move-object/from16 v0, v36

    invoke-virtual {v0, v14}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->display_format(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 239
    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->locale(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 240
    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->additionalLocales(Ljava/util/List;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 241
    move-object/from16 v0, v36

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->unit_system(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 242
    move-object/from16 v0, v36

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->feature_mode(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 243
    move-object/from16 v0, v36

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdScanSetting(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 244
    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->limit_bandwidth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 245
    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdBlacklistLastModified(Ljava/lang/Long;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 246
    move-object/from16 v0, v36

    invoke-virtual {v0, v12}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->dial_long_press_action(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;)Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    move-result-object v36

    .line 247
    invoke-virtual/range {v36 .. v36}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    move-result-object v15

    .line 249
    .local v15, "driverProfilePrefs":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    invoke-static {v15}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileSettingsToTheHud(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;)Z

    move-result v36

    return v36

    .line 224
    .end local v12    # "dialLongPressAction":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    .end local v15    # "driverProfilePrefs":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    :cond_4
    sget-object v12, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_VOICE_ASSISTANT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .restart local v12    # "dialLongPressAction":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    goto/16 :goto_3
.end method

.method public static sendGlancesSettingsToTheHud(Lcom/navdy/service/library/events/preferences/NotificationPreferences;)Z
    .locals 6
    .param p0, "notifPrefs"    # Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 645
    if-nez p0, :cond_0

    .line 660
    :goto_0
    return v3

    .line 649
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 650
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v4

    .line 651
    .local v0, "deviceIsConnected":Z
    :goto_1
    if-nez v0, :cond_2

    .line 652
    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUpdateSettingsUponNextConnection(Z)V

    goto :goto_0

    .end local v0    # "deviceIsConnected":Z
    :cond_1
    move v0, v3

    .line 650
    goto :goto_1

    .line 656
    .restart local v0    # "deviceIsConnected":Z
    :cond_2
    new-instance v2, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate$Builder;-><init>()V

    .line 657
    .local v2, "updateBldr":Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate$Builder;
    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 658
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NotificationPreferences;->serial_number:Ljava/lang/Long;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 659
    iput-object p0, v2, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/NotificationPreferences;

    .line 660
    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/NotificationPreferencesUpdate;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v3

    goto :goto_0
.end method

.method static sendHudSettings(Z)Z
    .locals 8
    .param p0, "gestureIsEnabled"    # Z

    .prologue
    .line 352
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 353
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v4, "hud_serial_number"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 356
    .local v2, "serialNumber":J
    invoke-static {v2, v3, p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->buildInputPreferences(JZ)Lcom/navdy/service/library/events/preferences/InputPreferences;

    move-result-object v0

    .line 358
    .local v0, "inputPrefs":Lcom/navdy/service/library/events/preferences/InputPreferences;
    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendInputSettingsToTheHud(Lcom/navdy/service/library/events/preferences/InputPreferences;)Z

    move-result v4

    return v4
.end method

.method private static sendHudSettingsBasedOnSharedPrefValue()Z
    .locals 4

    .prologue
    .line 366
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 367
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v2, "hud_gesture"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 368
    .local v0, "gestureIsEnabled":Z
    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendHudSettings(Z)Z

    move-result v2

    return v2
.end method

.method private static sendInputSettingsToTheHud(Lcom/navdy/service/library/events/preferences/InputPreferences;)Z
    .locals 6
    .param p0, "inputPrefs"    # Lcom/navdy/service/library/events/preferences/InputPreferences;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 396
    if-nez p0, :cond_0

    .line 411
    :goto_0
    return v3

    .line 400
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 401
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v4

    .line 402
    .local v0, "deviceIsConnected":Z
    :goto_1
    if-nez v0, :cond_2

    .line 403
    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUpdateSettingsUponNextConnection(Z)V

    goto :goto_0

    .end local v0    # "deviceIsConnected":Z
    :cond_1
    move v0, v3

    .line 401
    goto :goto_1

    .line 407
    .restart local v0    # "deviceIsConnected":Z
    :cond_2
    new-instance v2, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate$Builder;-><init>()V

    .line 408
    .local v2, "updateBldr":Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate$Builder;
    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 409
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/InputPreferences;->serial_number:Ljava/lang/Long;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 410
    iput-object p0, v2, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/InputPreferences;

    .line 411
    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/InputPreferencesUpdate;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v3

    goto :goto_0
.end method

.method static sendMessagingSettingsToTheHud(Ljava/util/ArrayList;Ljava/lang/Long;)Z
    .locals 6
    .param p1, "serialNumber"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p0, "cannedReplies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 575
    if-nez p0, :cond_0

    .line 590
    :goto_0
    return v3

    .line 579
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v2

    .line 580
    .local v2, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v4

    .line 581
    .local v1, "deviceIsConnected":Z
    :goto_1
    if-nez v1, :cond_2

    .line 582
    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUpdateSettingsUponNextConnection(Z)V

    goto :goto_0

    .end local v1    # "deviceIsConnected":Z
    :cond_1
    move v1, v3

    .line 580
    goto :goto_1

    .line 586
    .restart local v1    # "deviceIsConnected":Z
    :cond_2
    new-instance v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate$Builder;-><init>()V

    .line 587
    .local v0, "cannedResponses":Lcom/navdy/service/library/events/glances/CannedMessagesUpdate$Builder;
    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v3, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 588
    iput-object p1, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 589
    iput-object p0, v0, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate$Builder;->cannedMessage:Ljava/util/List;

    .line 590
    invoke-virtual {v0}, Lcom/navdy/service/library/events/glances/CannedMessagesUpdate$Builder;->build()Lcom/navdy/service/library/events/glances/CannedMessagesUpdate;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v3

    goto :goto_0
.end method

.method public static sendMessagingSettingsToTheHudBasedOnSharedPrefValue()Z
    .locals 8

    .prologue
    .line 559
    invoke-static {}, Lcom/navdy/client/app/ui/settings/MessagingSettingsActivity;->getRepliesFromSharedPrefs()Ljava/util/ArrayList;

    move-result-object v0

    .line 561
    .local v0, "cannedReplies":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 562
    .local v1, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v4, "nav_serial_number"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 565
    .local v2, "serialNumber":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendMessagingSettingsToTheHud(Ljava/util/ArrayList;Ljava/lang/Long;)Z

    move-result v4

    return v4
.end method

.method static sendNavSettingsToTheHud(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)Z
    .locals 6
    .param p0, "navPrefs"    # Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 504
    if-nez p0, :cond_0

    .line 519
    :goto_0
    return v3

    .line 508
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v5

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    .line 509
    .local v1, "remoteDevice":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v4

    .line 510
    .local v0, "deviceIsConnected":Z
    :goto_1
    if-nez v0, :cond_2

    .line 511
    invoke-static {v4}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->setUpdateSettingsUponNextConnection(Z)V

    goto :goto_0

    .end local v0    # "deviceIsConnected":Z
    :cond_1
    move v0, v3

    .line 509
    goto :goto_1

    .line 515
    .restart local v0    # "deviceIsConnected":Z
    :cond_2
    new-instance v2, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;-><init>()V

    .line 516
    .local v2, "updateBldr":Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;
    sget-object v3, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 517
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/NavigationPreferences;->serial_number:Ljava/lang/Long;

    iput-object v3, v2, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 518
    iput-object p0, v2, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->preferences:Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    .line 519
    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate$Builder;->build()Lcom/navdy/service/library/events/preferences/NavigationPreferencesUpdate;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v3

    goto :goto_0
.end method

.method public static sendNavSettingsToTheHudBasedOnSharedPrefValue()Z
    .locals 18

    .prologue
    .line 466
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v14

    .line 467
    .local v14, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v15, "nav_serial_number"

    const-wide/16 v16, 0x0

    invoke-interface/range {v14 .. v17}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 468
    .local v0, "serialNumber":J
    const-string v15, "nav_route_calculation"

    const/16 v16, 0x0

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 469
    .local v2, "calculateShortestRouteIsOn":Z
    const-string v15, "nav_auto_recalc"

    const/16 v16, 0x0

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 470
    .local v3, "autoRecalcIsOn":Z
    const-string v15, "nav_highways"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 471
    .local v4, "highwaysIsOn":Z
    const-string v15, "nav_toll_roads"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 472
    .local v5, "tollRoadsIsOn":Z
    const-string v15, "nav_ferries"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 473
    .local v6, "ferriesIsOn":Z
    const-string v15, "nav_tunnels"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 474
    .local v7, "tunnelsIsOn":Z
    const-string v15, "nav_unpaved_roads"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 476
    .local v8, "unpavedRoadsIsOn":Z
    const-string v15, "nav_auto_trains"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 478
    .local v9, "autoTrainsIsOn":Z
    const-string v15, "audio_speed_warnings"

    const/16 v16, 0x0

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 479
    .local v10, "spokenSpeedLimitWarningsAreOn":Z
    const-string v15, "audio_camera_warnings"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 480
    .local v11, "spokenCameraWarningsAreOn":Z
    const-string v15, "audio_turn_by_turn_instructions"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    .line 482
    .local v12, "spokenTurnByTurnIsOn":Z
    invoke-static/range {v0 .. v12}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->buildNavigationPreferences(JZZZZZZZZZZZ)Lcom/navdy/service/library/events/preferences/NavigationPreferences;

    move-result-object v13

    .line 495
    .local v13, "navPrefs":Lcom/navdy/service/library/events/preferences/NavigationPreferences;
    invoke-static {v13}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendNavSettingsToTheHud(Lcom/navdy/service/library/events/preferences/NavigationPreferences;)Z

    move-result v15

    return v15
.end method

.method public static sendSpeakerSettingsToTheHudBasedOnSharedPrefValue()Z
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 419
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    .line 420
    .local v6, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v8, "audio_serial_number"

    const-wide/16 v10, 0x0

    invoke-interface {v6, v8, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 421
    .local v0, "serialNumber":J
    const-string v8, "audio_all_audio"

    invoke-interface {v6, v8, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 423
    .local v2, "masterSound":Z
    const-string v8, "audio_hud_volume"

    const/16 v9, 0x64

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 425
    .local v3, "volumeLevel":I
    const-string v8, "audio_menu_feedback"

    invoke-interface {v6, v8, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 426
    .local v4, "feedbackSound":Z
    const-string v8, "audio_notifications"

    invoke-interface {v6, v8, v12}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 428
    .local v5, "notificationSound":Z
    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->buildDisplaySpeakerPreferences(JZIZZ)Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;

    move-result-object v7

    .line 434
    .local v7, "speakerPrefs":Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;
    invoke-static {v7}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDisplaySpeakerSettingsToTheHud(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)Z

    move-result v8

    return v8
.end method

.method public static setDefaultObdSettingDependingOnBlacklistAsync()V
    .locals 3

    .prologue
    .line 987
    sget-object v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setDefaultObdSettingDependingOnBlacklistAsync"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 988
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/settings/SettingsUtils$5;

    invoke-direct {v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils$5;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 1008
    return-void
.end method

.method public static setFeatureMode(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)V
    .locals 3
    .param p0, "featureMode"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .prologue
    .line 299
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/settings/SettingsUtils$4;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$4;-><init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 311
    return-void
.end method

.method static setLimitCellularData(Z)V
    .locals 3
    .param p0, "isLimited"    # Z

    .prologue
    .line 289
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/settings/SettingsUtils$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$3;-><init>(Z)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 296
    return-void
.end method

.method public static setSharedPrefsVersionToCurrent()V
    .locals 4

    .prologue
    .line 104
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 105
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 106
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "shared_pref_version"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 108
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 109
    return-void
.end method

.method public static setUnitSystem(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V
    .locals 3
    .param p0, "unitSystem"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .prologue
    .line 269
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/ui/settings/SettingsUtils$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$2;-><init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 279
    return-void
.end method

.method private static declared-synchronized setUpdateSettingsUponNextConnection(Z)V
    .locals 2
    .param p0, "updateNextTime"    # Z

    .prologue
    .line 115
    const-class v0, Lcom/navdy/client/app/ui/settings/SettingsUtils;

    monitor-enter v0

    :try_start_0
    sput-boolean p0, Lcom/navdy/client/app/ui/settings/SettingsUtils;->updateSettingsUponNextConnection:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    monitor-exit v0

    return-void

    .line 115
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static updateAllSettingsIfNecessary()V
    .locals 2

    .prologue
    .line 126
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getUpdateSettingsUponNextConnection()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 130
    :cond_0
    new-instance v0, Lcom/navdy/client/app/ui/settings/SettingsUtils$1;

    invoke-direct {v0}, Lcom/navdy/client/app/ui/settings/SettingsUtils$1;-><init>()V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 151
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/settings/SettingsUtils$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
