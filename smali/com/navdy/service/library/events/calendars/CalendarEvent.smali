.class public final Lcom/navdy/service/library/events/calendars/CalendarEvent;
.super Lcom/squareup/wire/Message;
.source "CalendarEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ALL_DAY:Ljava/lang/Boolean;

.field public static final DEFAULT_CALENDAR_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_COLOR:Ljava/lang/Integer;

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_END_TIME:Ljava/lang/Long;

.field public static final DEFAULT_LOCATION:Ljava/lang/String; = ""

.field public static final DEFAULT_START_TIME:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final all_day:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final calendar_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final color:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->FIXED32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destination:Lcom/navdy/service/library/events/destination/Destination;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final end_time:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final location:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final start_time:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 22
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->DEFAULT_START_TIME:Ljava/lang/Long;

    .line 23
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->DEFAULT_END_TIME:Ljava/lang/Long;

    .line 24
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->DEFAULT_ALL_DAY:Ljava/lang/Boolean;

    .line 25
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->DEFAULT_COLOR:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;)V
    .locals 9
    .param p1, "builder"    # Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;

    .prologue
    .line 92
    iget-object v1, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->display_name:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->start_time:Ljava/lang/Long;

    iget-object v3, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->end_time:Ljava/lang/Long;

    iget-object v4, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->all_day:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->color:Ljava/lang/Integer;

    iget-object v6, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->location:Ljava/lang/String;

    iget-object v7, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->calendar_name:Ljava/lang/String;

    iget-object v8, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;->destination:Lcom/navdy/service/library/events/destination/Destination;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/calendars/CalendarEvent;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination;)V

    .line 93
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 94
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;Lcom/navdy/service/library/events/calendars/CalendarEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/calendars/CalendarEvent$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/calendars/CalendarEvent;-><init>(Lcom/navdy/service/library/events/calendars/CalendarEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/destination/Destination;)V
    .locals 0
    .param p1, "display_name"    # Ljava/lang/String;
    .param p2, "start_time"    # Ljava/lang/Long;
    .param p3, "end_time"    # Ljava/lang/Long;
    .param p4, "all_day"    # Ljava/lang/Boolean;
    .param p5, "color"    # Ljava/lang/Integer;
    .param p6, "location"    # Ljava/lang/String;
    .param p7, "calendar_name"    # Ljava/lang/String;
    .param p8, "destination"    # Lcom/navdy/service/library/events/destination/Destination;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    .line 83
    iput-object p3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    .line 84
    iput-object p4, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    .line 85
    iput-object p5, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->color:Ljava/lang/Integer;

    .line 86
    iput-object p6, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->location:Ljava/lang/String;

    .line 87
    iput-object p7, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->calendar_name:Ljava/lang/String;

    .line 88
    iput-object p8, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 89
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    if-ne p1, p0, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v1

    .line 99
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/calendars/CalendarEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 100
    check-cast v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;

    .line 101
    .local v0, "o":Lcom/navdy/service/library/events/calendars/CalendarEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    .line 102
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    .line 103
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    .line 104
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->color:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->color:Ljava/lang/Integer;

    .line 105
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->location:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->location:Ljava/lang/String;

    .line 106
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->calendar_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->calendar_name:Ljava/lang/String;

    .line 107
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    iget-object v4, v0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    .line 108
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/calendars/CalendarEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 113
    iget v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->hashCode:I

    .line 114
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 115
    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->display_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 116
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->start_time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 117
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->end_time:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 118
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->all_day:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 119
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->color:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->color:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 120
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->location:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->location:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 121
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->calendar_name:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->calendar_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 122
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->destination:Lcom/navdy/service/library/events/destination/Destination;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/destination/Destination;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 123
    iput v0, p0, Lcom/navdy/service/library/events/calendars/CalendarEvent;->hashCode:I

    .line 125
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 115
    goto :goto_0

    :cond_3
    move v2, v1

    .line 116
    goto :goto_1

    :cond_4
    move v2, v1

    .line 117
    goto :goto_2

    :cond_5
    move v2, v1

    .line 118
    goto :goto_3

    :cond_6
    move v2, v1

    .line 119
    goto :goto_4

    :cond_7
    move v2, v1

    .line 120
    goto :goto_5

    :cond_8
    move v2, v1

    .line 121
    goto :goto_6
.end method
