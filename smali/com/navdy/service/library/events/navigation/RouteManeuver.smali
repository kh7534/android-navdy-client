.class public final Lcom/navdy/service/library/events/navigation/RouteManeuver;
.super Lcom/squareup/wire/Message;
.source "RouteManeuver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CURRENTROAD:Ljava/lang/String; = ""

.field public static final DEFAULT_DISTANCETOPENDINGROAD:Ljava/lang/Float;

.field public static final DEFAULT_DISTANCETOPENDINGROADUNIT:Lcom/navdy/service/library/events/navigation/DistanceUnit;

.field public static final DEFAULT_MANEUVERTIME:Ljava/lang/Integer;

.field public static final DEFAULT_PENDINGROAD:Ljava/lang/String; = ""

.field public static final DEFAULT_PENDINGTURN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field private static final serialVersionUID:J


# instance fields
.field public final currentRoad:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final distanceToPendingRoad:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final maneuverTime:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->UINT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final pendingRoad:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    sput-object v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->DEFAULT_PENDINGTURN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 19
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->DEFAULT_DISTANCETOPENDINGROAD:Ljava/lang/Float;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/navigation/DistanceUnit;->DISTANCE_MILES:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    sput-object v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->DEFAULT_DISTANCETOPENDINGROADUNIT:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 21
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->DEFAULT_MANEUVERTIME:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;

    .prologue
    .line 69
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->currentRoad:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->pendingRoad:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->distanceToPendingRoad:Ljava/lang/Float;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->maneuverTime:Ljava/lang/Integer;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/navigation/RouteManeuver;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationTurn;Ljava/lang/String;Ljava/lang/Float;Lcom/navdy/service/library/events/navigation/DistanceUnit;Ljava/lang/Integer;)V

    .line 70
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/RouteManeuver;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;Lcom/navdy/service/library/events/navigation/RouteManeuver$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/RouteManeuver$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/RouteManeuver;-><init>(Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationTurn;Ljava/lang/String;Ljava/lang/Float;Lcom/navdy/service/library/events/navigation/DistanceUnit;Ljava/lang/Integer;)V
    .locals 0
    .param p1, "currentRoad"    # Ljava/lang/String;
    .param p2, "pendingTurn"    # Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .param p3, "pendingRoad"    # Ljava/lang/String;
    .param p4, "distanceToPendingRoad"    # Ljava/lang/Float;
    .param p5, "distanceToPendingRoadUnit"    # Lcom/navdy/service/library/events/navigation/DistanceUnit;
    .param p6, "maneuverTime"    # Ljava/lang/Integer;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->currentRoad:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 62
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingRoad:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoad:Ljava/lang/Float;

    .line 64
    iput-object p5, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 65
    iput-object p6, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->maneuverTime:Ljava/lang/Integer;

    .line 66
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 76
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 77
    check-cast v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;

    .line 78
    .local v0, "o":Lcom/navdy/service/library/events/navigation/RouteManeuver;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->currentRoad:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->currentRoad:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuver;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuver;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingRoad:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingRoad:Ljava/lang/String;

    .line 80
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuver;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoad:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoad:Ljava/lang/Float;

    .line 81
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuver;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 82
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuver;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->maneuverTime:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->maneuverTime:Ljava/lang/Integer;

    .line 83
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/RouteManeuver;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 88
    iget v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->hashCode:I

    .line 89
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 90
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->currentRoad:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->currentRoad:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 91
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationTurn;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 92
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingRoad:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingRoad:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 93
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoad:Ljava/lang/Float;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoad:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 94
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/DistanceUnit;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 95
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->maneuverTime:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->maneuverTime:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 96
    iput v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver;->hashCode:I

    .line 98
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 90
    goto :goto_0

    :cond_3
    move v2, v1

    .line 91
    goto :goto_1

    :cond_4
    move v2, v1

    .line 92
    goto :goto_2

    :cond_5
    move v2, v1

    .line 93
    goto :goto_3

    :cond_6
    move v2, v1

    .line 94
    goto :goto_4
.end method
