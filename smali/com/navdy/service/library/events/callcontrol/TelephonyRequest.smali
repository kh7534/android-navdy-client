.class public final Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;
.super Lcom/squareup/wire/Message;
.source "TelephonyRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ACTION:Lcom/navdy/service/library/events/callcontrol/CallAction;

.field public static final DEFAULT_CALLUUID:Ljava/lang/String; = ""

.field public static final DEFAULT_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/navdy/service/library/events/callcontrol/CallAction;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final callUUID:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/CallAction;->CALL_ACCEPT:Lcom/navdy/service/library/events/callcontrol/CallAction;

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->DEFAULT_ACTION:Lcom/navdy/service/library/events/callcontrol/CallAction;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/callcontrol/CallAction;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "callUUID"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 42
    iput-object p2, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    .line 44
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;

    .prologue
    .line 47
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->number:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->callUUID:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;-><init>(Lcom/navdy/service/library/events/callcontrol/CallAction;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 49
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;-><init>(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 55
    check-cast v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    .line 56
    .local v0, "o":Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    .line 57
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    .line 58
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    iget v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->hashCode:I

    .line 64
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 65
    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/callcontrol/CallAction;->hashCode()I

    move-result v0

    .line 66
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 67
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 68
    iput v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->hashCode:I

    .line 70
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 65
    goto :goto_0

    :cond_3
    move v2, v1

    .line 66
    goto :goto_1
.end method
