.class public final Lcom/navdy/service/library/events/file/FileListRequest;
.super Lcom/squareup/wire/Message;
.source "FileListRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/FileListRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FILE_TYPE:Lcom/navdy/service/library/events/file/FileType;

.field private static final serialVersionUID:J


# instance fields
.field public final file_type:Lcom/navdy/service/library/events/file/FileType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_OTA:Lcom/navdy/service/library/events/file/FileType;

    sput-object v0, Lcom/navdy/service/library/events/file/FileListRequest;->DEFAULT_FILE_TYPE:Lcom/navdy/service/library/events/file/FileType;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/file/FileListRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/FileListRequest$Builder;

    .prologue
    .line 24
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileListRequest$Builder;->file_type:Lcom/navdy/service/library/events/file/FileType;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/file/FileListRequest;-><init>(Lcom/navdy/service/library/events/file/FileType;)V

    .line 25
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/FileListRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/FileListRequest$Builder;Lcom/navdy/service/library/events/file/FileListRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/FileListRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/FileListRequest$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/FileListRequest;-><init>(Lcom/navdy/service/library/events/file/FileListRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/FileType;)V
    .locals 0
    .param p1, "file_type"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileListRequest;->file_type:Lcom/navdy/service/library/events/file/FileType;

    .line 21
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 30
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 32
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 31
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/file/FileListRequest;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/file/FileListRequest;->file_type:Lcom/navdy/service/library/events/file/FileType;

    check-cast p1, Lcom/navdy/service/library/events/file/FileListRequest;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/file/FileListRequest;->file_type:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/file/FileListRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 37
    iget v0, p0, Lcom/navdy/service/library/events/file/FileListRequest;->hashCode:I

    .line 38
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileListRequest;->file_type:Lcom/navdy/service/library/events/file/FileType;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/file/FileListRequest;->file_type:Lcom/navdy/service/library/events/file/FileType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/file/FileType;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/file/FileListRequest;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
