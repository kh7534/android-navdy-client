.class public final Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
.super Lcom/squareup/wire/Message;
.source "VoiceSearchResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;,
        Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;,
        Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CONFIDENCELEVEL:Ljava/lang/Integer;

.field public static final DEFAULT_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public static final DEFAULT_LISTENINGOVERBLUETOOTH:Ljava/lang/Boolean;

.field public static final DEFAULT_LOCALE:Ljava/lang/String; = ""

.field public static final DEFAULT_PREFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_RECOGNIZEDWORDS:Ljava/lang/String; = ""

.field public static final DEFAULT_RESULTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

.field public static final DEFAULT_VOLUMELEVEL:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final confidenceLevel:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final listeningOverBluetooth:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final locale:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final prefix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final recognizedWords:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final results:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/destination/Destination;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public final state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final volumeLevel:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    sget-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->DEFAULT_STATE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 27
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->DEFAULT_VOLUMELEVEL:Ljava/lang/Integer;

    .line 28
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->DEFAULT_LISTENINGOVERBLUETOOTH:Ljava/lang/Boolean;

    .line 29
    sget-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->NOT_AVAILABLE:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->DEFAULT_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 30
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->DEFAULT_CONFIDENCELEVEL:Ljava/lang/Integer;

    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->DEFAULT_RESULTS:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;)V
    .locals 10
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;

    .prologue
    .line 103
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->volumeLevel:Ljava/lang/Integer;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iget-object v6, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->confidenceLevel:Ljava/lang/Integer;

    iget-object v7, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->results:Ljava/util/List;

    iget-object v8, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->prefix:Ljava/lang/String;

    iget-object v9, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->locale:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;-><init>(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 105
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$1;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;-><init>(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "state"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;
    .param p2, "recognizedWords"    # Ljava/lang/String;
    .param p3, "volumeLevel"    # Ljava/lang/Integer;
    .param p4, "listeningOverBluetooth"    # Ljava/lang/Boolean;
    .param p5, "error"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;
    .param p6, "confidenceLevel"    # Ljava/lang/Integer;
    .param p8, "prefix"    # Ljava/lang/String;
    .param p9, "locale"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 90
    .local p7, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 91
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 92
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    .line 93
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->volumeLevel:Ljava/lang/Integer;

    .line 94
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    .line 95
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 96
    iput-object p6, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    .line 97
    invoke-static {p7}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    .line 98
    iput-object p8, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    .line 99
    iput-object p9, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    .line 100
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 22
    invoke-static {p0}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 109
    if-ne p1, p0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v1

    .line 110
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 111
    check-cast v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    .line 112
    .local v0, "o":Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    .line 113
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->volumeLevel:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->volumeLevel:Ljava/lang/Integer;

    .line 114
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    .line 115
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 116
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    .line 117
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    .line 118
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    .line 119
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    .line 120
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 125
    iget v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->hashCode:I

    .line 126
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 127
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->hashCode()I

    move-result v0

    .line 128
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 129
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->volumeLevel:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->volumeLevel:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 130
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 131
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 132
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 133
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 134
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 135
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 136
    iput v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->hashCode:I

    .line 138
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 127
    goto :goto_0

    :cond_3
    move v2, v1

    .line 128
    goto :goto_1

    :cond_4
    move v2, v1

    .line 129
    goto :goto_2

    :cond_5
    move v2, v1

    .line 130
    goto :goto_3

    :cond_6
    move v2, v1

    .line 131
    goto :goto_4

    :cond_7
    move v2, v1

    .line 132
    goto :goto_5

    .line 133
    :cond_8
    const/4 v2, 0x1

    goto :goto_6

    :cond_9
    move v2, v1

    .line 134
    goto :goto_7
.end method
