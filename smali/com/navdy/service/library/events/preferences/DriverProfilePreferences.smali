.class public final Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
.super Lcom/squareup/wire/Message;
.source "DriverProfilePreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;,
        Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;,
        Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;,
        Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;,
        Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;,
        Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ADDITIONALLOCALES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTO_ON_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_CAR_MAKE:Ljava/lang/String; = ""

.field public static final DEFAULT_CAR_MODEL:Ljava/lang/String; = ""

.field public static final DEFAULT_CAR_YEAR:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_DIAL_LONG_PRESS_ACTION:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

.field public static final DEFAULT_DISPLAY_FORMAT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

.field public static final DEFAULT_DRIVER_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_DRIVER_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_FEATURE_MODE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

.field public static final DEFAULT_LIMIT_BANDWIDTH:Ljava/lang/Boolean;

.field public static final DEFAULT_LOCALE:Ljava/lang/String; = "en_US"

.field public static final DEFAULT_OBDBLACKLISTLASTMODIFIED:Ljava/lang/Long;

.field public static final DEFAULT_OBDSCANSETTING:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

.field public static final DEFAULT_PHOTO_CHECKSUM:Ljava/lang/String; = ""

.field public static final DEFAULT_PROFILE_IS_PUBLIC:Ljava/lang/Boolean;

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

.field public static final DEFAULT_UNIT_SYSTEM:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

.field private static final serialVersionUID:J


# instance fields
.field public final additionalLocales:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        tag = 0x13
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final auto_on_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final car_make:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final car_model:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final car_year:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final device_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x12
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final driver_email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final driver_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xe
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final limit_bandwidth:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x10
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final locale:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final obdBlacklistLastModified:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x11
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xf
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final photo_checksum:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final profile_is_public:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final serial_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 34
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_SERIAL_NUMBER:Ljava/lang/Long;

    .line 37
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_PROFILE_IS_PUBLIC:Ljava/lang/Boolean;

    .line 43
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_AUTO_ON_ENABLED:Ljava/lang/Boolean;

    .line 44
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->DISPLAY_FORMAT_NORMAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_DISPLAY_FORMAT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 46
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->UNIT_SYSTEM_IMPERIAL:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_UNIT_SYSTEM:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 47
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->FEATURE_MODE_RELEASE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_FEATURE_MODE:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 48
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->SCAN_DEFAULT_ON:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_OBDSCANSETTING:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 49
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_LIMIT_BANDWIDTH:Ljava/lang/Boolean;

    .line 50
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_OBDBLACKLISTLASTMODIFIED:Ljava/lang/Long;

    .line 51
    sget-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->DIAL_LONG_PRESS_VOICE_ASSISTANT:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_DIAL_LONG_PRESS_ACTION:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 52
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->DEFAULT_ADDITIONALLOCALES:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;)V
    .locals 21
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;

    .prologue
    .line 204
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->serial_number:Ljava/lang/Long;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_name:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->device_name:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->profile_is_public:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->photo_checksum:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->driver_email:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_make:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_model:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->car_year:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->auto_on_enabled:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->locale:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->limit_bandwidth:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->obdBlacklistLastModified:Ljava/lang/Long;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;->additionalLocales:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v20}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;-><init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;Ljava/lang/String;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;Ljava/util/List;)V

    .line 205
    invoke-virtual/range {p0 .. p1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 206
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$1;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;-><init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;Ljava/lang/String;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;Ljava/lang/Boolean;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;Ljava/util/List;)V
    .locals 2
    .param p1, "serial_number"    # Ljava/lang/Long;
    .param p2, "driver_name"    # Ljava/lang/String;
    .param p3, "device_name"    # Ljava/lang/String;
    .param p4, "profile_is_public"    # Ljava/lang/Boolean;
    .param p5, "photo_checksum"    # Ljava/lang/String;
    .param p6, "driver_email"    # Ljava/lang/String;
    .param p7, "car_make"    # Ljava/lang/String;
    .param p8, "car_model"    # Ljava/lang/String;
    .param p9, "car_year"    # Ljava/lang/String;
    .param p10, "auto_on_enabled"    # Ljava/lang/Boolean;
    .param p11, "display_format"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;
    .param p12, "locale"    # Ljava/lang/String;
    .param p13, "unit_system"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;
    .param p14, "feature_mode"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;
    .param p15, "obdScanSetting"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;
    .param p16, "limit_bandwidth"    # Ljava/lang/Boolean;
    .param p17, "obdBlacklistLastModified"    # Ljava/lang/Long;
    .param p18, "dial_long_press_action"    # Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    .local p19, "additionalLocales":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 182
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    .line 183
    iput-object p2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    .line 184
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->device_name:Ljava/lang/String;

    .line 185
    iput-object p4, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    .line 186
    iput-object p5, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->photo_checksum:Ljava/lang/String;

    .line 187
    iput-object p6, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_email:Ljava/lang/String;

    .line 188
    iput-object p7, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_make:Ljava/lang/String;

    .line 189
    iput-object p8, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_model:Ljava/lang/String;

    .line 190
    iput-object p9, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_year:Ljava/lang/String;

    .line 191
    iput-object p10, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->auto_on_enabled:Ljava/lang/Boolean;

    .line 192
    iput-object p11, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 193
    iput-object p12, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    .line 194
    iput-object p13, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 195
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 196
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 197
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->limit_bandwidth:Ljava/lang/Boolean;

    .line 198
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    .line 199
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 200
    invoke-static/range {p19 .. p19}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    .line 201
    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 31
    invoke-static {p0}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 210
    if-ne p1, p0, :cond_1

    .line 231
    :cond_0
    :goto_0
    return v1

    .line 211
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 212
    check-cast v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;

    .line 213
    .local v0, "o":Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    .line 214
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->device_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->device_name:Ljava/lang/String;

    .line 215
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    .line 216
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->photo_checksum:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->photo_checksum:Ljava/lang/String;

    .line 217
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_email:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_email:Ljava/lang/String;

    .line 218
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_make:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_make:Ljava/lang/String;

    .line 219
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_model:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_model:Ljava/lang/String;

    .line 220
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_year:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_year:Ljava/lang/String;

    .line 221
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->auto_on_enabled:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->auto_on_enabled:Ljava/lang/Boolean;

    .line 222
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    .line 223
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    .line 224
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    .line 225
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 226
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    .line 227
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->limit_bandwidth:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->limit_bandwidth:Ljava/lang/Boolean;

    .line 228
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    .line 229
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    .line 230
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    .line 231
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 236
    iget v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->hashCode:I

    .line 237
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 238
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->serial_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 239
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 240
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->device_name:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->device_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 241
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->profile_is_public:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 242
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->photo_checksum:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->photo_checksum:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 243
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_email:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->driver_email:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 244
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_make:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_make:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 245
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_model:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_model:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 246
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_year:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->car_year:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v3, v2

    .line 247
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->auto_on_enabled:Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->auto_on_enabled:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v3, v2

    .line 248
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->display_format:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DisplayFormat;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v3, v2

    .line 249
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->locale:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v3, v2

    .line 250
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->unit_system:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$UnitSystem;->hashCode()I

    move-result v2

    :goto_c
    add-int v0, v3, v2

    .line 251
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->feature_mode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->hashCode()I

    move-result v2

    :goto_d
    add-int v0, v3, v2

    .line 252
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdScanSetting:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$ObdScanSetting;->hashCode()I

    move-result v2

    :goto_e
    add-int v0, v3, v2

    .line 253
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->limit_bandwidth:Ljava/lang/Boolean;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->limit_bandwidth:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_f
    add-int v0, v3, v2

    .line 254
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->obdBlacklistLastModified:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_10
    add-int v0, v3, v2

    .line 255
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->dial_long_press_action:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$DialLongPressAction;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 256
    mul-int/lit8 v2, v0, 0x25

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->additionalLocales:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :goto_11
    add-int v0, v2, v1

    .line 257
    iput v0, p0, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences;->hashCode:I

    .line 259
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 238
    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 239
    goto/16 :goto_1

    :cond_4
    move v2, v1

    .line 240
    goto/16 :goto_2

    :cond_5
    move v2, v1

    .line 241
    goto/16 :goto_3

    :cond_6
    move v2, v1

    .line 242
    goto/16 :goto_4

    :cond_7
    move v2, v1

    .line 243
    goto/16 :goto_5

    :cond_8
    move v2, v1

    .line 244
    goto/16 :goto_6

    :cond_9
    move v2, v1

    .line 245
    goto/16 :goto_7

    :cond_a
    move v2, v1

    .line 246
    goto/16 :goto_8

    :cond_b
    move v2, v1

    .line 247
    goto/16 :goto_9

    :cond_c
    move v2, v1

    .line 248
    goto/16 :goto_a

    :cond_d
    move v2, v1

    .line 249
    goto/16 :goto_b

    :cond_e
    move v2, v1

    .line 250
    goto :goto_c

    :cond_f
    move v2, v1

    .line 251
    goto :goto_d

    :cond_10
    move v2, v1

    .line 252
    goto :goto_e

    :cond_11
    move v2, v1

    .line 253
    goto :goto_f

    :cond_12
    move v2, v1

    .line 254
    goto :goto_10

    .line 256
    :cond_13
    const/4 v1, 0x1

    goto :goto_11
.end method
