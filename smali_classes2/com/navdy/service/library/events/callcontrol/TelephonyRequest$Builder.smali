.class public final Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TelephonyRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/navdy/service/library/events/callcontrol/CallAction;

.field public callUUID:Ljava/lang/String;

.field public number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 84
    if-nez p1, :cond_0

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 86
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->number:Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->callUUID:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->callUUID:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public action(Lcom/navdy/service/library/events/callcontrol/CallAction;)Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/callcontrol/CallAction;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    .line 95
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->checkRequiredFields()V

    .line 120
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;-><init>(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->build()Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;

    move-result-object v0

    return-object v0
.end method

.method public callUUID(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;
    .locals 0
    .param p1, "callUUID"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->callUUID:Ljava/lang/String;

    .line 114
    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;
    .locals 0
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest$Builder;->number:Ljava/lang/String;

    .line 106
    return-object p0
.end method
