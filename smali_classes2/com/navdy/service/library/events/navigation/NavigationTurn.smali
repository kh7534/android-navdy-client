.class public final enum Lcom/navdy/service/library/events/navigation/NavigationTurn;
.super Ljava/lang/Enum;
.source "NavigationTurn.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/navigation/NavigationTurn;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_EXIT_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_EXIT_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_EXIT_ROUNDABOUT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_FERRY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_FOLLOW_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_MERGE_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_MERGE_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_MOTORWAY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_OUT_OF_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_E:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_NE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_NW:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_SE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_SW:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_ROUNDABOUT_W:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_STATE_BOUNDARY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_UNKNOWN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final enum NAV_TURN_UTURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 9
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_START"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 10
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_EASY_LEFT"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 11
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_EASY_RIGHT"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 12
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_END"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 13
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_KEEP_LEFT"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 14
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_KEEP_RIGHT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 15
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_LEFT"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 16
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_OUT_OF_ROUTE"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_OUT_OF_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 17
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_RIGHT"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 18
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_SHARP_LEFT"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 19
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_SHARP_RIGHT"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 20
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_STRAIGHT"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 21
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_UTURN_LEFT"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 22
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_UTURN_RIGHT"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 23
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_SE"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_SE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 24
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_E"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_E:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 25
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_NE"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_NE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 26
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_N"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 27
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_NW"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_NW:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 28
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_W"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_W:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 29
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_SW"

    const/16 v2, 0x14

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_SW:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 30
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_ROUNDABOUT_S"

    const/16 v2, 0x15

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 31
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_FERRY"

    const/16 v2, 0x16

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_FERRY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 32
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_STATE_BOUNDARY"

    const/16 v2, 0x17

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STATE_BOUNDARY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 33
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_FOLLOW_ROUTE"

    const/16 v2, 0x18

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_FOLLOW_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 34
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_EXIT_RIGHT"

    const/16 v2, 0x19

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 35
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_EXIT_LEFT"

    const/16 v2, 0x1a

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 36
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_MOTORWAY"

    const/16 v2, 0x1b

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MOTORWAY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 37
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_EXIT_ROUNDABOUT"

    const/16 v2, 0x1c

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_ROUNDABOUT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 38
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_UNKNOWN"

    const/16 v2, 0x1d

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UNKNOWN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 39
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_MERGE_LEFT"

    const/16 v2, 0x1e

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 40
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const-string v1, "NAV_TURN_MERGE_RIGHT"

    const/16 v2, 0x1f

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/navigation/NavigationTurn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 7
    const/16 v0, 0x20

    new-array v0, v0, [Lcom/navdy/service/library/events/navigation/NavigationTurn;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EASY_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_END:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_KEEP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_OUT_OF_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_SHARP_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STRAIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UTURN_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_SE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_E:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_NE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_N:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_NW:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_W:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_SW:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_ROUNDABOUT_S:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_FERRY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_STATE_BOUNDARY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_FOLLOW_ROUTE:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MOTORWAY:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_EXIT_ROUNDABOUT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_UNKNOWN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_LEFT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_MERGE_RIGHT:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->$VALUES:[Lcom/navdy/service/library/events/navigation/NavigationTurn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->value:I

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->$VALUES:[Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/navigation/NavigationTurn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/navigation/NavigationTurn;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->value:I

    return v0
.end method
