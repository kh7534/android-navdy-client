.class public final Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RouteManeuver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/navigation/RouteManeuver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/navigation/RouteManeuver;",
        ">;"
    }
.end annotation


# instance fields
.field public currentRoad:Ljava/lang/String;

.field public distanceToPendingRoad:Ljava/lang/Float;

.field public distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

.field public maneuverTime:Ljava/lang/Integer;

.field public pendingRoad:Ljava/lang/String;

.field public pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 111
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/navigation/RouteManeuver;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/navigation/RouteManeuver;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 115
    if-nez p1, :cond_0

    .line 122
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver;->currentRoad:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->currentRoad:Ljava/lang/String;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 118
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver;->pendingRoad:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->pendingRoad:Ljava/lang/String;

    .line 119
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoad:Ljava/lang/Float;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->distanceToPendingRoad:Ljava/lang/Float;

    .line 120
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 121
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/RouteManeuver;->maneuverTime:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->maneuverTime:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/navigation/RouteManeuver;
    .locals 2

    .prologue
    .line 174
    new-instance v0, Lcom/navdy/service/library/events/navigation/RouteManeuver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/navigation/RouteManeuver;-><init>(Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;Lcom/navdy/service/library/events/navigation/RouteManeuver$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->build()Lcom/navdy/service/library/events/navigation/RouteManeuver;

    move-result-object v0

    return-object v0
.end method

.method public currentRoad(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    .locals 0
    .param p1, "currentRoad"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->currentRoad:Ljava/lang/String;

    .line 129
    return-object p0
.end method

.method public distanceToPendingRoad(Ljava/lang/Float;)Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    .locals 0
    .param p1, "distanceToPendingRoad"    # Ljava/lang/Float;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->distanceToPendingRoad:Ljava/lang/Float;

    .line 153
    return-object p0
.end method

.method public distanceToPendingRoadUnit(Lcom/navdy/service/library/events/navigation/DistanceUnit;)Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    .locals 0
    .param p1, "distanceToPendingRoadUnit"    # Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->distanceToPendingRoadUnit:Lcom/navdy/service/library/events/navigation/DistanceUnit;

    .line 161
    return-object p0
.end method

.method public maneuverTime(Ljava/lang/Integer;)Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    .locals 0
    .param p1, "maneuverTime"    # Ljava/lang/Integer;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->maneuverTime:Ljava/lang/Integer;

    .line 169
    return-object p0
.end method

.method public pendingRoad(Ljava/lang/String;)Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    .locals 0
    .param p1, "pendingRoad"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->pendingRoad:Ljava/lang/String;

    .line 145
    return-object p0
.end method

.method public pendingTurn(Lcom/navdy/service/library/events/navigation/NavigationTurn;)Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;
    .locals 0
    .param p1, "pendingTurn"    # Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/RouteManeuver$Builder;->pendingTurn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 137
    return-object p0
.end method
