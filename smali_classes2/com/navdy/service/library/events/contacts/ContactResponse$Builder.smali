.class public final Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ContactResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/contacts/ContactResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/contacts/ContactResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;"
        }
    .end annotation
.end field

.field public identifier:Ljava/lang/String;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 95
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/contacts/ContactResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/contacts/ContactResponse;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 99
    if-nez p1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/ContactResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 101
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/ContactResponse;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 102
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/ContactResponse;->identifier:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->identifier:Ljava/lang/String;

    .line 103
    iget-object v0, p1, Lcom/navdy/service/library/events/contacts/ContactResponse;->contacts:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/contacts/ContactResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->contacts:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/contacts/ContactResponse;
    .locals 2

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->checkRequiredFields()V

    .line 141
    new-instance v0, Lcom/navdy/service/library/events/contacts/ContactResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/contacts/ContactResponse;-><init>(Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;Lcom/navdy/service/library/events/contacts/ContactResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->build()Lcom/navdy/service/library/events/contacts/ContactResponse;

    move-result-object v0

    return-object v0
.end method

.method public contacts(Ljava/util/List;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/contacts/Contact;",
            ">;)",
            "Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/contacts/Contact;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->contacts:Ljava/util/List;

    .line 135
    return-object p0
.end method

.method public identifier(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->identifier:Ljava/lang/String;

    .line 127
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 111
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/navdy/service/library/events/contacts/ContactResponse$Builder;->statusDetail:Ljava/lang/String;

    .line 119
    return-object p0
.end method
