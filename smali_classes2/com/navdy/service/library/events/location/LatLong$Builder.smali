.class public final Lcom/navdy/service/library/events/location/LatLong$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LatLong.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/location/LatLong;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/location/LatLong;",
        ">;"
    }
.end annotation


# instance fields
.field public latitude:Ljava/lang/Double;

.field public longitude:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/location/LatLong;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/location/LatLong;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 66
    if-nez p1, :cond_0

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/LatLong$Builder;->latitude:Ljava/lang/Double;

    .line 68
    iget-object v0, p1, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    iput-object v0, p0, Lcom/navdy/service/library/events/location/LatLong$Builder;->longitude:Ljava/lang/Double;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/location/LatLong;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/navdy/service/library/events/location/LatLong$Builder;->checkRequiredFields()V

    .line 84
    new-instance v0, Lcom/navdy/service/library/events/location/LatLong;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/location/LatLong;-><init>(Lcom/navdy/service/library/events/location/LatLong$Builder;Lcom/navdy/service/library/events/location/LatLong$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/navdy/service/library/events/location/LatLong$Builder;->build()Lcom/navdy/service/library/events/location/LatLong;

    move-result-object v0

    return-object v0
.end method

.method public latitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/LatLong$Builder;
    .locals 0
    .param p1, "latitude"    # Ljava/lang/Double;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/navdy/service/library/events/location/LatLong$Builder;->latitude:Ljava/lang/Double;

    .line 73
    return-object p0
.end method

.method public longitude(Ljava/lang/Double;)Lcom/navdy/service/library/events/location/LatLong$Builder;
    .locals 0
    .param p1, "longitude"    # Ljava/lang/Double;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/navdy/service/library/events/location/LatLong$Builder;->longitude:Ljava/lang/Double;

    .line 78
    return-object p0
.end method
