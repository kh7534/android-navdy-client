.class public final Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicArtworkResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicArtworkResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public author:Ljava/lang/String;

.field public collectionId:Ljava/lang/String;

.field public collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public name:Ljava/lang/String;

.field public photo:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 107
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicArtworkResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 111
    if-nez p1, :cond_0

    .line 119
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 113
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->name:Ljava/lang/String;

    .line 114
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->album:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->album:Ljava/lang/String;

    .line 115
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->author:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->author:Ljava/lang/String;

    .line 116
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->photo:Lokio/ByteString;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->photo:Lokio/ByteString;

    .line 117
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 118
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;->collectionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public album(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .locals 0
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->album:Ljava/lang/String;

    .line 139
    return-object p0
.end method

.method public author(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .locals 0
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->author:Ljava/lang/String;

    .line 144
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/audio/MusicArtworkResponse;
    .locals 2

    .prologue
    .line 164
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse;-><init>(Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;Lcom/navdy/service/library/events/audio/MusicArtworkResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->build()Lcom/navdy/service/library/events/audio/MusicArtworkResponse;

    move-result-object v0

    return-object v0
.end method

.method public collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .locals 0
    .param p1, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionId:Ljava/lang/String;

    .line 159
    return-object p0
.end method

.method public collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 126
    return-object p0
.end method

.method public collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .locals 0
    .param p1, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 154
    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->name:Ljava/lang/String;

    .line 134
    return-object p0
.end method

.method public photo(Lokio/ByteString;)Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;
    .locals 0
    .param p1, "photo"    # Lokio/ByteString;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicArtworkResponse$Builder;->photo:Lokio/ByteString;

    .line 149
    return-object p0
.end method
