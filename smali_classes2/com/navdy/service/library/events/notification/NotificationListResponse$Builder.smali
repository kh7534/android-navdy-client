.class public final Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "NotificationListResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/notification/NotificationListResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/notification/NotificationListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public status_detail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationListResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/notification/NotificationListResponse;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 82
    if-nez p1, :cond_0

    .line 86
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationListResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 84
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationListResponse;->status_detail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->status_detail:Ljava/lang/String;

    .line 85
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationListResponse;->ids:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/notification/NotificationListResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->ids:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/notification/NotificationListResponse;
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->checkRequiredFields()V

    .line 112
    new-instance v0, Lcom/navdy/service/library/events/notification/NotificationListResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/notification/NotificationListResponse;-><init>(Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;Lcom/navdy/service/library/events/notification/NotificationListResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->build()Lcom/navdy/service/library/events/notification/NotificationListResponse;

    move-result-object v0

    return-object v0
.end method

.method public ids(Ljava/util/List;)Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->ids:Ljava/util/List;

    .line 106
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 90
    return-object p0
.end method

.method public status_detail(Ljava/lang/String;)Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;
    .locals 0
    .param p1, "status_detail"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationListResponse$Builder;->status_detail:Ljava/lang/String;

    .line 98
    return-object p0
.end method
