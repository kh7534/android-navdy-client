.class public interface abstract Lcom/navdy/service/library/device/RemoteDeviceRegistry$DeviceListUpdatedListener;
.super Ljava/lang/Object;
.source "RemoteDeviceRegistry.java"

# interfaces
.implements Lcom/navdy/service/library/util/Listenable$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/RemoteDeviceRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceListUpdatedListener"
.end annotation


# virtual methods
.method public abstract onDeviceListChanged(Ljava/util/Set;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionInfo;",
            ">;)V"
        }
    .end annotation
.end method
