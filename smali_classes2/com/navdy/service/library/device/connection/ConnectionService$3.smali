.class Lcom/navdy/service/library/device/connection/ConnectionService$3;
.super Landroid/content/BroadcastReceiver;
.source "ConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/connection/ConnectionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/service/library/device/connection/ConnectionService;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/service/library/device/connection/ConnectionService;

    .prologue
    .line 715
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/ConnectionService$3;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 718
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 720
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 721
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    const/high16 v3, -0x80000000

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 722
    .local v1, "btState":I
    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    .line 723
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$3;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->state:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionService$State;->START:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    if-ne v2, v3, :cond_1

    .line 724
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$3;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "bluetooth turned on - exiting START state"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 725
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$3;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    sget-object v3, Lcom/navdy/service/library/device/connection/ConnectionService$State;->IDLE:Lcom/navdy/service/library/device/connection/ConnectionService$State;

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/device/connection/ConnectionService;->setState(Lcom/navdy/service/library/device/connection/ConnectionService$State;)V

    .line 732
    .end local v1    # "btState":I
    :cond_0
    :goto_0
    return-void

    .line 727
    .restart local v1    # "btState":I
    :cond_1
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$3;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "bluetooth turned on - restarting listeners"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 728
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/ConnectionService$3;->this$0:Lcom/navdy/service/library/device/connection/ConnectionService;

    iget-object v2, v2, Lcom/navdy/service/library/device/connection/ConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
