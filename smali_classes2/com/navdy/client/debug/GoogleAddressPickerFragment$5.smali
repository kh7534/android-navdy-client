.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/location/places/PlaceBuffer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 402
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0

    .prologue
    .line 402
    check-cast p1, Lcom/google/android/gms/location/places/PlaceBuffer;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->onResult(Lcom/google/android/gms/location/places/PlaceBuffer;)V

    return-void
.end method

.method public onResult(Lcom/google/android/gms/location/places/PlaceBuffer;)V
    .locals 19
    .param p1, "places"    # Lcom/google/android/gms/location/places/PlaceBuffer;

    .prologue
    .line 408
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/location/places/PlaceBuffer;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v15

    if-nez v15, :cond_0

    .line 410
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v15}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$200(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    .line 411
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Place query did not complete. Error: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/location/places/PlaceBuffer;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/common/api/Status;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 412
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v15

    .line 413
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/location/places/PlaceBuffer;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/api/Status;->toString()Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x1

    .line 412
    invoke-static/range {v15 .. v17}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v15

    .line 414
    invoke-virtual {v15}, Landroid/widget/Toast;->show()V

    .line 478
    :goto_0
    return-void

    .line 417
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/location/places/PlaceBuffer;->getCount()I

    move-result v15

    if-nez v15, :cond_1

    .line 419
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v15}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$200(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    .line 420
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/location/places/PlaceBuffer;->release()V

    .line 421
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v15

    const-string v16, "No places returned"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 422
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v15

    const v16, 0x7f0803e7

    const/16 v17, 0x1

    invoke-static/range {v15 .. v17}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v15

    .line 424
    invoke-virtual {v15}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 428
    :cond_1
    const/4 v14, 0x0

    .line 429
    .local v14, "place":Lcom/google/android/gms/location/places/Place;
    const/4 v7, 0x0

    .line 430
    .local v7, "exitLoop":Z
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/location/places/PlaceBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/gms/location/places/Place;

    .line 431
    .local v13, "p":Lcom/google/android/gms/location/places/Place;
    invoke-interface {v13}, Lcom/google/android/gms/location/places/Place;->getPlaceTypes()Ljava/util/List;

    move-result-object v10

    .line 432
    .local v10, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 433
    .local v8, "i":Ljava/lang/Integer;
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const/16 v18, 0x3fd

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 434
    move-object v14, v13

    .line 435
    const/4 v7, 0x1

    .line 439
    .end local v8    # "i":Ljava/lang/Integer;
    :cond_4
    if-eqz v7, :cond_2

    .line 444
    .end local v10    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v13    # "p":Lcom/google/android/gms/location/places/Place;
    :cond_5
    if-nez v14, :cond_6

    .line 445
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v15

    const-string v16, "Street address type lookup failed: place is null"

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 446
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/google/android/gms/location/places/PlaceBuffer;->get(I)Lcom/google/android/gms/location/places/Place;

    move-result-object v14

    .line 447
    const/4 v4, 0x0

    .line 451
    .local v4, "b":Z
    :goto_1
    move v9, v4

    .line 452
    .local v9, "isStreetAddress":Z
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Place details received: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-interface {v14}, Lcom/google/android/gms/location/places/Place;->getName()Ljava/lang/CharSequence;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 453
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iput-object v14, v15, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->mLastSelectedPlace:Lcom/google/android/gms/location/places/Place;

    .line 454
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v15, v14}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1200(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/location/places/Place;)V

    .line 456
    move-object v6, v14

    .line 457
    .local v6, "destinationPlace":Lcom/google/android/gms/location/places/Place;
    invoke-static {}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$700()Lcom/navdy/service/library/log/Logger;

    move-result-object v15

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "google display coordinate="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-interface {v14}, Lcom/google/android/gms/location/places/Place;->getLatLng()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 459
    invoke-interface {v6}, Lcom/google/android/gms/location/places/Place;->getName()Ljava/lang/CharSequence;

    move-result-object v11

    .line 460
    .local v11, "name":Ljava/lang/CharSequence;
    invoke-interface {v6}, Lcom/google/android/gms/location/places/Place;->getAddress()Ljava/lang/CharSequence;

    move-result-object v2

    .line 461
    .local v2, "address":Ljava/lang/CharSequence;
    if-eqz v11, :cond_7

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    .line 462
    .local v12, "nameString":Ljava/lang/String;
    :goto_2
    if-eqz v2, :cond_8

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 463
    .local v3, "addressString":Ljava/lang/String;
    :goto_3
    new-instance v5, Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {v5, v12, v3}, Lcom/navdy/client/app/framework/models/Destination;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    .local v5, "destination":Lcom/navdy/client/app/framework/models/Destination;
    new-instance v15, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v15, v0, v6, v9, v1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;-><init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;Lcom/google/android/gms/location/places/Place;ZLcom/google/android/gms/location/places/PlaceBuffer;)V

    invoke-static {v5, v15}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    goto/16 :goto_0

    .line 449
    .end local v2    # "address":Ljava/lang/CharSequence;
    .end local v3    # "addressString":Ljava/lang/String;
    .end local v4    # "b":Z
    .end local v5    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    .end local v6    # "destinationPlace":Lcom/google/android/gms/location/places/Place;
    .end local v9    # "isStreetAddress":Z
    .end local v11    # "name":Ljava/lang/CharSequence;
    .end local v12    # "nameString":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x1

    .restart local v4    # "b":Z
    goto :goto_1

    .line 461
    .restart local v2    # "address":Ljava/lang/CharSequence;
    .restart local v6    # "destinationPlace":Lcom/google/android/gms/location/places/Place;
    .restart local v9    # "isStreetAddress":Z
    .restart local v11    # "name":Ljava/lang/CharSequence;
    :cond_7
    const-string v12, ""

    goto :goto_2

    .line 462
    .restart local v12    # "nameString":Ljava/lang/String;
    :cond_8
    const-string v3, ""

    goto :goto_3
.end method
