.class public Lcom/navdy/client/debug/NavCoordTestSuiteActivity;
.super Landroid/app/Activity;
.source "NavCoordTestSuiteActivity.java"


# instance fields
.field private input:Landroid/widget/EditText;

.field private logOutput:Landroid/widget/TextView;

.field private navCoordTestSuite:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static startNavCoordTestSuiteActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v1, 0x7f030036

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->setContentView(I)V

    .line 41
    const v1, 0x7f10010d

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->input:Landroid/widget/EditText;

    .line 43
    invoke-virtual {p0}, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "query":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 49
    :cond_0
    invoke-static {}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->getInstance()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->navCoordTestSuite:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .line 50
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->navCoordTestSuite:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-virtual {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->onPause()V

    .line 63
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 64
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 55
    .local v0, "handler":Landroid/os/Handler;
    const v1, 0x7f100110

    invoke-virtual {p0, v1}, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->logOutput:Landroid/widget/TextView;

    .line 56
    iget-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->navCoordTestSuite:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    iget-object v2, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->logOutput:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->onResume(Landroid/os/Handler;Landroid/widget/TextView;)V

    .line 57
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 58
    return-void
.end method

.method public onRunSingleTestClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 72
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 73
    iget-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->input:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "query":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->navCoordTestSuite:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-virtual {v1, v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->run(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public onRunTestSuiteClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->dismissKeyboard(Landroid/app/Activity;)V

    .line 68
    iget-object v0, p0, Lcom/navdy/client/debug/NavCoordTestSuiteActivity;->navCoordTestSuite:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-virtual {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->run()V

    .line 69
    return-void
.end method
