.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment;->initMap()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    .prologue
    .line 864
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapClick(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 5
    .param p1, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 867
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v2, p1}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1600(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/maps/model/LatLng;)Ljava/util/List;

    move-result-object v1

    .line 868
    .local v1, "selectedPolylines":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/maps/model/Polyline;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 869
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$500(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)V

    .line 870
    iget-object v3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1700(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)I

    move-result v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_1

    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1704(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)I

    move-result v2

    :goto_0
    invoke-static {v3, v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1702(Lcom/navdy/client/debug/GoogleAddressPickerFragment;I)I

    .line 871
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-static {v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1700(Lcom/navdy/client/debug/GoogleAddressPickerFragment;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/Polyline;

    .line 872
    .local v0, "p":Lcom/google/android/gms/maps/model/Polyline;
    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/Polyline;->setWidth(F)V

    .line 873
    iget-object v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$8;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    invoke-virtual {v2}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0016

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/Polyline;->setColor(I)V

    .line 875
    .end local v0    # "p":Lcom/google/android/gms/maps/model/Polyline;
    :cond_0
    return-void

    .line 870
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
