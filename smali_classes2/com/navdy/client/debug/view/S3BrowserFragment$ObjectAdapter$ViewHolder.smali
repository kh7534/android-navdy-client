.class Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "S3BrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewHolder"
.end annotation


# instance fields
.field private final key:Landroid/widget/TextView;

.field private final size:Landroid/widget/TextView;

.field final synthetic this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;


# direct methods
.method private constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;Landroid/view/View;)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    const v0, 0x7f1002f9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->key:Landroid/widget/TextView;

    .line 358
    const v0, 0x7f1002fa

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->size:Landroid/widget/TextView;

    .line 359
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;Landroid/view/View;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;
    .param p2, "x1"    # Landroid/view/View;
    .param p3, "x2"    # Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->key:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter$ViewHolder;->size:Landroid/widget/TextView;

    return-object v0
.end method
