.class final enum Lcom/navdy/client/debug/SettingsFragment$ListItem;
.super Ljava/lang/Enum;
.source "SettingsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/SettingsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ListItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/debug/SettingsFragment$ListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/debug/SettingsFragment$ListItem;

.field public static final enum ABOUT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

.field public static final enum CONNECT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

.field public static final enum HUD_SETTINGS:Lcom/navdy/client/debug/SettingsFragment$ListItem;

.field public static final enum SCREEN_LAYOUT:Lcom/navdy/client/debug/SettingsFragment$ListItem;


# instance fields
.field private final mResId:I

.field private mString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    const-string v1, "CONNECT"

    const v2, 0x7f08042f

    invoke-direct {v0, v1, v3, v2}, Lcom/navdy/client/debug/SettingsFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->CONNECT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    .line 35
    new-instance v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    const-string v1, "SCREEN_LAYOUT"

    const v2, 0x7f080408

    invoke-direct {v0, v1, v4, v2}, Lcom/navdy/client/debug/SettingsFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->SCREEN_LAYOUT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    .line 37
    new-instance v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    const-string v1, "HUD_SETTINGS"

    const v2, 0x7f08024a

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/client/debug/SettingsFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->HUD_SETTINGS:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    .line 38
    new-instance v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    const-string v1, "ABOUT"

    const v2, 0x7f0800a2

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/client/debug/SettingsFragment$ListItem;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->ABOUT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/client/debug/SettingsFragment$ListItem;

    sget-object v1, Lcom/navdy/client/debug/SettingsFragment$ListItem;->CONNECT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/debug/SettingsFragment$ListItem;->SCREEN_LAYOUT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/debug/SettingsFragment$ListItem;->HUD_SETTINGS:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/debug/SettingsFragment$ListItem;->ABOUT:Lcom/navdy/client/debug/SettingsFragment$ListItem;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/SettingsFragment$ListItem;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "item"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->mResId:I

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/debug/SettingsFragment$ListItem;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/debug/SettingsFragment$ListItem;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->$VALUES:[Lcom/navdy/client/debug/SettingsFragment$ListItem;

    invoke-virtual {v0}, [Lcom/navdy/client/debug/SettingsFragment$ListItem;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/debug/SettingsFragment$ListItem;

    return-object v0
.end method


# virtual methods
.method bindResource(Landroid/content/res/Resources;)V
    .locals 1
    .param p1, "r"    # Landroid/content/res/Resources;

    .prologue
    .line 50
    iget v0, p0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->mResId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->mString:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/navdy/client/debug/SettingsFragment$ListItem;->mString:Ljava/lang/String;

    return-object v0
.end method
