.class Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;
.super Ljava/lang/Object;
.source "NavCoordTestSuite.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->log(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

.field final synthetic val$message:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 650
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->val$message:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 653
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1300(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 654
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->val$message:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x1388

    if-le v0, v1, :cond_0

    .line 655
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/lang/CharSequence;

    move-result-object v0

    const/16 v1, 0x1f4

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    .line 657
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->val$message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1402(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 658
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1300(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$5;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1400(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 660
    :cond_1
    return-void
.end method
