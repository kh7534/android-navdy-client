.class Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;
.super Ljava/lang/Object;
.source "NavigationDemoActivity.java"

# interfaces
.implements Lcom/here/android/mpa/guidance/VoiceCatalog$OnDownloadDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;->onDownloadDone(Lcom/here/android/mpa/guidance/VoiceCatalog$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;

.field final synthetic val$voicePackage:Lcom/here/android/mpa/guidance/VoicePackage;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;Lcom/here/android/mpa/guidance/VoicePackage;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;

    .prologue
    .line 400
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;->this$1:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;->val$voicePackage:Lcom/here/android/mpa/guidance/VoicePackage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDownloadDone(Lcom/here/android/mpa/guidance/VoiceCatalog$Error;)V
    .locals 4
    .param p1, "error"    # Lcom/here/android/mpa/guidance/VoiceCatalog$Error;

    .prologue
    .line 404
    sget-object v0, Lcom/here/android/mpa/guidance/VoiceCatalog$Error;->NONE:Lcom/here/android/mpa/guidance/VoiceCatalog$Error;

    if-ne p1, v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;->this$1:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;

    iget-object v0, v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;->this$1:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;

    iget-object v1, v1, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2;->this$0:Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;

    invoke-static {v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$400(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;)Lcom/here/android/mpa/guidance/VoiceCatalog;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity$2$1;->val$voicePackage:Lcom/here/android/mpa/guidance/VoicePackage;

    invoke-virtual {v2}, Lcom/here/android/mpa/guidance/VoicePackage;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/here/android/mpa/guidance/VoiceCatalog;->getLocalVoiceSkin(J)Lcom/here/android/mpa/guidance/VoiceSkin;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->access$502(Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;Lcom/here/android/mpa/guidance/VoiceSkin;)Lcom/here/android/mpa/guidance/VoiceSkin;

    .line 411
    :goto_0
    return-void

    .line 409
    :cond_0
    sget-object v0, Lcom/navdy/client/debug/navdebug/NavigationDemoActivity;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Download voice package failed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/here/android/mpa/guidance/VoiceCatalog$Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
