.class public final Lcom/navdy/client/app/tracking/SetDestinationTracker;
.super Ljava/lang/Object;
.source "SetDestinationTracker.java"


# static fields
.field private static final VERBOSE:Z

.field private static final instance:Lcom/navdy/client/app/tracking/SetDestinationTracker;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private destinationType:Ljava/lang/String;

.field private favoriteType:Ljava/lang/String;

.field private sourceValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/tracking/SetDestinationTracker;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->logger:Lcom/navdy/service/library/log/Logger;

    .line 23
    new-instance v0, Lcom/navdy/client/app/tracking/SetDestinationTracker;

    invoke-direct {v0}, Lcom/navdy/client/app/tracking/SetDestinationTracker;-><init>()V

    sput-object v0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->instance:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->destinationType:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->sourceValue:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->favoriteType:Ljava/lang/String;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/tracking/SetDestinationTracker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/tracking/SetDestinationTracker;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->favoriteType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/tracking/SetDestinationTracker;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/tracking/SetDestinationTracker;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->favoriteType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/tracking/SetDestinationTracker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/tracking/SetDestinationTracker;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->destinationType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/tracking/SetDestinationTracker;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/tracking/SetDestinationTracker;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->sourceValue:Ljava/lang/String;

    return-object v0
.end method

.method public static getInstance()Lcom/navdy/client/app/tracking/SetDestinationTracker;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->instance:Lcom/navdy/client/app/tracking/SetDestinationTracker;

    return-object v0
.end method


# virtual methods
.method public getDestinationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->destinationType:Ljava/lang/String;

    return-object v0
.end method

.method public getFavoriteType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->favoriteType:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->sourceValue:Ljava/lang/String;

    return-object v0
.end method

.method public resetValues()V
    .locals 1

    .prologue
    .line 66
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->destinationType:Ljava/lang/String;

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->sourceValue:Ljava/lang/String;

    .line 68
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->favoriteType:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setDestinationType(Ljava/lang/String;)V
    .locals 1
    .param p1, "destinationType"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iput-object p1, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->destinationType:Ljava/lang/String;

    .line 39
    :cond_0
    return-void
.end method

.method public setFavoriteType(Ljava/lang/String;)V
    .locals 1
    .param p1, "favoriteType"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    iput-object p1, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->favoriteType:Ljava/lang/String;

    .line 51
    :cond_0
    return-void
.end method

.method public setSourceValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "sourceValue"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    iput-object p1, p0, Lcom/navdy/client/app/tracking/SetDestinationTracker;->sourceValue:Ljava/lang/String;

    .line 45
    :cond_0
    return-void
.end method

.method public tagSetDestinationEvent(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 3
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 76
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/tracking/SetDestinationTracker$1;-><init>(Lcom/navdy/client/app/tracking/SetDestinationTracker;Lcom/navdy/client/app/framework/models/Destination;)V

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 107
    return-void
.end method
