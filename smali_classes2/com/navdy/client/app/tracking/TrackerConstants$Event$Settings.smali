.class public Lcom/navdy/client/app/tracking/TrackerConstants$Event$Settings;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Event;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Settings"
.end annotation


# static fields
.field public static final AUDIO_SETTINGS_CHANGED:Ljava/lang/String; = "Audio_Settings_Changed"

.field public static final DIAL_SETTINGS_CHANGED:Ljava/lang/String; = "Dial_Settings_Changed"

.field public static final NAVIGATION_SETTINGS_CHANGED:Ljava/lang/String; = "Navigation_Settings_Changed"

.field public static final OTA_SETTINGS_CHANGED:Ljava/lang/String; = "Ota_Settings_Changed"

.field public static final PROFILE_SETTINGS_CHANGED:Ljava/lang/String; = "Profile_Settings_Changed"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
