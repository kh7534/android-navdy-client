.class Lcom/navdy/client/app/ui/routing/ActiveTripActivity$3;
.super Ljava/lang/Object;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->northUpOrientation(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$3;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 180
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 2
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 176
    const/4 v0, 0x0

    sget-object v1, Lcom/here/android/mpa/mapping/Map$Animation;->BOW:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {p1, v0, v1}, Lcom/here/android/mpa/mapping/Map;->setOrientation(FLcom/here/android/mpa/mapping/Map$Animation;)V

    .line 177
    return-void
.end method
