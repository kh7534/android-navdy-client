.class Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$4;
.super Ljava/lang/Object;
.source "SearchRecyclerAdapter.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->clear()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    .prologue
    .line 990
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter$4;->this$0:Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 3
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 993
    if-eqz p1, :cond_0

    .line 995
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/maps/GoogleMap;->clear()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1000
    :cond_0
    :goto_0
    return-void

    .line 996
    :catch_0
    move-exception v0

    .line 997
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-static {}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Unable to clear the google map due to IllegalArgumentException."

    invoke-virtual {v1, v2, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
