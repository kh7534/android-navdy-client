.class Lcom/navdy/client/app/ui/glances/GlancesFragment$9;
.super Landroid/os/AsyncTask;
.source "GlancesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/glances/GlancesFragment;->setGlances(Ljava/lang/Boolean;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

.field final synthetic val$glancesAreEnabled:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/glances/GlancesFragment;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/glances/GlancesFragment;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->val$glancesAreEnabled:Ljava/lang/Boolean;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 335
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 338
    const-string v0, "glances"

    iget-object v1, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->val$glancesAreEnabled:Ljava/lang/Boolean;

    .line 339
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 338
    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->saveGlancesConfigurationChanges(Ljava/lang/String;Z)V

    .line 341
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$100(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    .line 343
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 335
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "aVoid"    # Ljava/lang/Void;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/navdy/client/app/ui/glances/GlancesFragment$9;->this$0:Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/glances/GlancesFragment;->access$1100(Lcom/navdy/client/app/ui/glances/GlancesFragment;)V

    .line 349
    return-void
.end method
