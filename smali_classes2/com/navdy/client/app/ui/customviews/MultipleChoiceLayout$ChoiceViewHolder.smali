.class Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MultipleChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ChoiceViewHolder"
.end annotation


# instance fields
.field highlightView:Landroid/view/View;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100107
    .end annotation
.end field

.field textView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10000d
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 67
    invoke-static {p0, p1}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 68
    return-void
.end method
