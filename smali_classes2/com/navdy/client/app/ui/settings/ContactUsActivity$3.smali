.class Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;
.super Ljava/lang/Object;
.source "ContactUsActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/customviews/CustomSpinner$OnSpinnerStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/ContactUsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

.field final synthetic val$introAdapter:Landroid/widget/ArrayAdapter;

.field final synthetic val$problemAdapter:Landroid/widget/ArrayAdapter;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/ContactUsActivity;Landroid/widget/ArrayAdapter;Landroid/widget/ArrayAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->val$problemAdapter:Landroid/widget/ArrayAdapter;

    iput-object p3, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->val$introAdapter:Landroid/widget/ArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public spinnerClosed()V
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$400(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSelectedItemPosition: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$500(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)V

    .line 162
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->val$introAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->getSelectedItemPosition()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 168
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$600(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Landroid/widget/EditText;

    move-result-object v0

    const v1, 0x7f080471

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$600(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Landroid/widget/EditText;

    move-result-object v0

    const v1, 0x7f080470

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0
.end method

.method public spinnerOpened()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-static {v0}, Lcom/navdy/client/app/ui/settings/ContactUsActivity;->access$300(Lcom/navdy/client/app/ui/settings/ContactUsActivity;)Lcom/navdy/client/app/ui/customviews/CustomSpinner;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/ContactUsActivity$3;->val$problemAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/CustomSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 156
    return-void
.end method
