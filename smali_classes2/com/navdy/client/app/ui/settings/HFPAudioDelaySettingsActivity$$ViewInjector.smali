.class public Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "HFPAudioDelaySettingsActivity$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f100293

    const-string v2, "field \'btnPlaySequence\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Button;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->btnPlaySequence:Landroid/widget/Button;

    .line 12
    const v1, 0x7f100292

    const-string v2, "field \'multipleChoiceLayout\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->multipleChoiceLayout:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .line 14
    return-void
.end method

.method public static reset(Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->btnPlaySequence:Landroid/widget/Button;

    .line 18
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/HFPAudioDelaySettingsActivity;->multipleChoiceLayout:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .line 19
    return-void
.end method
