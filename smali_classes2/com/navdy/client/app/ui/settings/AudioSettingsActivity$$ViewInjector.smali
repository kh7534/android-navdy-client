.class public Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector;
.super Ljava/lang/Object;
.source "AudioSettingsActivity$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f100380

    const-string v2, "field \'mWelcomeMessageSwitch\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Switch;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessageSwitch:Landroid/widget/Switch;

    .line 12
    const v1, 0x7f100381

    const-string v2, "field \'mSppedLimitWarnings\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Switch;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSppedLimitWarnings:Landroid/widget/Switch;

    .line 14
    const v1, 0x7f100382

    const-string v2, "field \'mCameraWarnings\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Switch;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarnings:Landroid/widget/Switch;

    .line 16
    const v1, 0x7f10037f

    const-string v2, "field \'mTurnByTurnNavigation\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/Switch;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTurnByTurnNavigation:Landroid/widget/Switch;

    .line 18
    const v1, 0x7f100375

    const-string v2, "field \'mSpeaker\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "view":Landroid/view/View;
    move-object v1, v0

    .line 19
    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeaker:Landroid/widget/RadioButton;

    .line 20
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$1;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    const v1, 0x7f100374

    const-string v2, "field \'mBluetooth\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 29
    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mBluetooth:Landroid/widget/RadioButton;

    .line 30
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$2;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$2;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    const v1, 0x7f100373

    const-string v2, "field \'mSmartBluetooth\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 39
    check-cast v0, Landroid/widget/RadioButton;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSmartBluetooth:Landroid/widget/RadioButton;

    .line 40
    const v1, 0x7f10037a

    const-string v2, "field \'mTxtVoiceName\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTxtVoiceName:Landroid/widget/TextView;

    .line 42
    const v1, 0x7f100378

    const-string v2, "field \'mVoicePreference\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 43
    .restart local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mVoicePreference:Landroid/view/View;

    .line 44
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$3;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$3;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    const v1, 0x7f10037b

    const-string v2, "field \'mSpeechDelayPreference\' and method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 53
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeechDelayPreference:Landroid/view/View;

    .line 54
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$4;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$4;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v1, 0x7f10037d

    const-string v2, "field \'tvSpeechLevel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 63
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->tvSpeechLevel:Landroid/widget/TextView;

    .line 64
    const v1, 0x7f100370

    const-string v2, "field \'mMainDescription\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 65
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mMainDescription:Landroid/widget/TextView;

    .line 66
    const v1, 0x7f100377

    const-string v2, "field \'speechLevelSettings\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 67
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->speechLevelSettings:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .line 68
    const v1, 0x7f100372

    const-string v2, "method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 69
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$5;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$5;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    const v1, 0x7f100371

    const-string v2, "method \'onClick\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$6;

    invoke-direct {v1, p1}, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity$$ViewInjector$6;-><init>(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    return-void
.end method

.method public static reset(Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;

    .prologue
    const/4 v0, 0x0

    .line 89
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mWelcomeMessageSwitch:Landroid/widget/Switch;

    .line 90
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSppedLimitWarnings:Landroid/widget/Switch;

    .line 91
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mCameraWarnings:Landroid/widget/Switch;

    .line 92
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTurnByTurnNavigation:Landroid/widget/Switch;

    .line 93
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeaker:Landroid/widget/RadioButton;

    .line 94
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mBluetooth:Landroid/widget/RadioButton;

    .line 95
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSmartBluetooth:Landroid/widget/RadioButton;

    .line 96
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mTxtVoiceName:Landroid/widget/TextView;

    .line 97
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mVoicePreference:Landroid/view/View;

    .line 98
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mSpeechDelayPreference:Landroid/view/View;

    .line 99
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->tvSpeechLevel:Landroid/widget/TextView;

    .line 100
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->mMainDescription:Landroid/widget/TextView;

    .line 101
    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/AudioSettingsActivity;->speechLevelSettings:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .line 102
    return-void
.end method
