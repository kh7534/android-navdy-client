.class Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$2;
.super Ljava/lang/Object;
.source "MicPermissionDialogActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->onButtonClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$2;->this$0:Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 32
    new-instance v2, Landroid/content/Intent;

    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$2;->this$0:Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;

    invoke-virtual {v4}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/navdy/client/app/ui/homescreen/PermissionFailedActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .local v2, "intent":Landroid/content/Intent;
    const v3, 0x7f0801bf

    .line 36
    .local v3, "titleRes":I
    const v1, 0x7f0801bd

    .line 37
    .local v1, "descriptionRes":I
    const v0, 0x7f0801bb

    .line 39
    .local v0, "buttonTextRes":I
    const-string v4, "extra_title_res"

    const v5, 0x7f0801bf

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 40
    const-string v4, "extra_description_res"

    const v5, 0x7f0801bd

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 41
    const-string v4, "extra_button_res"

    const v5, 0x7f0801bb

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 43
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$2;->this$0:Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;

    invoke-virtual {v4, v2}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 44
    iget-object v4, p0, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity$2;->this$0:Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;

    invoke-virtual {v4}, Lcom/navdy/client/app/ui/homescreen/MicPermissionDialogActivity;->finish()V

    .line 45
    return-void
.end method
