.class public Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "DemoVideoDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCloseClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;->onBackPressed()V

    .line 42
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f03004a

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;->setContentView(I)V

    .line 26
    return-void
.end method

.method public onWatchVideoClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 29
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 30
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "user_watched_the_demo"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 32
    const v4, 0x7f080123

    invoke-virtual {p0, v4}, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "videoUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 34
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "video_url"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 37
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/DemoVideoDialogActivity;->finish()V

    .line 38
    return-void
.end method
