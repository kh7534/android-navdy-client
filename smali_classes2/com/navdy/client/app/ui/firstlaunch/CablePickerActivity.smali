.class public Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "CablePickerActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->finish()V

    .line 65
    return-void
.end method

.method public onClaClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 54
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "power_cable_selection"

    const-string v4, "CLA"

    .line 55
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 57
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 59
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/InstallClaActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 61
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v0, 0x7f03006a

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->setContentView(I)V

    .line 27
    const v0, 0x7f1000b1

    const v1, 0x7f0201ef

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->loadImage(II)V

    .line 28
    const v0, 0x7f100189

    const v1, 0x7f020210

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->loadImage(II)V

    .line 29
    const v0, 0x7f10018c

    const v1, 0x7f0201da

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->loadImage(II)V

    .line 30
    return-void
.end method

.method public onObd2Click(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 42
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "power_cable_selection"

    const-string v4, "OBD"

    .line 43
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 45
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 47
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_step"

    const v3, 0x7f030071

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 49
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 50
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 35
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/CablePickerActivity;->hideSystemUI()V

    .line 37
    const-string v0, "Installation_Plugging_In"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 38
    return-void
.end method
