.class public Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "PhonePermissionActivity.java"


# instance fields
.field appSetupScreen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

.field private bottomCard:Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

.field private hud:Landroid/widget/ImageView;

.field private showingFail:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 27
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->getScreen(I)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->appSetupScreen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->showingFail:Z

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->goToHomeScreen()V

    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->showFailureForCurrentScreen()V

    return-void
.end method

.method private goToHomeScreen()V
    .locals 3

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .local v0, "i":Landroid/content/Intent;
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 67
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->startActivity(Landroid/content/Intent;)V

    .line 68
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->finish()V

    .line 69
    return-void
.end method

.method private showFailureForCurrentScreen()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "showFailureForCurrentScreen"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->bottomCard:Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->showingFail:Z

    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->bottomCard:Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->showFail()V

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->hud:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->hud:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->appSetupScreen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v1, v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudResFail:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 80
    :cond_1
    return-void
.end method


# virtual methods
.method public onButtonClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->appSetupScreen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->isEnding(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->appSetupScreen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget-boolean v0, v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->showingFail:Z

    if-nez v0, :cond_3

    .line 119
    :cond_2
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;)V

    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->requestPhonePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 131
    :cond_3
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->goToHomeScreen()V

    goto :goto_0
.end method

.method public onContactSupportClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/settings/ContactUsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->startActivity(Landroid/content/Intent;)V

    .line 97
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v3, 0x7f030067

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->setContentView(I)V

    .line 39
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v4, 0x7f0800be

    .line 40
    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v3

    .line 41
    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 44
    const v3, 0x7f100168

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->hud:Landroid/widget/ImageView;

    .line 46
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->hud:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 47
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->hud:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->appSetupScreen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    iget v4, v4, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudRes:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 50
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 51
    .local v1, "fm":Landroid/support/v4/app/FragmentManager;
    if-eqz v1, :cond_1

    .line 52
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 53
    .local v2, "ft":Landroid/support/v4/app/FragmentTransaction;
    if-eqz v2, :cond_1

    .line 54
    new-instance v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    invoke-direct {v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;-><init>()V

    iput-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->bottomCard:Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    .line 55
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 56
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "screen"

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->appSetupScreen:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 57
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->bottomCard:Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    invoke-virtual {v3, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->setArguments(Landroid/os/Bundle;)V

    .line 58
    const v3, 0x7f10016b

    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->bottomCard:Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 59
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 62
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_1
    return-void
.end method

.method public onDescriptionClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->showingFail:Z

    if-eqz v0, :cond_0

    .line 101
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/SystemUtils;->goToSystemSettingsAppInfoForOurApp(Landroid/app/Activity;)V

    .line 103
    :cond_0
    return-void
.end method

.method public onHelpCenterClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 106
    invoke-static {}, Lcom/navdy/client/app/framework/util/ZendeskJWT;->getZendeskUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->openBrowserFor(Landroid/net/Uri;)V

    .line 107
    return-void
.end method

.method public onPrivacyPolicyClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 89
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 90
    .local v0, "browserIntent":Landroid/content/Intent;
    const-string v1, "type"

    const-string v2, "Privacy"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/PhonePermissionActivity;->startActivity(Landroid/content/Intent;)V

    .line 92
    return-void
.end method
