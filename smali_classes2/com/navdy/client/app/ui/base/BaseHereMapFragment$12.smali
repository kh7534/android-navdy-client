.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentReady;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->setUsableArea(IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field final synthetic val$paddingBottom:I

.field final synthetic val$paddingLeft:I

.field final synthetic val$paddingRight:I

.field final synthetic val$paddingTop:I


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;IIII)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 413
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingTop:I

    iput p3, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingBottom:I

    iput p4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingLeft:I

    iput p5, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingRight:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 450
    return-void
.end method

.method public onReady(Lcom/here/android/mpa/mapping/Map;)V
    .locals 11
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 416
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/Map;->getWidth()I

    move-result v3

    .line 417
    .local v3, "width":I
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/Map;->getHeight()I

    move-result v0

    .line 428
    .local v0, "height":I
    iget v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingTop:I

    iget v5, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingBottom:I

    add-int/2addr v4, v5

    if-ge v4, v0, :cond_0

    iget v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingLeft:I

    iget v5, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingRight:I

    add-int/2addr v4, v5

    if-lt v4, v3, :cond_1

    .line 429
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalid paddings. width="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " height="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " paddingTop="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingTop:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " paddingBottom="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingBottom:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " paddingLeft="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingLeft:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " paddingRight="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingRight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 447
    :goto_0
    return-void

    .line 439
    :cond_1
    invoke-virtual {p1}, Lcom/here/android/mpa/mapping/Map;->getTransformCenter()Landroid/graphics/PointF;

    move-result-object v2

    .line 440
    .local v2, "transformCenter":Landroid/graphics/PointF;
    new-instance v1, Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingLeft:I

    int-to-double v6, v5

    div-double/2addr v6, v8

    double-to-int v5, v6

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget v5, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingRight:I

    int-to-double v6, v5

    div-double/2addr v6, v8

    double-to-int v5, v6

    int-to-float v5, v5

    sub-float/2addr v4, v5

    iget v5, v2, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingTop:I

    int-to-double v6, v6

    div-double/2addr v6, v8

    double-to-int v6, v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    iget v6, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingBottom:I

    int-to-double v6, v6

    div-double/2addr v6, v8

    double-to-int v6, v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-direct {v1, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 444
    .local v1, "newTransformCenter":Landroid/graphics/PointF;
    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->setTransformCenter(Landroid/graphics/PointF;)Lcom/here/android/mpa/mapping/Map;

    .line 445
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    new-instance v5, Lcom/here/android/mpa/common/ViewRect;

    iget v6, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingLeft:I

    iget v7, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingTop:I

    iget v8, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingLeft:I

    sub-int v8, v3, v8

    iget v9, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingRight:I

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingTop:I

    sub-int v9, v0, v9

    iget v10, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->val$paddingBottom:I

    sub-int/2addr v9, v10

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/here/android/mpa/common/ViewRect;-><init>(IIII)V

    invoke-static {v4, v5}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1702(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/common/ViewRect;)Lcom/here/android/mpa/common/ViewRect;

    .line 446
    iget-object v4, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$12;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-static {v4, v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$1802(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    goto :goto_0
.end method
