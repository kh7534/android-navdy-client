.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

.field final synthetic val$container:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;Landroid/view/View;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->val$container:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 14

    .prologue
    .line 209
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->val$container:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v7

    .line 210
    .local v7, "height":I
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->val$container:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v9

    .line 212
    .local v9, "width":I
    if-eqz v7, :cond_6

    if-eqz v9, :cond_6

    .line 213
    const/4 v8, 0x0

    .line 217
    .local v8, "padding":I
    const-wide v10, 0x3fc3333333333333L    # 0.15

    int-to-double v12, v7

    mul-double/2addr v10, v12

    double-to-int v3, v10

    .line 219
    .local v3, "additionalTopDownPadding":I
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v10, v10, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v10}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$500(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v10

    sub-int v10, v9, v10

    iget-object v11, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v11, v11, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v11}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$600(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v11

    sub-int/2addr v10, v11

    mul-int/lit8 v10, v10, 0x2

    if-ge v3, v10, :cond_1

    const/4 v4, 0x1

    .line 220
    .local v4, "additionalTopDownPaddingFitsSides":Z
    :goto_0
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v10, v10, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v10}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$700(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v10

    sub-int v10, v7, v10

    iget-object v11, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v11, v11, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v11}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$800(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v11

    sub-int/2addr v10, v11

    mul-int/lit8 v10, v10, 0x2

    if-ge v3, v10, :cond_2

    const/4 v5, 0x1

    .line 222
    .local v5, "additionalTopDownPaddingFitsTopDown":Z
    :goto_1
    if-eqz v5, :cond_3

    if-eqz v4, :cond_3

    .line 223
    move v8, v3

    .line 235
    :cond_0
    :goto_2
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v10, v10, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->val$latLngBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-static {v10, v8}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v6

    .line 236
    .local v6, "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v10, v10, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iget-object v11, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-boolean v11, v11, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->val$animate:Z

    invoke-static {v10, v6, v11}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$900(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/google/android/gms/maps/CameraUpdate;Z)V

    .line 240
    .end local v3    # "additionalTopDownPadding":I
    .end local v4    # "additionalTopDownPaddingFitsSides":Z
    .end local v5    # "additionalTopDownPaddingFitsTopDown":Z
    .end local v6    # "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    .end local v8    # "padding":I
    :goto_3
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->val$container:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v10

    invoke-virtual {v10, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 241
    return-void

    .line 219
    .restart local v3    # "additionalTopDownPadding":I
    .restart local v8    # "padding":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 220
    .restart local v4    # "additionalTopDownPaddingFitsSides":Z
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 225
    .restart local v5    # "additionalTopDownPaddingFitsTopDown":Z
    :cond_3
    const-wide v10, 0x3fb999999999999aL    # 0.1

    int-to-double v12, v9

    mul-double/2addr v10, v12

    double-to-int v0, v10

    .line 227
    .local v0, "additionalSidePadding":I
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v10, v10, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v10}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$500(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v10

    sub-int v10, v9, v10

    iget-object v11, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v11, v11, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v11}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$600(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v11

    sub-int/2addr v10, v11

    mul-int/lit8 v10, v10, 0x2

    if-ge v0, v10, :cond_4

    const/4 v1, 0x1

    .line 228
    .local v1, "additionalSidePaddingFitsSides":Z
    :goto_4
    iget-object v10, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v10, v10, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v10}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$700(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v10

    sub-int v10, v7, v10

    iget-object v11, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;->this$1:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;

    iget-object v11, v11, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v11}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$800(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)I

    move-result v11

    sub-int/2addr v10, v11

    mul-int/lit8 v10, v10, 0x2

    if-ge v0, v10, :cond_5

    const/4 v2, 0x1

    .line 230
    .local v2, "additionalSidePaddingFitsTopDown":Z
    :goto_5
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 231
    move v8, v0

    goto :goto_2

    .line 227
    .end local v1    # "additionalSidePaddingFitsSides":Z
    .end local v2    # "additionalSidePaddingFitsTopDown":Z
    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    .line 228
    .restart local v1    # "additionalSidePaddingFitsSides":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    .line 238
    .end local v0    # "additionalSidePadding":I
    .end local v1    # "additionalSidePaddingFitsSides":Z
    .end local v3    # "additionalTopDownPadding":I
    .end local v4    # "additionalTopDownPaddingFitsSides":Z
    .end local v5    # "additionalTopDownPaddingFitsTopDown":Z
    .end local v8    # "padding":I
    :cond_6
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v10

    const-string v11, "zoomTo, googleMap has no dimensions"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_3
.end method
