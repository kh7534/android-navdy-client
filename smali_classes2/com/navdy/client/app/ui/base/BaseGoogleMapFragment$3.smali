.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->zoomTo(Lcom/google/android/gms/maps/model/LatLngBounds;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field final synthetic val$animate:Z

.field final synthetic val$latLngBounds:Lcom/google/android/gms/maps/model/LatLngBounds;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/google/android/gms/maps/model/LatLngBounds;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->val$latLngBounds:Lcom/google/android/gms/maps/model/LatLngBounds;

    iput-boolean p3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->val$animate:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 4
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 201
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "zoomTo, creating new CameraUpdate, map ready? "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 205
    .local v0, "container":Landroid/view/View;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;

    invoke-direct {v2, p0, v0}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3$1;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$3;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 246
    :goto_1
    return-void

    .line 201
    .end local v0    # "container":Landroid/view/View;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 244
    .restart local v0    # "container":Landroid/view/View;
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "zoomTo, view is null or its viewtreeobserver not alive"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_1
.end method
