.class Lcom/navdy/client/app/ui/base/BaseActivity$7;
.super Ljava/lang/Object;
.source "BaseActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseActivity;->requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

.field final synthetic val$doThingsThatRequireLocationPermission:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseActivity;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseActivity;

    .prologue
    .line 718
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseActivity$7;->this$0:Lcom/navdy/client/app/ui/base/BaseActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseActivity$7;->val$doThingsThatRequireLocationPermission:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 721
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->initLocationServices()V

    .line 722
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity$7;->val$doThingsThatRequireLocationPermission:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 723
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseActivity$7;->val$doThingsThatRequireLocationPermission:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 725
    :cond_0
    return-void
.end method
