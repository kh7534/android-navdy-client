.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->updateCarMarker(Lcom/navdy/service/library/events/location/Coordinate;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

.field final synthetic val$carLocation:Lcom/navdy/service/library/events/location/Coordinate;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 607
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->val$carLocation:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 8
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 610
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->val$carLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->val$carLocation:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v3, v3, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 612
    .local v1, "carLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1500(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 613
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1500(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    .line 615
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 616
    .local v0, "appContext":Landroid/content/Context;
    const v3, 0x7f0800f0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 618
    .local v2, "carLocationString":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    new-instance v4, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 619
    invoke-virtual {v4, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    .line 620
    invoke-virtual {v4, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    const v5, 0x7f020188

    .line 621
    invoke-static {v5}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    .line 618
    invoke-virtual {p1, v4}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1502(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;Lcom/google/android/gms/maps/model/Marker;)Lcom/google/android/gms/maps/model/Marker;

    .line 624
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$000(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 625
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$10;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->centerOnLastKnownLocation(Z)V

    .line 627
    :cond_1
    return-void
.end method
