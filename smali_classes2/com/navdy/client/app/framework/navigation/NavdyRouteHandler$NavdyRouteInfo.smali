.class public Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
.super Ljava/lang/Object;
.source "NavdyRouteHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NavdyRouteInfo"
.end annotation


# static fields
.field public static final HEAVY_TRAFFIC_THRESHOLD:D = 1.25


# instance fields
.field private cancelHandle:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final destination:Lcom/navdy/client/app/framework/models/Destination;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation
.end field

.field private distanceToDestination:I

.field public final isTrafficHeavy:Z

.field private progress:Lcom/here/android/mpa/common/GeoPolyline;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field final requestId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private final route:Lcom/here/android/mpa/common/GeoPolyline;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field final routeId:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private timeToDestination:I

.field public final via:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Lcom/here/android/mpa/common/GeoPolyline;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 6
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "via"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "route"    # Lcom/here/android/mpa/common/GeoPolyline;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "routeId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p6, "duration"    # I
    .param p7, "durationWithTraffic"    # I
    .param p8, "distanceToDestination"    # I

    .prologue
    const/4 v0, 0x0

    .line 1003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1004
    iput-object p2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    .line 1005
    iput-object p4, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    .line 1006
    iput-object p5, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    .line 1008
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 1009
    iput-object p3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    .line 1011
    iput p8, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    .line 1013
    const/4 v1, -0x1

    if-eq p7, v1, :cond_1

    .line 1014
    iput p7, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    .line 1015
    int-to-double v2, p7

    int-to-double v4, p6

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x3ff4000000000000L    # 1.25

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    .line 1020
    :goto_0
    return-void

    .line 1017
    :cond_1
    iput p6, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    .line 1018
    iput-boolean v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    goto :goto_0
.end method

.method private constructor <init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 1
    .param p1, "navdyRouteInfo"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 1022
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1023
    iget-object v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    .line 1024
    iget-object v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    .line 1025
    iget-object v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    .line 1026
    iget-boolean v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    .line 1027
    iget-object v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 1028
    iget-object v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    .line 1029
    iget-object v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    .line 1030
    iget-object v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    .line 1031
    iget v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    iput v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    .line 1032
    iget v0, p1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    iput v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    .line 1033
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .param p2, "x1"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$1;

    .prologue
    .line 973
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 973
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 973
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 973
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .param p1, "x1"    # I

    .prologue
    .line 973
    iput p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    return p1
.end method

.method static synthetic access$2102(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .param p1, "x1"    # I

    .prologue
    .line 973
    iput p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    return p1
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 973
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    return-object v0
.end method

.method static synthetic access$402(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/common/GeoPolyline;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .param p1, "x1"    # Lcom/here/android/mpa/common/GeoPolyline;

    .prologue
    .line 973
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1064
    if-ne p0, p1, :cond_1

    .line 1078
    :cond_0
    :goto_0
    return v1

    .line 1065
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 1067
    check-cast v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 1069
    .local v0, "that":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    iget-boolean v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    iget-boolean v4, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    iget v4, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    iget v4, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    iget-object v2, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    .line 1072
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_4
    iget-object v3, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    iget-object v2, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    .line 1073
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_5
    iget-object v3, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    if-eqz v3, :cond_6

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    iget-object v2, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    .line 1074
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_6
    iget-object v3, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    .line 1075
    invoke-virtual {v3, v4}, Lcom/navdy/client/app/framework/models/Destination;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    if-eqz v3, :cond_7

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    iget-object v2, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    .line 1076
    invoke-virtual {v1, v2}, Lcom/here/android/mpa/common/GeoPolyline;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_7
    iget-object v3, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    if-eqz v3, :cond_8

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    iget-object v2, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    .line 1077
    invoke-virtual {v1, v2}, Lcom/here/android/mpa/common/GeoPolyline;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    :cond_8
    iget-object v3, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    if-eqz v3, :cond_9

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    iget-object v2, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    .line 1078
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/16 :goto_0

    :cond_9
    iget-object v3, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    goto/16 :goto_0
.end method

.method public getDestination()Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method public getDistanceToDestination()I
    .locals 1

    .prologue
    .line 1059
    iget v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    return v0
.end method

.method public getProgress()Lcom/here/android/mpa/common/GeoPolyline;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1047
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    return-object v0
.end method

.method public getRoute()Lcom/here/android/mpa/common/GeoPolyline;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1042
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    return-object v0
.end method

.method public getTimeToDestination()I
    .locals 1

    .prologue
    .line 1055
    iget v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1084
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->via:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1085
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 1086
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 1087
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->isTrafficHeavy:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_3
    add-int v0, v3, v2

    .line 1088
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/Destination;->hashCode()I

    move-result v3

    add-int v0, v2, v3

    .line 1089
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->route:Lcom/here/android/mpa/common/GeoPolyline;

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoPolyline;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 1090
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->progress:Lcom/here/android/mpa/common/GeoPolyline;

    invoke-virtual {v2}, Lcom/here/android/mpa/common/GeoPolyline;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 1091
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 1092
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->timeToDestination:I

    add-int v0, v1, v2

    .line 1093
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->distanceToDestination:I

    add-int v0, v1, v2

    .line 1094
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 1084
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 1085
    goto :goto_1

    :cond_3
    move v2, v1

    .line 1086
    goto :goto_2

    :cond_4
    move v2, v1

    .line 1087
    goto :goto_3

    :cond_5
    move v2, v1

    .line 1089
    goto :goto_4

    :cond_6
    move v2, v1

    .line 1090
    goto :goto_5
.end method

.method public setHandle(Ljava/lang/String;)V
    .locals 0
    .param p1, "cancelHandle"    # Ljava/lang/String;

    .prologue
    .line 1051
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->cancelHandle:Ljava/lang/String;

    .line 1052
    return-void
.end method
