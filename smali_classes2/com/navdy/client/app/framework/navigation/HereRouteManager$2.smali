.class Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;
.super Ljava/lang/Object;
.source "HereRouteManager.java"

# interfaces
.implements Lcom/here/android/mpa/routing/CoreRouter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/navigation/HereRouteManager;->calculateRouteWithCoords(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

.field final synthetic val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/navigation/HereRouteManager;Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;->this$0:Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    iput-object p2, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;->val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onCalculateRouteFinished(Ljava/lang/Object;Ljava/lang/Enum;)V
    .locals 0

    .prologue
    .line 104
    check-cast p1, Ljava/util/List;

    check-cast p2, Lcom/here/android/mpa/routing/RoutingError;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;->onCalculateRouteFinished(Ljava/util/List;Lcom/here/android/mpa/routing/RoutingError;)V

    return-void
.end method

.method public onCalculateRouteFinished(Ljava/util/List;Lcom/here/android/mpa/routing/RoutingError;)V
    .locals 4
    .param p2, "routingError"    # Lcom/here/android/mpa/routing/RoutingError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/routing/RouteResult;",
            ">;",
            "Lcom/here/android/mpa/routing/RoutingError;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/routing/RouteResult;>;"
    const/4 v3, 0x0

    .line 107
    sget-object v0, Lcom/here/android/mpa/routing/RoutingError;->NONE:Lcom/here/android/mpa/routing/RoutingError;

    if-ne p2, v0, :cond_0

    if-eqz p1, :cond_0

    .line 108
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/routing/RouteResult;

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/RouteResult;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 113
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;->val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

    sget-object v2, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->NONE:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/here/android/mpa/routing/RouteResult;

    invoke-virtual {v0}, Lcom/here/android/mpa/routing/RouteResult;->getRoute()Lcom/here/android/mpa/routing/Route;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;->onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V

    .line 122
    :goto_0
    return-void

    .line 115
    :cond_0
    sget-object v0, Lcom/here/android/mpa/routing/RoutingError;->NO_END_POINT:Lcom/here/android/mpa/routing/RoutingError;

    if-ne p2, v0, :cond_1

    .line 116
    const-string v0, "No_End_Point"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;)V

    .line 119
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HereRouteManager Error: Here Internal Error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/HereRouteManager$2;->val$listener:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;->HERE_INTERNAL_ERROR:Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;->onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V

    goto :goto_0
.end method

.method public onProgress(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 125
    return-void
.end method
