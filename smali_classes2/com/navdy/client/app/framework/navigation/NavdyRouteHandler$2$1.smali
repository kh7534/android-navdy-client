.class Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;
.super Ljava/lang/Object;
.source "NavdyRouteHandler.java"

# interfaces
.implements Lcom/here/android/mpa/common/OnEngineInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEngineInitializationCompleted(Lcom/here/android/mpa/common/OnEngineInitListener$Error;)V
    .locals 14
    .param p1, "error"    # Lcom/here/android/mpa/common/OnEngineInitListener$Error;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$502(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 301
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$600(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isHudMapEngineReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$700(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    .line 303
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$800(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    .line 306
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "requestNewRoute, setting state to CALCULATING_ROUTES"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$002(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 310
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 311
    .local v4, "newRequestId":Ljava/lang/String;
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v9, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, -0x1

    invoke-direct/range {v0 .. v8}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Lcom/here/android/mpa/common/GeoPolyline;Ljava/lang/String;Ljava/lang/String;III)V

    invoke-static {v9, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$102(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 313
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    new-instance v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;

    invoke-direct {v1, p0, v4}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$1;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    .line 342
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    new-instance v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$2;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1100(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    .line 365
    .end local v4    # "newRequestId":Ljava/lang/String;
    :goto_0
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 352
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$900()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "requestNewRoute, setting state to CALCULATING_PENDING_ROUTE"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 353
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$002(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 354
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    new-instance v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v6, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v12, -0x1

    const/4 v13, -0x1

    invoke-direct/range {v5 .. v13}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Lcom/here/android/mpa/common/GeoPolyline;Ljava/lang/String;Ljava/lang/String;III)V

    invoke-static {v0, v5}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$102(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 356
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    new-instance v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1$3;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$1100(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    .line 363
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v0, v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->this$0:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2$1;->this$1:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->access$300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0
.end method
