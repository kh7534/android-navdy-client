.class Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;
.super Ljava/lang/Object;
.source "LocalMusicPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$000(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$100(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$100(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$100(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v1

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$202(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;I)I

    .line 56
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$000(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v1}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$200(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)I

    move-result v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v2}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$100(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;->onPositionUpdate(II)V

    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/framework/music/LocalMusicPlayer$1;->this$0:Lcom/navdy/client/app/framework/music/LocalMusicPlayer;

    invoke-static {v0}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$300(Lcom/navdy/client/app/framework/music/LocalMusicPlayer;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/music/LocalMusicPlayer;->access$400()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Media player isn\'t playing - did you release it without clearing callbacks?"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
