.class Lcom/navdy/client/app/framework/models/Destination$2;
.super Ljava/lang/Object;
.source "Destination.java"

# interfaces
.implements Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/models/Destination;->refreshPlaceIdDataAndUpdateListsAsync(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/models/Destination;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1653
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination$2;->this$0:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p2, p0, Lcom/navdy/client/app/framework/models/Destination$2;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
    .locals 4
    .param p2, "queryType"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;
    .param p3, "error"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;",
            ">;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;",
            "Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1658
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/models/Destination$2$1;

    invoke-direct {v1, p0, p3, p1}, Lcom/navdy/client/app/framework/models/Destination$2$1;-><init>(Lcom/navdy/client/app/framework/models/Destination$2;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;Ljava/util/List;)V

    const/4 v2, 0x1

    sget-object v3, Lcom/navdy/service/library/task/TaskManager$TaskPriority;->LOW:Lcom/navdy/service/library/task/TaskManager$TaskPriority;

    invoke-virtual {v0, v1, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;ILcom/navdy/service/library/task/TaskManager$TaskPriority;)Ljava/util/concurrent/Future;

    .line 1685
    return-void
.end method
