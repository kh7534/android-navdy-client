.class Lcom/navdy/client/app/framework/models/Destination$6;
.super Ljava/lang/Object;
.source "Destination.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/models/Destination;->updateAllColumnsOrInsertNewEntryInDbAsync(Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/models/Destination;

.field final synthetic val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 1941
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination$6;->this$0:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p2, p0, Lcom/navdy/client/app/framework/models/Destination$6;->val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1944
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$6;->this$0:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->updateAllColumnsOrInsertNewEntryInDb()Landroid/net/Uri;

    move-result-object v0

    .line 1945
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$6;->val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    if-eqz v1, :cond_0

    .line 1946
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination$6;->val$callback:Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v1, v0}, Lcom/navdy/client/app/providers/NavdyContentProvider$QueryResultCallback;->onQueryCompleted(ILandroid/net/Uri;)V

    .line 1948
    :cond_0
    return-void

    .line 1946
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
