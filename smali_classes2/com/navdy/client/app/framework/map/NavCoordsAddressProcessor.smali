.class public Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;,
        Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;,
        Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;
    }
.end annotation


# static fields
.field private static final MAX_DISTANCE_BETWEEN_COORDINATES_WITHOUT_STREET_NUMBER:F = 2000.0f

.field private static final MAX_DISTANCE_BETWEEN_COORDINATES_WITH_STREET_NUMBER:F = 1000.0f

.field private static final VERBOSE:Z

.field private static final bgHandlerThread:Landroid/os/HandlerThread;

.field private static final bgThreadHandler:Landroid/os/Handler;

.field private static final logger:Lcom/navdy/service/library/log/Logger;

.field private static final uiThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->uiThreadHandler:Landroid/os/Handler;

    .line 74
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "BgHandlerThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->bgHandlerThread:Landroid/os/HandlerThread;

    .line 75
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->bgHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 76
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->bgHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->bgThreadHandler:Landroid/os/Handler;

    .line 77
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->bgThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1100()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->uiThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "x1"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p2, "x2"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p3, "x3"    # Lcom/here/android/mpa/search/Address;
    .param p4, "x4"    # Lcom/navdy/client/app/framework/models/Destination$Precision;
    .param p5, "x5"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    .prologue
    .line 56
    invoke-static/range {p0 .. p5}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->saveToCacheAndDb(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "x2"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 56
    invoke-static {p0, p1, p2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callbackWithError(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->tryHerePlacesSearchNativeApiWithNameAndAddress(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    return-void
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->tryHerePlacesSearchNativeApiWithAddress(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->isInvalidOrExceedsThreshold(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callGoogleDirectionsWebApi(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    return-void
.end method

.method private static callGoogleDirectionsWebApi(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 1
    .param p0, "processedDestination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    .prologue
    .line 288
    new-instance v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;

    invoke-direct {v0, p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$4;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    invoke-static {p0, v0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->getCoordinatesFromGoogleDirectionsWebApi(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;)V

    .line 329
    return-void
.end method

.method public static callWebHereGeocoderApi(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 34
    .param p0, "address"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 338
    const-string v13, "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id="

    .line 339
    .local v13, "HERE_GEOCODER_ENDPOINT1":Ljava/lang/String;
    const-string v14, "&app_code="

    .line 340
    .local v14, "HERE_GEOCODER_ENDPOINT2":Ljava/lang/String;
    const-string v15, "&gen=9&searchtext="

    .line 341
    .local v15, "HERE_GEOCODER_ENDPOINT3":Ljava/lang/String;
    const-string v24, "Response"

    .line 342
    .local v24, "HERE_RESPONSE_STR":Ljava/lang/String;
    const-string v28, "View"

    .line 343
    .local v28, "HERE_VIEW_STR":Ljava/lang/String;
    const-string v25, "Result"

    .line 344
    .local v25, "HERE_RESULT_STR":Ljava/lang/String;
    const-string v20, "Location"

    .line 345
    .local v20, "HERE_LOCATION_STR":Ljava/lang/String;
    const-string v10, "DisplayPosition"

    .line 346
    .local v10, "HERE_DISPLAY_STR":Ljava/lang/String;
    const-string v22, "NavigationPosition"

    .line 347
    .local v22, "HERE_NAVIGATION_STR":Ljava/lang/String;
    const-string v19, "Latitude"

    .line 348
    .local v19, "HERE_LATITUDE_STR":Ljava/lang/String;
    const-string v21, "Longitude"

    .line 349
    .local v21, "HERE_LONGITUDE_STR":Ljava/lang/String;
    const-string v4, "Address"

    .line 350
    .local v4, "HERE_ADDRESS_STR":Ljava/lang/String;
    const-string v18, "Label"

    .line 351
    .local v18, "HERE_LABEL_STR":Ljava/lang/String;
    const-string v7, "Country"

    .line 352
    .local v7, "HERE_COUNTRY_STR":Ljava/lang/String;
    const-string v27, "State"

    .line 353
    .local v27, "HERE_STATE_STR":Ljava/lang/String;
    const-string v9, "County"

    .line 354
    .local v9, "HERE_COUNTY_STR":Ljava/lang/String;
    const-string v5, "City"

    .line 355
    .local v5, "HERE_CITY_STR":Ljava/lang/String;
    const-string v23, "PostalCode"

    .line 356
    .local v23, "HERE_POSTALCODE_STR":Ljava/lang/String;
    const-string v3, "AdditionalData"

    .line 357
    .local v3, "HERE_ADDNL_DATA_STR":Ljava/lang/String;
    const-string v6, "CountryName"

    .line 358
    .local v6, "HERE_COUNTRY_NAME_STR":Ljava/lang/String;
    const-string v8, "CountyName"

    .line 359
    .local v8, "HERE_COUNTY_NAME_STR":Ljava/lang/String;
    const-string v26, "StateName"

    .line 361
    .local v26, "HERE_STATE_NAME_STR":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    .line 362
    .local v29, "context":Landroid/content/Context;
    const v31, 0x7f080558

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 363
    .local v16, "HERE_GEO_APP_ID":Ljava/lang/String;
    const v31, 0x7f080559

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 365
    .local v17, "HERE_GEO_APP_TOKEN":Ljava/lang/String;
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&app_code="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&gen=9&searchtext="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 368
    .local v11, "HERE_GEOCODER_BASE_URL":Ljava/lang/String;
    const-string v12, "https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&gen=9&searchtext="

    .line 372
    .local v12, "HERE_GEOCODER_BASE_URL_4_LOGS":Ljava/lang/String;
    if-eqz v16, :cond_0

    if-nez v17, :cond_1

    .line 373
    :cond_0
    const-string v30, "no here token in manifest"

    .line 374
    .local v30, "str":Ljava/lang/String;
    sget-object v31, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 375
    sget-object v31, Lcom/here/android/mpa/search/ErrorCode;->INVALID_CREDENTIALS:Lcom/here/android/mpa/search/ErrorCode;

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    .line 471
    .end local v30    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 378
    :cond_1
    sget-object v31, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v32, "using here geocoder to get nav position"

    invoke-virtual/range {v31 .. v32}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 379
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v31

    new-instance v32, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v11, v1, v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    const/16 v33, 0x3

    invoke-virtual/range {v31 .. v33}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static callWebHerePlacesSearchApi(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 34
    .param p0, "address"    # Ljava/lang/String;
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    .prologue
    .line 480
    const-string v19, "https://places.cit.api.here.com/places/v1/discover/search?app_id="

    .line 481
    .local v19, "HERE_SEARCH_ENDPOINT1":Ljava/lang/String;
    const-string v20, "&app_code="

    .line 482
    .local v20, "HERE_SEARCH_ENDPOINT2":Ljava/lang/String;
    const-string v21, "&at="

    .line 483
    .local v21, "HERE_SEARCH_ENDPOINT3":Ljava/lang/String;
    const-string v22, "&q="

    .line 484
    .local v22, "HERE_SEARCH_ENDPOINT4":Ljava/lang/String;
    const-string v16, "results"

    .line 485
    .local v16, "HERE_RESULTS_STR":Ljava/lang/String;
    const-string v12, "items"

    .line 486
    .local v12, "HERE_ITEMS_STR":Ljava/lang/String;
    const-string v13, "location"

    .line 487
    .local v13, "HERE_LOCATION_STR":Ljava/lang/String;
    const-string v14, "position"

    .line 488
    .local v14, "HERE_POSITION_STR":Ljava/lang/String;
    const-string v26, "title"

    .line 489
    .local v26, "HERE_TITLE_STR":Ljava/lang/String;
    const-string v27, "vicinity"

    .line 490
    .local v27, "HERE_VICINITY_STR":Ljava/lang/String;
    const-string v11, "href"

    .line 491
    .local v11, "HERE_HREF_STR":Ljava/lang/String;
    const-string v5, "address"

    .line 492
    .local v5, "HERE_ADDRESS_STR":Ljava/lang/String;
    const-string v25, "text"

    .line 493
    .local v25, "HERE_TEXT_STR":Ljava/lang/String;
    const-string v10, "house"

    .line 494
    .local v10, "HERE_HOUSE_STR":Ljava/lang/String;
    const-string v24, "street"

    .line 495
    .local v24, "HERE_STREET_STR":Ljava/lang/String;
    const-string v6, "city"

    .line 496
    .local v6, "HERE_CITY_STR":Ljava/lang/String;
    const-string v23, "stateCode"

    .line 497
    .local v23, "HERE_STATE_STR":Ljava/lang/String;
    const-string v7, "country"

    .line 498
    .local v7, "HERE_COUNTRY_STR":Ljava/lang/String;
    const-string v15, "postalCode"

    .line 499
    .local v15, "HERE_POSTALCODE_STR":Ljava/lang/String;
    const-string v28, "view"

    .line 501
    .local v28, "HERE_VIEW_STR":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v29

    .line 502
    .local v29, "context":Landroid/content/Context;
    const v31, 0x7f080558

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 503
    .local v8, "HERE_GEO_APP_ID":Ljava/lang/String;
    const v31, 0x7f080559

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/navdy/client/app/framework/util/CredentialsUtils;->getCredentials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 505
    .local v9, "HERE_GEO_APP_TOKEN":Ljava/lang/String;
    new-instance v31, Ljava/lang/StringBuilder;

    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    const-string v32, "https://places.cit.api.here.com/places/v1/discover/search?app_id="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&app_code="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "&at="

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 508
    .local v17, "HERE_SEARCH_BASE_URL":Ljava/lang/String;
    const-string v18, "https://places.cit.api.here.com/places/v1/discover/search?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&at="

    .line 512
    .local v18, "HERE_SEARCH_BASE_URL_4_LOGS":Ljava/lang/String;
    if-eqz v8, :cond_0

    if-nez v9, :cond_1

    .line 513
    :cond_0
    const-string v30, "no here token in manifest"

    .line 514
    .local v30, "str":Ljava/lang/String;
    sget-object v31, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 515
    sget-object v31, Lcom/here/android/mpa/search/ErrorCode;->INVALID_CREDENTIALS:Lcom/here/android/mpa/search/ErrorCode;

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    .line 676
    .end local v30    # "str":Ljava/lang/String;
    :goto_0
    return-void

    .line 518
    :cond_1
    sget-object v31, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v32, "using here geocoder to get nav position"

    invoke-virtual/range {v31 .. v32}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 519
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v31

    new-instance v32, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;

    move-object/from16 v0, v32

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    move-object/from16 v3, v17

    move-object/from16 v4, v29

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$6;-><init>(Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    const/16 v33, 0x3

    invoke-virtual/range {v31 .. v33}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static callbackWithError(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 2
    .param p0, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 745
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->uiThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$8;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 753
    return-void
.end method

.method private static callbackWithSuccess(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 2
    .param p0, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 734
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->uiThreadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$7;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$7;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 742
    return-void
.end method

.method public static getCoordinatesFromGoogleDirectionsWebApi(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;)V
    .locals 2
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    .prologue
    .line 764
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 765
    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->NO_INTERNET:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;->onFailure(Ljava/lang/String;)V

    .line 769
    :cond_0
    new-instance v0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;

    invoke-direct {v1, p1, p0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;-><init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-direct {v0, v1}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;-><init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch$GoogleSearchListener;)V

    .line 849
    .local v0, "googleDirectionsSearch":Lcom/navdy/client/app/framework/search/GooglePlacesSearch;
    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runDirectionsSearchWebApi(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 850
    return-void
.end method

.method public static getDistanceThresholdFor(Lcom/navdy/client/app/framework/models/Destination;)D
    .locals 4
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 882
    const/high16 v0, 0x44fa0000    # 2000.0f

    .line 885
    .local v0, "coordinateThreshold":F
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->hasStreetNumber()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 889
    const/high16 v0, 0x447a0000    # 1000.0f

    .line 891
    :cond_0
    float-to-double v2, v0

    return-wide v2
.end method

.method private static isInvalidOrExceedsThreshold(Lcom/google/android/gms/maps/model/LatLng;Lcom/navdy/client/app/framework/models/Destination;)Z
    .locals 14
    .param p0, "latLng"    # Lcom/google/android/gms/maps/model/LatLng;
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 854
    invoke-static {p0}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/google/android/gms/maps/model/LatLng;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 855
    const/4 v0, 0x1

    .line 874
    :goto_0
    return v0

    .line 857
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v0

    if-nez v0, :cond_1

    .line 858
    const/4 v0, 0x0

    goto :goto_0

    .line 861
    :cond_1
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 862
    .local v8, "results":[F
    iget-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    iget-wide v4, p0, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-wide v6, p0, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 865
    const/4 v0, 0x0

    aget v0, v8, v0

    float-to-double v12, v0

    .line 867
    .local v12, "distance":D
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->getDistanceThresholdFor(Lcom/navdy/client/app/framework/models/Destination;)D

    move-result-wide v10

    .line 869
    .local v10, "coordinateThreshold":D
    cmpl-double v0, v12, v10

    if-lez v0, :cond_2

    .line 870
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Max Distance exceeded between display and navigation coordinates. Nav coords are: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Display coords are: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 872
    const/4 v0, 0x1

    goto :goto_0

    .line 874
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static processDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 3
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 89
    if-nez p0, :cond_0

    .line 90
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "processDestination, destination is null, calling back with error"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 91
    const/4 v0, 0x0

    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->INVALID_REQUEST:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-static {p1, v0, v1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callbackWithError(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    .line 181
    :goto_0
    return-void

    .line 95
    :cond_0
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$1;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private static saveToCacheAndDb(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 14
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "displayCoords"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "navigationCoords"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "address"    # Lcom/here/android/mpa/search/Address;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "precision"    # Lcom/navdy/client/app/framework/models/Destination$Precision;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p5, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 694
    if-nez p0, :cond_0

    .line 695
    sget-object v3, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "saveToCacheAndDb, destination is null, no-op"

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 696
    const/4 v3, 0x0

    sget-object v12, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->INVALID_REQUEST:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    move-object/from16 v0, p5

    invoke-static {v0, v3, v12}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callbackWithError(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    .line 731
    :goto_0
    return-void

    .line 700
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/AppInstance;->canReachInternet()Z

    move-result v3

    if-nez v3, :cond_1

    .line 701
    sget-object v3, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v12, "saveToCacheAndDb, no internet so not saving to cache, no-op"

    invoke-virtual {v3, v12}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 702
    sget-object v3, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->SERVICE_ERROR:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    move-object/from16 v0, p5

    invoke-static {v0, p0, v3}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callbackWithError(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    goto :goto_0

    .line 706
    :cond_1
    const-wide/16 v4, 0x0

    .line 707
    .local v4, "displayLat":D
    const-wide/16 v6, 0x0

    .line 708
    .local v6, "displayLng":D
    invoke-static {p1}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 709
    iget-object v3, p1, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 710
    iget-object v3, p1, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    .line 713
    :cond_2
    const-wide/16 v8, 0x0

    .line 714
    .local v8, "navigationLat":D
    const-wide/16 v10, 0x0

    .line 715
    .local v10, "navigationLng":D
    invoke-static/range {p2 .. p2}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(Lcom/navdy/service/library/events/location/Coordinate;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 716
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    .line 717
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    .line 720
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    .local v2, "originalAddress":Ljava/lang/String;
    move-object v3, p0

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    .line 722
    invoke-virtual/range {v3 .. v13}, Lcom/navdy/client/app/framework/models/Destination;->handleNewCoordsAndAddress(DDDDLcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    .line 729
    invoke-static {v2, p0}, Lcom/navdy/client/app/providers/NavdyContentProvider;->addToCacheIfNotAlreadyIn(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 730
    move-object/from16 v0, p5

    invoke-static {v0, p0}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callbackWithSuccess(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0
.end method

.method private static tryHerePlacesSearchNativeApiWithAddress(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 2
    .param p0, "processedDestination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$3;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder;->makeRequest(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    .line 285
    return-void
.end method

.method private static tryHerePlacesSearchNativeApiWithNameAndAddress(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V
    .locals 6
    .param p0, "processedDestination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p1, "callback"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;

    .prologue
    .line 191
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0802d6

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/navdy/client/app/framework/models/Destination;->rawAddressNotForDisplay:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "query":Ljava/lang/String;
    sget-object v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tryHerePlacesSearchNativeApiWithNameAndAddress, query is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 196
    new-instance v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$2;-><init>(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;)V

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/map/HereGeocoder;->makeRequest(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V

    .line 241
    return-void
.end method
