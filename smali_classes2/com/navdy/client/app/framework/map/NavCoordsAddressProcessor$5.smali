.class final Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->callWebHereGeocoderApi(Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$HERE_GEOCODER_BASE_URL:Ljava/lang/String;

.field final synthetic val$address:Ljava/lang/String;

.field final synthetic val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;)V
    .locals 0

    .prologue
    .line 379
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$HERE_GEOCODER_BASE_URL:Ljava/lang/String;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$address:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 40

    .prologue
    .line 382
    const/16 v17, 0x0

    .line 385
    .local v17, "inputStream":Ljava/io/InputStream;
    :try_start_0
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$HERE_GEOCODER_BASE_URL:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$address:Ljava/lang/String;

    move-object/from16 v34, v0

    const-string v35, "UTF-8"

    .line 386
    invoke-static/range {v34 .. v35}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    .line 388
    .local v31, "urlStr":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v33

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "Calling HERE with URL: https://geocoder.cit.api.here.com/6.2/geocode.json?app_id=HERE_GEO_APP_ID&app_code=HERE_GEO_APP_TOKEN&gen=9&searchtext="

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$address:Ljava/lang/String;

    move-object/from16 v35, v0

    const-string v36, "UTF-8"

    .line 389
    invoke-static/range {v35 .. v36}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 388
    invoke-virtual/range {v33 .. v34}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 391
    new-instance v29, Ljava/net/URL;

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 392
    .local v29, "url":Ljava/net/URL;
    invoke-virtual/range {v29 .. v29}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v30

    check-cast v30, Ljavax/net/ssl/HttpsURLConnection;

    .line 393
    .local v30, "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    invoke-virtual/range {v30 .. v30}, Ljavax/net/ssl/HttpsURLConnection;->getResponseCode()I

    move-result v15

    .line 395
    .local v15, "httpResponse":I
    const-wide/16 v34, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    .line 396
    .local v9, "dispLat":Ljava/lang/Double;
    const-wide/16 v34, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    .line 397
    .local v12, "dispLong":Ljava/lang/Double;
    const-wide/16 v34, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v20

    .line 398
    .local v20, "navLat":Ljava/lang/Double;
    const-wide/16 v34, 0x0

    invoke-static/range {v34 .. v35}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v23

    .line 399
    .local v23, "navLong":Ljava/lang/Double;
    new-instance v14, Lcom/here/android/mpa/search/Address;

    invoke-direct {v14}, Lcom/here/android/mpa/search/Address;-><init>()V

    .line 401
    .local v14, "hereAddress":Lcom/here/android/mpa/search/Address;
    const/16 v33, 0xc8

    move/from16 v0, v33

    if-lt v15, v0, :cond_3

    const/16 v33, 0x12c

    move/from16 v0, v33

    if-ge v15, v0, :cond_3

    .line 402
    invoke-virtual/range {v30 .. v30}, Ljavax/net/ssl/HttpsURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v17

    .line 403
    const-string v33, "UTF-8"

    move-object/from16 v0, v17

    move-object/from16 v1, v33

    invoke-static {v0, v1}, Lcom/navdy/service/library/util/IOUtils;->convertInputStreamToString(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 405
    .local v18, "jsonStr":Ljava/lang/String;
    new-instance v27, Lorg/json/JSONObject;

    move-object/from16 v0, v27

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 406
    .local v27, "root":Lorg/json/JSONObject;
    const-string v33, "Response"

    move-object/from16 v0, v27

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v25

    .line 407
    .local v25, "response":Lorg/json/JSONObject;
    const-string v33, "View"

    move-object/from16 v0, v25

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v33

    const/16 v34, 0x0

    .line 408
    invoke-virtual/range {v33 .. v34}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lorg/json/JSONObject;

    .line 409
    .local v32, "view":Lorg/json/JSONObject;
    const-string v33, "Result"

    invoke-virtual/range {v32 .. v33}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v33

    const/16 v34, 0x0

    .line 410
    invoke-virtual/range {v33 .. v34}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v26

    .line 411
    .local v26, "result":Lorg/json/JSONObject;
    const-string v33, "Location"

    move-object/from16 v0, v26

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v19

    .line 413
    .local v19, "location":Lorg/json/JSONObject;
    const-string v33, "DisplayPosition"

    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 414
    .local v13, "dispPos":Lorg/json/JSONObject;
    const-string v33, "Latitude"

    move-object/from16 v0, v33

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 415
    .local v10, "dispLatStr":Ljava/lang/String;
    const-string v33, "Longitude"

    move-object/from16 v0, v33

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 416
    .local v11, "dispLonStr":Ljava/lang/String;
    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    .line 417
    invoke-static {v11}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v12

    .line 419
    const-string v33, "NavigationPosition"

    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v33

    const/16 v34, 0x0

    .line 420
    invoke-virtual/range {v33 .. v34}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v24

    .line 421
    .local v24, "navPos":Lorg/json/JSONObject;
    const-string v33, "Latitude"

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 422
    .local v21, "navLatStr":Ljava/lang/String;
    const-string v33, "Longitude"

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 423
    .local v22, "navLonStr":Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v20

    .line 424
    invoke-static/range {v22 .. v22}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v23

    .line 426
    const-string v33, "Address"

    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 427
    .local v7, "address":Lorg/json/JSONObject;
    const-string v33, "Label"

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setText(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 428
    const-string v33, "Country"

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setCountryName(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 429
    const-string v33, "State"

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setState(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 430
    const-string v33, "County"

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setCounty(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 431
    const-string v33, "City"

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setCity(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 432
    const-string v33, "PostalCode"

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setPostalCode(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 434
    const-string v33, "AdditionalData"

    move-object/from16 v0, v33

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 435
    .local v6, "additionalData":Lorg/json/JSONArray;
    if-eqz v6, :cond_4

    .line 437
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v33

    move/from16 v0, v16

    move/from16 v1, v33

    if-ge v0, v1, :cond_4

    .line 438
    move/from16 v0, v16

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 439
    .local v8, "data":Lorg/json/JSONObject;
    const-string v33, "CountryName"

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_0

    .line 440
    const-string v33, "CountryName"

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setCountryName(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 442
    :cond_0
    const-string v33, "CountyName"

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_1

    .line 443
    const-string v33, "CountyName"

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setCounty(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 445
    :cond_1
    const-string v33, "StateName"

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v33

    if-eqz v33, :cond_2

    .line 446
    const-string v33, "StateName"

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v0, v33

    invoke-virtual {v14, v0}, Lcom/here/android/mpa/search/Address;->setState(Ljava/lang/String;)Lcom/here/android/mpa/search/Address;

    .line 437
    :cond_2
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 451
    .end local v6    # "additionalData":Lorg/json/JSONArray;
    .end local v7    # "address":Lorg/json/JSONObject;
    .end local v8    # "data":Lorg/json/JSONObject;
    .end local v10    # "dispLatStr":Ljava/lang/String;
    .end local v11    # "dispLonStr":Ljava/lang/String;
    .end local v13    # "dispPos":Lorg/json/JSONObject;
    .end local v16    # "i":I
    .end local v18    # "jsonStr":Ljava/lang/String;
    .end local v19    # "location":Lorg/json/JSONObject;
    .end local v21    # "navLatStr":Ljava/lang/String;
    .end local v22    # "navLonStr":Ljava/lang/String;
    .end local v24    # "navPos":Lorg/json/JSONObject;
    .end local v25    # "response":Lorg/json/JSONObject;
    .end local v26    # "result":Lorg/json/JSONObject;
    .end local v27    # "root":Lorg/json/JSONObject;
    .end local v32    # "view":Lorg/json/JSONObject;
    :cond_3
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v33

    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    const-string v35, "callWebHereGeocoderApi: response code:"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-virtual/range {v33 .. v34}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 452
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    move-object/from16 v33, v0

    sget-object v34, Lcom/here/android/mpa/search/ErrorCode;->BAD_REQUEST:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface/range {v33 .. v34}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V

    .line 455
    :cond_4
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v34

    const-wide/16 v36, 0x0

    cmpl-double v33, v34, v36

    if-eqz v33, :cond_5

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v34

    const-wide/16 v36, 0x0

    cmpl-double v33, v34, v36

    if-eqz v33, :cond_5

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    move-object/from16 v33, v0

    new-instance v34, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v36

    invoke-virtual {v12}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v38

    move-object/from16 v0, v34

    move-wide/from16 v1, v36

    move-wide/from16 v3, v38

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    new-instance v35, Lcom/here/android/mpa/common/GeoCoordinate;

    .line 457
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v36

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v38

    invoke-direct/range {v35 .. v39}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    sget-object v36, Lcom/navdy/client/app/framework/models/Destination$Precision;->UNKNOWN:Lcom/navdy/client/app/framework/models/Destination$Precision;

    .line 456
    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v35

    move-object/from16 v3, v36

    invoke-interface {v0, v1, v2, v14, v3}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onComplete(Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/common/GeoCoordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    :goto_1
    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 469
    .end local v9    # "dispLat":Ljava/lang/Double;
    .end local v12    # "dispLong":Ljava/lang/Double;
    .end local v14    # "hereAddress":Lcom/here/android/mpa/search/Address;
    .end local v15    # "httpResponse":I
    .end local v20    # "navLat":Ljava/lang/Double;
    .end local v23    # "navLong":Ljava/lang/Double;
    .end local v29    # "url":Ljava/net/URL;
    .end local v30    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v31    # "urlStr":Ljava/lang/String;
    :goto_2
    return-void

    .line 461
    .restart local v9    # "dispLat":Ljava/lang/Double;
    .restart local v12    # "dispLong":Ljava/lang/Double;
    .restart local v14    # "hereAddress":Lcom/here/android/mpa/search/Address;
    .restart local v15    # "httpResponse":I
    .restart local v20    # "navLat":Ljava/lang/Double;
    .restart local v23    # "navLong":Ljava/lang/Double;
    .restart local v29    # "url":Ljava/net/URL;
    .restart local v30    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .restart local v31    # "urlStr":Ljava/lang/String;
    :cond_5
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    move-object/from16 v33, v0

    sget-object v34, Lcom/here/android/mpa/search/ErrorCode;->BAD_LOCATION:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface/range {v33 .. v34}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 463
    .end local v9    # "dispLat":Ljava/lang/Double;
    .end local v12    # "dispLong":Ljava/lang/Double;
    .end local v14    # "hereAddress":Lcom/here/android/mpa/search/Address;
    .end local v15    # "httpResponse":I
    .end local v20    # "navLat":Ljava/lang/Double;
    .end local v23    # "navLong":Ljava/lang/Double;
    .end local v29    # "url":Ljava/net/URL;
    .end local v30    # "urlConnection":Ljavax/net/ssl/HttpsURLConnection;
    .end local v31    # "urlStr":Ljava/lang/String;
    :catch_0
    move-exception v28

    .line 464
    .local v28, "t":Ljava/lang/Throwable;
    :try_start_2
    invoke-static {}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v33

    const-string v34, "getNavigationCoordinate"

    move-object/from16 v0, v33

    move-object/from16 v1, v34

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 465
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$5;->val$callback:Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;

    move-object/from16 v33, v0

    sget-object v34, Lcom/here/android/mpa/search/ErrorCode;->SERVER_INTERNAL:Lcom/here/android/mpa/search/ErrorCode;

    invoke-interface/range {v33 .. v34}, Lcom/navdy/client/app/framework/map/HereGeocoder$OnGeocodeCompleteCallback;->onError(Lcom/here/android/mpa/search/ErrorCode;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 467
    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto :goto_2

    .end local v28    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v33

    invoke-static/range {v17 .. v17}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v33
.end method
