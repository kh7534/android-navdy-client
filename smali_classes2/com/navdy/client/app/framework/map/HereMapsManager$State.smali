.class final enum Lcom/navdy/client/app/framework/map/HereMapsManager$State;
.super Ljava/lang/Enum;
.source "HereMapsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/map/HereMapsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/map/HereMapsManager$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/map/HereMapsManager$State;

.field public static final enum FAILED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

.field public static final enum IDLE:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

.field public static final enum INITIALIZED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

.field public static final enum INITIALIZING:Lcom/navdy/client/app/framework/map/HereMapsManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 179
    new-instance v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/map/HereMapsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->IDLE:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    new-instance v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    const-string v1, "INITIALIZING"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/map/HereMapsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZING:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    new-instance v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/map/HereMapsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    new-instance v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/map/HereMapsManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->FAILED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    .line 178
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->IDLE:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZING:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->INITIALIZED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->FAILED:Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->$VALUES:[Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 178
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/map/HereMapsManager$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 178
    const-class v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/map/HereMapsManager$State;
    .locals 1

    .prologue
    .line 178
    sget-object v0, Lcom/navdy/client/app/framework/map/HereMapsManager$State;->$VALUES:[Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/map/HereMapsManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/map/HereMapsManager$State;

    return-object v0
.end method
