.class Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;
.super Ljava/lang/Object;
.source "NavCoordsAddressProcessor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

.field final synthetic val$finalPrecision:Lcom/navdy/client/app/framework/models/Destination$Precision;

.field final synthetic val$lat:Ljava/lang/String;

.field final synthetic val$lng:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

    .prologue
    .line 812
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->this$1:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->val$lat:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->val$lng:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->val$finalPrecision:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 815
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->val$lat:Ljava/lang/String;

    .line 816
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->val$lng:Ljava/lang/String;

    .line 817
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 815
    invoke-static {v2, v3, v4, v5}, Lcom/navdy/client/app/framework/map/MapUtils;->buildNewCoordinate(DD)Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v0

    .line 820
    .local v0, "latLngCoord":Lcom/navdy/service/library/events/location/Coordinate;
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->this$1:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    sget-object v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->NONE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    if-eq v1, v2, :cond_0

    .line 821
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->this$1:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->this$1:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;->onFailure(Ljava/lang/String;)V

    .line 826
    :goto_0
    return-void

    .line 823
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->this$1:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;

    iget-object v1, v1, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;->val$callback:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->this$1:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;

    iget-object v2, v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1;->this$0:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;

    iget-object v2, v2, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getDisplayCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$9$1$1;->val$finalPrecision:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;->onSuccess(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V

    goto :goto_0
.end method
