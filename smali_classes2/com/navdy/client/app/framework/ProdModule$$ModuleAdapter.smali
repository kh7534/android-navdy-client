.class public final Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;
.super Ldagger/internal/ModuleAdapter;
.source "ProdModule$$ModuleAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;,
        Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;,
        Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideTTSAudioRouterProvidesAdapter;,
        Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldagger/internal/ModuleAdapter",
        "<",
        "Lcom/navdy/client/app/framework/ProdModule;",
        ">;"
    }
.end annotation


# static fields
.field private static final INCLUDES:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final INJECTS:[Ljava/lang/String;

.field private static final STATIC_INJECTIONS:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 14
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "members/com.navdy.client.app.NavdyApplication"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "members/com.navdy.client.app.ui.settings.AudioSettingsActivity"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "members/com.navdy.client.app.framework.servicehandler.SpeechServiceHandler"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "members/com.navdy.client.app.framework.util.TTSAudioRouter"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "members/com.navdy.client.app.framework.util.CarMdClient"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "members/com.navdy.client.app.framework.servicehandler.NetworkStatusManager"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "members/com.navdy.client.app.framework.AppInstance"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "members/com.navdy.client.app.ui.search.SearchActivity"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "members/com.navdy.client.app.ui.homescreen.SuggestionsFragment"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "members/com.navdy.client.app.ui.routing.RoutingActivity"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "members/com.navdy.client.app.ui.favorites.FavoritesEditActivity"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "members/com.navdy.client.debug.SubmitTicketFragment"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "members/com.navdy.client.app.framework.servicehandler.VoiceServiceHandler"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "members/com.navdy.client.app.ui.settings.HFPAudioDelaySettingsActivity"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "members/com.navdy.client.app.service.ActivityRecognizedCallbackService"

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    .line 15
    new-array v0, v3, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    .line 16
    new-array v0, v3, [Ljava/lang/Class;

    sput-object v0, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 19
    const-class v1, Lcom/navdy/client/app/framework/ProdModule;

    sget-object v2, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;->INJECTS:[Ljava/lang/String;

    sget-object v3, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;->STATIC_INJECTIONS:[Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;->INCLUDES:[Ljava/lang/Class;

    move-object v0, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Ldagger/internal/ModuleAdapter;-><init>(Ljava/lang/Class;[Ljava/lang/String;[Ljava/lang/Class;Z[Ljava/lang/Class;ZZ)V

    .line 20
    return-void
.end method


# virtual methods
.method public getBindings(Ldagger/internal/BindingsGroup;Lcom/navdy/client/app/framework/ProdModule;)V
    .locals 2
    .param p1, "bindings"    # Ldagger/internal/BindingsGroup;
    .param p2, "module"    # Lcom/navdy/client/app/framework/ProdModule;

    .prologue
    .line 28
    const-string v0, "com.navdy.service.library.network.http.IHttpManager"

    new-instance v1, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideHttpManagerProvidesAdapter;-><init>(Lcom/navdy/client/app/framework/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 29
    const-string v0, "com.navdy.client.app.framework.util.TTSAudioRouter"

    new-instance v1, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideTTSAudioRouterProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideTTSAudioRouterProvidesAdapter;-><init>(Lcom/navdy/client/app/framework/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 30
    const-string v0, "android.content.SharedPreferences"

    new-instance v1, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideSharedPreferencesProvidesAdapter;-><init>(Lcom/navdy/client/app/framework/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 31
    const-string v0, "com.navdy.client.app.framework.servicehandler.NetworkStatusManager"

    new-instance v1, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;

    invoke-direct {v1, p2}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter$ProvideNetworkStatusManagerProvidesAdapter;-><init>(Lcom/navdy/client/app/framework/ProdModule;)V

    invoke-virtual {p1, v0, v1}, Ldagger/internal/BindingsGroup;->contributeProvidesBinding(Ljava/lang/String;Ldagger/internal/ProvidesBinding;)Ldagger/internal/Binding;

    .line 32
    return-void
.end method

.method public bridge synthetic getBindings(Ldagger/internal/BindingsGroup;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p2, Lcom/navdy/client/app/framework/ProdModule;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/ProdModule$$ModuleAdapter;->getBindings(Ldagger/internal/BindingsGroup;Lcom/navdy/client/app/framework/ProdModule;)V

    return-void
.end method
