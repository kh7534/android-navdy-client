.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->getSuggestedArticles(Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 445
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 8
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 448
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheckHelpCenterSettings(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->getLocale()Ljava/util/Locale;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->getBestLocale(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Ljava/util/Locale;

    move-result-object v3

    .line 451
    .local v3, "queryLocale":Ljava/util/Locale;
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->getLabelNames()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v4, 0x0

    .line 455
    .local v4, "labelNames":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    move-result-object v0

    .line 456
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    .line 457
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->getQuery()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    .line 460
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->getCategoryId()Ljava/lang/Long;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    .line 461
    invoke-virtual {v6}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->getSectionId()Ljava/lang/Long;

    move-result-object v6

    iget-object v7, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    .line 455
    invoke-virtual/range {v0 .. v7}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->getSuggestedArticles(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Lcom/zendesk/service/ZendeskCallback;)V

    .line 464
    .end local v3    # "queryLocale":Ljava/util/Locale;
    .end local v4    # "labelNames":Ljava/lang/String;
    :cond_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->getLocale()Ljava/util/Locale;

    move-result-object v3

    goto :goto_0

    .line 451
    .restart local v3    # "queryLocale":Ljava/util/Locale;
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->val$suggestedArticleSearch:Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;

    .line 453
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/helpcenter/SuggestedArticleSearch;->getLabelNames()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 445
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$15;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
