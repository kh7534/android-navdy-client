.class public Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;
.super Ljava/lang/Object;
.source "ZendeskRequestInterceptor.java"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field private final oauthId:Ljava/lang/String;

.field private final userAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "oauthId"    # Ljava/lang/String;
    .param p2, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;->oauthId:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;->userAgent:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 4
    .param p1, "chain"    # Lokhttp3/Interceptor$Chain;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v1

    invoke-virtual {v1}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v1

    const-string v2, "User-Agent"

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;->userAgent:Ljava/lang/String;

    .line 35
    invoke-virtual {v1, v2, v3}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v1

    const-string v2, "Accept"

    const-string v3, "application/json"

    .line 36
    invoke-virtual {v1, v2, v3}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 38
    .local v0, "requestHeadersBuilder":Lokhttp3/Request$Builder;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;->oauthId:Ljava/lang/String;

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    const-string v1, "Client-Identifier"

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestInterceptor;->oauthId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    .line 42
    :cond_0
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v1

    invoke-interface {p1, v1}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v1

    return-object v1
.end method
