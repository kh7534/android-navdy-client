.class Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskUserProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->setUserFields(Ljava/util/Map;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$userFields:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/util/Map;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;->val$userFields:Ljava/util/Map;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 5
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 97
    new-instance v0, Lcom/zendesk/sdk/model/request/UserFieldRequest;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;->val$userFields:Ljava/util/Map;

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/model/request/UserFieldRequest;-><init>(Ljava/util/Map;)V

    .line 98
    .local v0, "userFieldRequest":Lcom/zendesk/sdk/model/request/UserFieldRequest;
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;

    invoke-static {v1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;->access$100(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider;)Lcom/zendesk/sdk/network/impl/ZendeskUserService;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4$1;

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v3, p0, v4}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v1, v2, v0, v3}, Lcom/zendesk/sdk/network/impl/ZendeskUserService;->setUserFields(Ljava/lang/String;Lcom/zendesk/sdk/model/request/UserFieldRequest;Lcom/zendesk/service/ZendeskCallback;)V

    .line 107
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 94
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$4;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
