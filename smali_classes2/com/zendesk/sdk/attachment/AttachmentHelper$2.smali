.class final Lcom/zendesk/sdk/attachment/AttachmentHelper$2;
.super Ljava/lang/Object;
.source "AttachmentHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/attachment/AttachmentHelper;->showAttachmentTryAgainDialog(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereResult;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$file:Lcom/zendesk/belvedere/BelvedereResult;

.field final synthetic val$imageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/attachment/ImageUploadHelper;Lcom/zendesk/belvedere/BelvedereResult;Landroid/content/Context;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$imageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    iput-object p2, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    iput-object p3, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$imageUploadHelper:Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    iget-object v2, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-virtual {v3}, Lcom/zendesk/belvedere/BelvedereResult;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/zendesk/sdk/attachment/AttachmentHelper;->access$000(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->uploadImage(Lcom/zendesk/belvedere/BelvedereResult;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$attachmentContainerHost:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;

    iget-object v1, p0, Lcom/zendesk/sdk/attachment/AttachmentHelper$2;->val$file:Lcom/zendesk/belvedere/BelvedereResult;

    invoke-virtual {v1}, Lcom/zendesk/belvedere/BelvedereResult;->getFile()Ljava/io/File;

    move-result-object v1

    sget-object v2, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;->UPLOADING:Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost;->setAttachmentState(Ljava/io/File;Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentState;)V

    .line 97
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 98
    return-void
.end method
