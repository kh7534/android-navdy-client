.class public Lcom/zendesk/sdk/rating/impl/IdentityRateMyAppRule;
.super Ljava/lang/Object;
.source "IdentityRateMyAppRule.java"

# interfaces
.implements Lcom/zendesk/sdk/rating/RateMyAppRule;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "IdentityRateMyAppRule"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/rating/impl/IdentityRateMyAppRule;->mContext:Landroid/content/Context;

    .line 38
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public permitsShowOfDialog()Z
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 43
    iget-object v10, p0, Lcom/zendesk/sdk/rating/impl/IdentityRateMyAppRule;->mContext:Landroid/content/Context;

    if-nez v10, :cond_0

    .line 68
    :goto_0
    return v8

    .line 47
    :cond_0
    sget-object v10, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v10}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v10

    invoke-interface {v10}, Lcom/zendesk/sdk/storage/StorageStore;->identityStorage()Lcom/zendesk/sdk/storage/IdentityStorage;

    move-result-object v10

    invoke-interface {v10}, Lcom/zendesk/sdk/storage/IdentityStorage;->getIdentity()Lcom/zendesk/sdk/model/access/Identity;

    move-result-object v2

    .line 48
    .local v2, "identity":Lcom/zendesk/sdk/model/access/Identity;
    sget-object v10, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v10}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v7

    .line 49
    .local v7, "mobileSettings":Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    invoke-virtual {v7}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isConversationsEnabled()Z

    move-result v0

    .line 53
    .local v0, "conversationsEnabled":Z
    instance-of v6, v2, Lcom/zendesk/sdk/model/access/JwtIdentity;

    .line 55
    .local v6, "isJwt":Z
    instance-of v3, v2, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 57
    .local v3, "isAnonymousIdentity":Z
    if-eqz v3, :cond_2

    check-cast v2, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 58
    .end local v2    # "identity":Lcom/zendesk/sdk/model/access/Identity;
    invoke-virtual {v2}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getEmail()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    move v5, v9

    .line 60
    .local v5, "isAnonymousIdentityWithEmail":Z
    :goto_1
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    move v4, v9

    .line 62
    .local v4, "isAnonymousIdentityAndConversationsEnabled":Z
    :goto_2
    if-nez v6, :cond_1

    if-nez v4, :cond_1

    if-eqz v5, :cond_4

    :cond_1
    move v1, v9

    .line 64
    .local v1, "feedbackIsPossible":Z
    :goto_3
    const-string v10, "IdentityRateMyAppRule"

    const-string v11, "Is Jwt: %s, is anonymous with conversations enabled: %s, is anonymous with email: %s"

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    .line 66
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v12, v8

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v12, v9

    const/4 v8, 0x2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v12, v8

    .line 64
    invoke-static {v10, v11, v12}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v8, v1

    .line 68
    goto :goto_0

    .end local v1    # "feedbackIsPossible":Z
    .end local v4    # "isAnonymousIdentityAndConversationsEnabled":Z
    .end local v5    # "isAnonymousIdentityWithEmail":Z
    :cond_2
    move v5, v8

    .line 58
    goto :goto_1

    .restart local v5    # "isAnonymousIdentityWithEmail":Z
    :cond_3
    move v4, v8

    .line 60
    goto :goto_2

    .restart local v4    # "isAnonymousIdentityAndConversationsEnabled":Z
    :cond_4
    move v1, v8

    .line 62
    goto :goto_3
.end method
