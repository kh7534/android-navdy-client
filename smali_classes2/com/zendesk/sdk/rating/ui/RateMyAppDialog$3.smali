.class Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;
.super Lcom/zendesk/service/ZendeskCallback;
.source "RateMyAppDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->show(Landroid/support/v4/app/FragmentActivity;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/settings/SafeMobileSettings;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;

.field final synthetic val$fragmentActivityRef:Ljava/lang/ref/WeakReference;

.field final synthetic val$overrideShowDelay:Z


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Ljava/lang/ref/WeakReference;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;->this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;

    iput-object p2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;->val$fragmentActivityRef:Ljava/lang/ref/WeakReference;

    iput-boolean p3, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;->val$overrideShowDelay:Z

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 5
    .param p1, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 196
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Error showing RMA, unable to load settings | reason: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->getReason()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 4
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 186
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;->val$fragmentActivityRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 187
    .local v0, "fa":Landroid/support/v4/app/FragmentActivity;
    if-eqz v0, :cond_0

    .line 188
    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;->this$0:Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;

    iget-boolean v2, p0, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;->val$overrideShowDelay:Z

    invoke-static {v1, v0, v2, p1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->access$200(Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;Landroid/support/v4/app/FragmentActivity;ZLcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_0
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Host Activity is gone. Cannot display the RateMyAppDialog."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 183
    check-cast p1, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/rating/ui/RateMyAppDialog$3;->onSuccess(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V

    return-void
.end method
