.class Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;
.super Lcom/zendesk/service/ZendeskCallback;
.source "FeedbackDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/CreateRequest;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

.field final synthetic val$feedback:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iput-object p2, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->val$feedback:Ljava/lang/String;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 4
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 171
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Lcom/zendesk/service/ErrorResponse;)V

    .line 172
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$rateMyAppStorage:Lcom/zendesk/sdk/storage/RateMyAppStorage;

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->val$feedback:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->setSavedFeedback(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$sendButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 175
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$editText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 176
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 178
    invoke-interface {p1}, Lcom/zendesk/service/ErrorResponse;->isNetworkError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v1, v1, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    sget v2, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_send_error_no_connectivity_toast:I

    .line 181
    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 183
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 191
    :goto_0
    return-void

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v1, v1, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    sget v2, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_send_error_toast:I

    .line 187
    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V
    .locals 4
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/CreateRequest;

    .prologue
    const/4 v3, 0x0

    .line 149
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Feedback was submitted successfully."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$100(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 152
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Notifying feedback listener of success"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$rateMyAppStorage:Lcom/zendesk/sdk/storage/RateMyAppStorage;

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->clearUserData()V

    .line 154
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-static {v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$100(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;)Lcom/zendesk/sdk/network/SubmissionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/network/SubmissionListener;->onSubmissionCompleted()V

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v1, v1, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    sget v2, Lcom/zendesk/sdk/R$string;->rate_my_app_dialog_feedback_send_success_toast:I

    .line 159
    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 157
    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 163
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->val$rateMyAppStorage:Lcom/zendesk/sdk/storage/RateMyAppStorage;

    invoke-virtual {v0}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->setDontShowAgain()V

    .line 165
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->this$1:Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;

    iget-object v0, v0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    invoke-virtual {v0}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->dismiss()V

    .line 166
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 146
    check-cast p1, Lcom/zendesk/sdk/model/request/CreateRequest;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$2$1;->onSuccess(Lcom/zendesk/sdk/model/request/CreateRequest;)V

    return-void
.end method
