.class Lcom/zendesk/sdk/storage/ZendeskRequestStorage;
.super Ljava/lang/Object;
.source "ZendeskRequestStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/RequestStorage;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static final PREFS_COMMENT_COUNT_KEY_PREFIX:Ljava/lang/String; = "request-id-cc"

.field private static final PREFS_NAME:Ljava/lang/String; = "zendesk-authorization"

.field private static final REQUEST_KEY:Ljava/lang/String; = "stored_requests"


# instance fields
.field private final storage:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    .line 45
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 46
    sget-object v0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Storage was not initialised, requests will not be stored."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    :cond_0
    return-void

    .line 43
    :cond_1
    const-string v0, "zendesk-authorization"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    goto :goto_0
.end method

.method private getCommentCountKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 115
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s-%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "request-id-cc"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public clearUserData()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 123
    :cond_0
    return-void
.end method

.method public getCacheKey()Ljava/lang/String;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 128
    const-string v0, "zendesk-authorization"

    return-object v0
.end method

.method public getCommentCount(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 6
    .param p1, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 102
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 103
    sget-object v3, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Invalid requestId provided"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    :cond_0
    :goto_0
    return-object v2

    .line 107
    :cond_1
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->getCommentCountKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 110
    .local v0, "commentCount":I
    if-eq v0, v4, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method

.method public getStoredRequestIds()Ljava/util/List;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v1, "requests":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v3, :cond_0

    .line 57
    iget-object v3, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    const-string v4, "stored_requests"

    const-string v5, ""

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "storedRequestId":Ljava/lang/String;
    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->fromCsv(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 62
    .local v0, "parsedValues":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 67
    .end local v0    # "parsedValues":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "storedRequestId":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method public setCommentCount(Ljava/lang/String;I)V
    .locals 4
    .param p1, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "commentCount"    # I

    .prologue
    .line 90
    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-gez p2, :cond_1

    .line 91
    :cond_0
    sget-object v1, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Invalid requestId or commentCount provided"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->getCommentCountKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method public storeRequestId(Ljava/lang/String;)V
    .locals 5
    .param p1, "requestId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 73
    iget-object v3, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    if-eqz v3, :cond_0

    invoke-static {p1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->getStoredRequestIds()Ljava/util/List;

    move-result-object v2

    .line 77
    .local v2, "requestList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v0, 0x1

    .line 79
    .local v0, "requestIdNotYetStored":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 80
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->toCsvString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "requestIdsToBeStored":Ljava/lang/String;
    iget-object v3, p0, Lcom/zendesk/sdk/storage/ZendeskRequestStorage;->storage:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "stored_requests"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 86
    .end local v0    # "requestIdNotYetStored":Z
    .end local v1    # "requestIdsToBeStored":Ljava/lang/String;
    .end local v2    # "requestList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    return-void

    .line 77
    .restart local v2    # "requestList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
