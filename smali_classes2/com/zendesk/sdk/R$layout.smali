.class public final Lcom/zendesk/sdk/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f030000

.field public static final abc_action_bar_up_container:I = 0x7f030001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f030002

.field public static final abc_action_menu_item_layout:I = 0x7f030003

.field public static final abc_action_menu_layout:I = 0x7f030004

.field public static final abc_action_mode_bar:I = 0x7f030005

.field public static final abc_action_mode_close_item_material:I = 0x7f030006

.field public static final abc_activity_chooser_view:I = 0x7f030007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f030008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f030009

.field public static final abc_alert_dialog_material:I = 0x7f03000a

.field public static final abc_dialog_title_material:I = 0x7f03000c

.field public static final abc_expanded_menu_layout:I = 0x7f03000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f03000e

.field public static final abc_list_menu_item_icon:I = 0x7f03000f

.field public static final abc_list_menu_item_layout:I = 0x7f030010

.field public static final abc_list_menu_item_radio:I = 0x7f030011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f030012

.field public static final abc_popup_menu_item_layout:I = 0x7f030013

.field public static final abc_screen_content_include:I = 0x7f030014

.field public static final abc_screen_simple:I = 0x7f030015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f030016

.field public static final abc_screen_toolbar:I = 0x7f030017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f030018

.field public static final abc_search_view:I = 0x7f030019

.field public static final abc_select_dialog_material:I = 0x7f03001a

.field public static final activity_contact_zendesk:I = 0x7f03001f

.field public static final activity_requests:I = 0x7f030022

.field public static final activity_support:I = 0x7f030023

.field public static final activity_view_article:I = 0x7f030025

.field public static final activity_view_request:I = 0x7f030026

.field public static final belvedere_dialog:I = 0x7f03002d

.field public static final belvedere_dialog_row:I = 0x7f03002e

.field public static final design_bottom_sheet_dialog:I = 0x7f030038

.field public static final design_layout_snackbar:I = 0x7f030039

.field public static final design_layout_snackbar_include:I = 0x7f03003a

.field public static final design_layout_tab_icon:I = 0x7f03003b

.field public static final design_layout_tab_text:I = 0x7f03003c

.field public static final design_menu_item_action_area:I = 0x7f03003d

.field public static final design_navigation_item:I = 0x7f03003e

.field public static final design_navigation_item_header:I = 0x7f03003f

.field public static final design_navigation_item_separator:I = 0x7f030040

.field public static final design_navigation_item_subheader:I = 0x7f030041

.field public static final design_navigation_menu:I = 0x7f030042

.field public static final design_navigation_menu_item:I = 0x7f030043

.field public static final fragment_contact_zendesk:I = 0x7f030086

.field public static final fragment_help:I = 0x7f03008d

.field public static final fragment_request_list:I = 0x7f030095

.field public static final fragment_send_feedback:I = 0x7f030099

.field public static final fragment_view_request:I = 0x7f03009d

.field public static final include_no_network_view:I = 0x7f0300b0

.field public static final include_retry_view:I = 0x7f0300b1

.field public static final notification_media_action:I = 0x7f0300be

.field public static final notification_media_cancel_action:I = 0x7f0300bf

.field public static final notification_template_big_media:I = 0x7f0300c0

.field public static final notification_template_big_media_narrow:I = 0x7f0300c2

.field public static final notification_template_media:I = 0x7f0300c7

.field public static final notification_template_part_chronometer:I = 0x7f0300c9

.field public static final notification_template_part_time:I = 0x7f0300ca

.field public static final row_action:I = 0x7f0300d7

.field public static final row_agent_comment:I = 0x7f0300d8

.field public static final row_article:I = 0x7f0300d9

.field public static final row_article_attachment:I = 0x7f0300da

.field public static final row_category:I = 0x7f0300db

.field public static final row_end_user_comment:I = 0x7f0300dc

.field public static final row_loading_progress:I = 0x7f0300dd

.field public static final row_no_articles_found:I = 0x7f0300de

.field public static final row_padding:I = 0x7f0300df

.field public static final row_request:I = 0x7f0300e0

.field public static final row_search_article:I = 0x7f0300e1

.field public static final row_section:I = 0x7f0300e2

.field public static final select_dialog_item_material:I = 0x7f0300ea

.field public static final select_dialog_multichoice_material:I = 0x7f0300eb

.field public static final select_dialog_singlechoice_material:I = 0x7f0300ec

.field public static final support_simple_spinner_dropdown_item:I = 0x7f030101

.field public static final view_attachment_container_item:I = 0x7f03010a

.field public static final view_attachment_inline_item:I = 0x7f03010b

.field public static final zendesk_toolbar:I = 0x7f03010d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 999
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
