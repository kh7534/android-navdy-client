.class Lcom/zendesk/sdk/requests/ImageLoader$1;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Lcom/squareup/picasso/Target;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/ImageLoader;->loadAndShowImage(Lcom/zendesk/sdk/model/request/Attachment;Landroid/content/Context;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/belvedere/Belvedere;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/ImageLoader;

.field final synthetic val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

.field final synthetic val$belvedere:Lcom/zendesk/belvedere/Belvedere;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/belvedere/Belvedere;Lcom/zendesk/sdk/model/request/Attachment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ImageLoader;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    iput-object p2, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p3, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$belvedere:Lcom/zendesk/belvedere/Belvedere;

    iput-object p4, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "errorDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "Error loading attachment"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 87
    :cond_0
    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "from"    # Lcom/squareup/picasso/Picasso$LoadedFrom;

    .prologue
    .line 79
    new-instance v0, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;

    iget-object v1, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    iget-object v2, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iget-object v3, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$belvedere:Lcom/zendesk/belvedere/Belvedere;

    invoke-direct {v0, v1, v2, v3}, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/belvedere/Belvedere;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/zendesk/sdk/requests/ImageLoader$TaskData;

    const/4 v2, 0x0

    new-instance v3, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;

    iget-object v4, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    iget-object v5, p0, Lcom/zendesk/sdk/requests/ImageLoader$1;->val$attachment:Lcom/zendesk/sdk/model/request/Attachment;

    invoke-direct {v3, v4, v5, p1}, Lcom/zendesk/sdk/requests/ImageLoader$TaskData;-><init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/sdk/model/request/Attachment;Landroid/graphics/Bitmap;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/requests/ImageLoader$DownloadImageToExternalStorage;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 80
    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "placeHolderDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 92
    return-void
.end method
