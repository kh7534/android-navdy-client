.class Lcom/zendesk/sdk/requests/CommentWithUser;
.super Ljava/lang/Object;
.source "CommentWithUser.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "CommentWithUser"


# instance fields
.field private final mAuthor:Lcom/zendesk/sdk/model/request/User;

.field private final mComment:Lcom/zendesk/sdk/model/request/CommentResponse;


# direct methods
.method private constructor <init>(Lcom/zendesk/sdk/model/request/CommentResponse;Lcom/zendesk/sdk/model/request/User;)V
    .locals 0
    .param p1, "comment"    # Lcom/zendesk/sdk/model/request/CommentResponse;
    .param p2, "author"    # Lcom/zendesk/sdk/model/request/User;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/zendesk/sdk/requests/CommentWithUser;->mComment:Lcom/zendesk/sdk/model/request/CommentResponse;

    .line 26
    iput-object p2, p0, Lcom/zendesk/sdk/requests/CommentWithUser;->mAuthor:Lcom/zendesk/sdk/model/request/User;

    .line 27
    return-void
.end method

.method public static build(Lcom/zendesk/sdk/model/request/CommentResponse;Lcom/zendesk/sdk/model/request/User;)Lcom/zendesk/sdk/requests/CommentWithUser;
    .locals 1
    .param p0, "comment"    # Lcom/zendesk/sdk/model/request/CommentResponse;
    .param p1, "user"    # Lcom/zendesk/sdk/model/request/User;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 95
    if-eqz p0, :cond_0

    .line 96
    new-instance v0, Lcom/zendesk/sdk/requests/CommentWithUser;

    invoke-direct {v0, p0, p1}, Lcom/zendesk/sdk/requests/CommentWithUser;-><init>(Lcom/zendesk/sdk/model/request/CommentResponse;Lcom/zendesk/sdk/model/request/User;)V

    .line 100
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static build(Lcom/zendesk/sdk/model/request/CommentResponse;Ljava/util/List;)Lcom/zendesk/sdk/requests/CommentWithUser;
    .locals 6
    .param p0, "comment"    # Lcom/zendesk/sdk/model/request/CommentResponse;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/request/CommentResponse;",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/request/User;",
            ">;)",
            "Lcom/zendesk/sdk/requests/CommentWithUser;"
        }
    .end annotation

    .prologue
    .local p1, "users":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/User;>;"
    const/4 v3, 0x0

    .line 64
    if-eqz p0, :cond_3

    .line 66
    const/4 v0, 0x0

    .line 68
    .local v0, "author":Lcom/zendesk/sdk/model/request/User;
    invoke-static {p1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 69
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/request/User;

    .line 71
    .local v1, "user":Lcom/zendesk/sdk/model/request/User;
    if-nez v1, :cond_1

    move-object v2, v3

    .line 73
    .local v2, "userId":Ljava/lang/Long;
    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/zendesk/sdk/model/request/CommentResponse;->getAuthorId()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 74
    move-object v0, v1

    goto :goto_0

    .line 71
    .end local v2    # "userId":Ljava/lang/Long;
    :cond_1
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/User;->getId()Ljava/lang/Long;

    move-result-object v2

    goto :goto_1

    .line 79
    .end local v1    # "user":Lcom/zendesk/sdk/model/request/User;
    :cond_2
    new-instance v3, Lcom/zendesk/sdk/requests/CommentWithUser;

    invoke-direct {v3, p0, v0}, Lcom/zendesk/sdk/requests/CommentWithUser;-><init>(Lcom/zendesk/sdk/model/request/CommentResponse;Lcom/zendesk/sdk/model/request/User;)V

    .line 82
    .end local v0    # "author":Lcom/zendesk/sdk/model/request/User;
    :cond_3
    return-object v3
.end method


# virtual methods
.method public getAuthor()Lcom/zendesk/sdk/model/request/User;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/zendesk/sdk/requests/CommentWithUser;->mAuthor:Lcom/zendesk/sdk/model/request/User;

    if-nez v0, :cond_0

    .line 47
    const-string v0, "CommentWithUser"

    const-string v1, "Author is null, returning default author"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    new-instance v0, Lcom/zendesk/sdk/model/request/User;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/request/User;-><init>()V

    .line 51
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/zendesk/sdk/requests/CommentWithUser;->mAuthor:Lcom/zendesk/sdk/model/request/User;

    goto :goto_0
.end method

.method public getComment()Lcom/zendesk/sdk/model/request/CommentResponse;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/zendesk/sdk/requests/CommentWithUser;->mComment:Lcom/zendesk/sdk/model/request/CommentResponse;

    return-object v0
.end method
