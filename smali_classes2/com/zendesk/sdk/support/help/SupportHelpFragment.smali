.class public Lcom/zendesk/sdk/support/help/SupportHelpFragment;
.super Landroid/support/v4/app/Fragment;
.source "SupportHelpFragment.java"


# static fields
.field private static final ARG_SDK_UI_CONFIG:Ljava/lang/String; = "arg_sdk_ui_config"

.field public static final LOG_TAG:Ljava/lang/String; = "SupportHelpFragment"


# instance fields
.field private adapter:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

.field private presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

.field private recyclerView:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/zendesk/sdk/support/SupportUiConfig;)Lcom/zendesk/sdk/support/help/SupportHelpFragment;
    .locals 3
    .param p0, "supportUiConfig"    # Lcom/zendesk/sdk/support/SupportUiConfig;

    .prologue
    .line 88
    new-instance v1, Lcom/zendesk/sdk/support/help/SupportHelpFragment;

    invoke-direct {v1}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;-><init>()V

    .line 90
    .local v1, "fragment":Lcom/zendesk/sdk/support/help/SupportHelpFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "arg_sdk_ui_config"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 93
    invoke-virtual {v1, v0}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->setArguments(Landroid/os/Bundle;)V

    .line 95
    return-object v1
.end method

.method private setupRecyclerView()V
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    .line 79
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 78
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 80
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/zendesk/sdk/support/help/SeparatorDecoration;

    .line 81
    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/zendesk/sdk/R$drawable;->help_separator:I

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/zendesk/sdk/support/help/SeparatorDecoration;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 80
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 83
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 85
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->setRetainInstance(Z)V

    .line 56
    new-instance v1, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-virtual {p0}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "arg_sdk_ui_config"

    .line 57
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-direct {v1, v0}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;-><init>(Lcom/zendesk/sdk/support/SupportUiConfig;)V

    iput-object v1, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    .line 59
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->setContentUpdateListener(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 68
    sget v1, Lcom/zendesk/sdk/R$layout;->fragment_help:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 70
    .local v0, "view":Landroid/view/View;
    sget v1, Lcom/zendesk/sdk/R$id;->recyclerView:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 72
    invoke-direct {p0}, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->setupRecyclerView()V

    .line 74
    return-object v0
.end method

.method public setPresenter(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V
    .locals 1
    .param p1, "presenter"    # Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->presenter:Lcom/zendesk/sdk/support/SupportMvp$Presenter;

    .line 44
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/SupportHelpFragment;->adapter:Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/support/help/HelpRecyclerViewAdapter;->setContentUpdateListener(Lcom/zendesk/sdk/support/SupportMvp$Presenter;)V

    .line 47
    :cond_0
    return-void
.end method
