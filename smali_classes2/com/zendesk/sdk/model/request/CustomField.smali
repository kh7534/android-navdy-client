.class public Lcom/zendesk/sdk/model/request/CustomField;
.super Ljava/lang/Object;
.source "CustomField.java"


# instance fields
.field private id:Ljava/lang/Long;

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/Long;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/zendesk/sdk/model/request/CustomField;->id:Ljava/lang/Long;

    .line 21
    iput-object p2, p0, Lcom/zendesk/sdk/model/request/CustomField;->value:Ljava/lang/String;

    .line 22
    return-void
.end method
