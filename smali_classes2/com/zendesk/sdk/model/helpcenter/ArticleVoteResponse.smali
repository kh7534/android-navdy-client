.class public Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;
.super Ljava/lang/Object;
.source "ArticleVoteResponse.java"


# instance fields
.field private vote:Lcom/zendesk/sdk/model/helpcenter/ArticleVote;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getVote()Lcom/zendesk/sdk/model/helpcenter/ArticleVote;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/zendesk/sdk/model/helpcenter/ArticleVoteResponse;->vote:Lcom/zendesk/sdk/model/helpcenter/ArticleVote;

    return-object v0
.end method
