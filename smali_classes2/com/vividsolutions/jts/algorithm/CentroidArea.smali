.class public Lcom/vividsolutions/jts/algorithm/CentroidArea;
.super Ljava/lang/Object;
.source "CentroidArea.java"


# instance fields
.field private areasum2:D

.field private basePt:Lcom/vividsolutions/jts/geom/Coordinate;

.field private centSum:Lcom/vividsolutions/jts/geom/Coordinate;

.field private cg3:Lcom/vividsolutions/jts/geom/Coordinate;

.field private totalLength:D

.field private triangleCent3:Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->basePt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 56
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->triangleCent3:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 57
    iput-wide v2, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->areasum2:D

    .line 58
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->cg3:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 61
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 62
    iput-wide v2, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->totalLength:D

    .line 66
    iput-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->basePt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 67
    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 2
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->addShell([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 126
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getNumInteriorRing()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 127
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Polygon;->getInteriorRingN(I)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->addHole([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_0
    return-void
.end method

.method private addHole([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 5
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 141
    invoke-static {p1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v1

    .line 142
    .local v1, "isPositiveArea":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 143
    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->basePt:Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v3, p1, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, p1, v4

    invoke-direct {p0, v2, v3, v4, v1}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->addTriangle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->addLinearSegments([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 146
    return-void
.end method

.method private addLinearSegments([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 14
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    .line 189
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 190
    aget-object v1, p1, v0

    add-int/lit8 v8, v0, 0x1

    aget-object v8, p1, v8

    invoke-virtual {v1, v8}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 191
    .local v6, "segmentLen":D
    iget-wide v8, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->totalLength:D

    add-double/2addr v8, v6

    iput-wide v8, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->totalLength:D

    .line 193
    aget-object v1, p1, v0

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-int/lit8 v1, v0, 0x1

    aget-object v1, p1, v1

    iget-wide v10, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v8, v10

    div-double v2, v8, v12

    .line 194
    .local v2, "midx":D
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double v10, v6, v2

    add-double/2addr v8, v10

    iput-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 195
    aget-object v1, p1, v0

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-int/lit8 v1, v0, 0x1

    aget-object v1, p1, v1

    iget-wide v10, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v8, v10

    div-double v4, v8, v12

    .line 196
    .local v4, "midy":D
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double v10, v6, v4

    add-double/2addr v8, v10

    iput-wide v8, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    .end local v2    # "midx":D
    .end local v4    # "midy":D
    .end local v6    # "segmentLen":D
    :cond_0
    return-void
.end method

.method private addShell([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 5
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 133
    invoke-static {p1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 134
    .local v1, "isPositiveArea":Z
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    .line 135
    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->basePt:Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v3, p1, v0

    add-int/lit8 v4, v0, 0x1

    aget-object v4, p1, v4

    invoke-direct {p0, v2, v3, v4, v1}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->addTriangle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Z)V

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 133
    .end local v0    # "i":I
    .end local v1    # "isPositiveArea":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 137
    .restart local v0    # "i":I
    .restart local v1    # "isPositiveArea":Z
    :cond_1
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->addLinearSegments([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 138
    return-void
.end method

.method private addTriangle(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Z)V
    .locals 12
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p4, "isPositiveArea"    # Z

    .prologue
    .line 149
    if-eqz p4, :cond_0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 150
    .local v2, "sign":D
    :goto_0
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->triangleCent3:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-static {p1, p2, p3, v4}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->centroid3(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 151
    invoke-static {p1, p2, p3}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->area2(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 152
    .local v0, "area2":D
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->cg3:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double v8, v2, v0

    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->triangleCent3:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v10, v5, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iput-wide v6, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 153
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->cg3:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double v8, v2, v0

    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->triangleCent3:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v10, v5, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iput-wide v6, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 154
    iget-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->areasum2:D

    mul-double v6, v2, v0

    add-double/2addr v4, v6

    iput-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->areasum2:D

    .line 155
    return-void

    .line 149
    .end local v0    # "area2":D
    .end local v2    # "sign":D
    :cond_0
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method private static area2(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 8
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p3"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 174
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v0, v2

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v2, v4

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method private static centroid3(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 4
    .param p0, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p3"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "c"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 163
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v0, v2

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 164
    iget-wide v0, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v0, v2

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 165
    return-void
.end method

.method private setBasePoint(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "basePt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->basePt:Lcom/vividsolutions/jts/geom/Coordinate;

    if-nez v0, :cond_0

    .line 120
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->basePt:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 5
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 77
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v3, :cond_1

    move-object v2, p1

    .line 78
    check-cast v2, Lcom/vividsolutions/jts/geom/Polygon;

    .line 79
    .local v2, "poly":Lcom/vividsolutions/jts/geom/Polygon;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Polygon;->getExteriorRing()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->setBasePoint(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 80
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->add(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 88
    .end local v2    # "poly":Lcom/vividsolutions/jts/geom/Polygon;
    :cond_0
    return-void

    .line 82
    :cond_1
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 83
    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 84
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 85
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public add([Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "ring"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 98
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->setBasePoint(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 99
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/CentroidArea;->addShell([Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 100
    return-void
.end method

.method public getCentroid()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    .line 104
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 105
    .local v0, "cent":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->areasum2:D

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->cg3:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    div-double/2addr v2, v6

    iget-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->areasum2:D

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 107
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->cg3:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    div-double/2addr v2, v6

    iget-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->areasum2:D

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 114
    :goto_0
    return-object v0

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->totalLength:D

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 112
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/algorithm/CentroidArea;->totalLength:D

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    goto :goto_0
.end method
