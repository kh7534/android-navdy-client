.class Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$VertexCoordinateFilter;
.super Ljava/lang/Object;
.source "SimpleMinimumClearance.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VertexCoordinateFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$VertexCoordinateFilter;->this$0:Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 3
    .param p1, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$VertexCoordinateFilter;->this$0:Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;

    invoke-static {v0}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;->access$000(Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    new-instance v1, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$ComputeMCCoordinateSequenceFilter;

    iget-object v2, p0, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$VertexCoordinateFilter;->this$0:Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;

    invoke-direct {v1, v2, p1}, Lcom/vividsolutions/jts/precision/SimpleMinimumClearance$ComputeMCCoordinateSequenceFilter;-><init>(Lcom/vividsolutions/jts/precision/SimpleMinimumClearance;Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateSequenceFilter;)V

    .line 133
    return-void
.end method
