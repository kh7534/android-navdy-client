.class public Lcom/vividsolutions/jts/index/quadtree/Node;
.super Lcom/vividsolutions/jts/index/quadtree/NodeBase;
.source "Node.java"


# instance fields
.field private centrex:D

.field private centrey:D

.field private env:Lcom/vividsolutions/jts/geom/Envelope;

.field private level:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Envelope;I)V
    .locals 6
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;
    .param p2, "level"    # I

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 72
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/quadtree/NodeBase;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    .line 75
    iput p2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->level:I

    .line 76
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v2

    add-double/2addr v0, v2

    div-double/2addr v0, v4

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    .line 77
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v2

    add-double/2addr v0, v2

    div-double/2addr v0, v4

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    .line 78
    return-void
.end method

.method public static createExpanded(Lcom/vividsolutions/jts/index/quadtree/Node;Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/Node;
    .locals 3
    .param p0, "node"    # Lcom/vividsolutions/jts/index/quadtree/Node;
    .param p1, "addEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 58
    new-instance v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 59
    .local v0, "expandEnv":Lcom/vividsolutions/jts/geom/Envelope;
    if-eqz p0, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/Envelope;->expandToInclude(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 61
    :cond_0
    invoke-static {v0}, Lcom/vividsolutions/jts/index/quadtree/Node;->createNode(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/Node;

    move-result-object v1

    .line 62
    .local v1, "largerNode":Lcom/vividsolutions/jts/index/quadtree/Node;
    if-eqz p0, :cond_1

    invoke-virtual {v1, p0}, Lcom/vividsolutions/jts/index/quadtree/Node;->insertNode(Lcom/vividsolutions/jts/index/quadtree/Node;)V

    .line 63
    :cond_1
    return-object v1
.end method

.method public static createNode(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/Node;
    .locals 4
    .param p0, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 51
    new-instance v0, Lcom/vividsolutions/jts/index/quadtree/Key;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/index/quadtree/Key;-><init>(Lcom/vividsolutions/jts/geom/Envelope;)V

    .line 52
    .local v0, "key":Lcom/vividsolutions/jts/index/quadtree/Key;
    new-instance v1, Lcom/vividsolutions/jts/index/quadtree/Node;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/quadtree/Key;->getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/quadtree/Key;->getLevel()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/vividsolutions/jts/index/quadtree/Node;-><init>(Lcom/vividsolutions/jts/geom/Envelope;I)V

    .line 53
    .local v1, "node":Lcom/vividsolutions/jts/index/quadtree/Node;
    return-object v1
.end method

.method private createSubnode(I)Lcom/vividsolutions/jts/index/quadtree/Node;
    .locals 11
    .param p1, "index"    # I

    .prologue
    .line 163
    const-wide/16 v2, 0x0

    .line 164
    .local v2, "minx":D
    const-wide/16 v4, 0x0

    .line 165
    .local v4, "maxx":D
    const-wide/16 v6, 0x0

    .line 166
    .local v6, "miny":D
    const-wide/16 v8, 0x0

    .line 168
    .local v8, "maxy":D
    packed-switch p1, :pswitch_data_0

    .line 194
    :goto_0
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-direct/range {v1 .. v9}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(DDDD)V

    .line 195
    .local v1, "sqEnv":Lcom/vividsolutions/jts/geom/Envelope;
    new-instance v0, Lcom/vividsolutions/jts/index/quadtree/Node;

    iget v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->level:I

    add-int/lit8 v10, v10, -0x1

    invoke-direct {v0, v1, v10}, Lcom/vividsolutions/jts/index/quadtree/Node;-><init>(Lcom/vividsolutions/jts/geom/Envelope;I)V

    .line 196
    .local v0, "node":Lcom/vividsolutions/jts/index/quadtree/Node;
    return-object v0

    .line 170
    .end local v0    # "node":Lcom/vividsolutions/jts/index/quadtree/Node;
    .end local v1    # "sqEnv":Lcom/vividsolutions/jts/geom/Envelope;
    :pswitch_0
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    .line 171
    iget-wide v4, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    .line 172
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v6

    .line 173
    iget-wide v8, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    .line 174
    goto :goto_0

    .line 176
    :pswitch_1
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    .line 177
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v4

    .line 178
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v6

    .line 179
    iget-wide v8, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    .line 180
    goto :goto_0

    .line 182
    :pswitch_2
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    .line 183
    iget-wide v4, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    .line 184
    iget-wide v6, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    .line 185
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v8

    .line 186
    goto :goto_0

    .line 188
    :pswitch_3
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    .line 189
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v4

    .line 190
    iget-wide v6, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    .line 191
    iget-object v10, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v10}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v8

    goto :goto_0

    .line 168
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getSubnode(I)Lcom/vividsolutions/jts/index/quadtree/Node;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/index/quadtree/Node;->createSubnode(I)Lcom/vividsolutions/jts/index/quadtree/Node;

    move-result-object v1

    aput-object v1, v0, p1

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v0, v0, p1

    return-object v0
.end method


# virtual methods
.method public find(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/NodeBase;
    .locals 6
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 115
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    invoke-static {p1, v2, v3, v4, v5}, Lcom/vividsolutions/jts/index/quadtree/Node;->getSubnodeIndex(Lcom/vividsolutions/jts/geom/Envelope;DD)I

    move-result v1

    .line 116
    .local v1, "subnodeIndex":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 124
    .end local p0    # "this":Lcom/vividsolutions/jts/index/quadtree/Node;
    :cond_0
    :goto_0
    return-object p0

    .line 118
    .restart local p0    # "this":Lcom/vividsolutions/jts/index/quadtree/Node;
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    .line 120
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aget-object v0, v2, v1

    .line 121
    .local v0, "node":Lcom/vividsolutions/jts/index/quadtree/Node;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/index/quadtree/Node;->find(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/NodeBase;

    move-result-object p0

    goto :goto_0
.end method

.method public getEnvelope()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    return-object v0
.end method

.method public getNode(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/Node;
    .locals 6
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 96
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    iget-wide v4, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    invoke-static {p1, v2, v3, v4, v5}, Lcom/vividsolutions/jts/index/quadtree/Node;->getSubnodeIndex(Lcom/vividsolutions/jts/geom/Envelope;DD)I

    move-result v1

    .line 98
    .local v1, "subnodeIndex":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 100
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/index/quadtree/Node;->getSubnode(I)Lcom/vividsolutions/jts/index/quadtree/Node;

    move-result-object v0

    .line 102
    .local v0, "node":Lcom/vividsolutions/jts/index/quadtree/Node;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/index/quadtree/Node;->getNode(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/index/quadtree/Node;

    move-result-object p0

    .line 105
    .end local v0    # "node":Lcom/vividsolutions/jts/index/quadtree/Node;
    .end local p0    # "this":Lcom/vividsolutions/jts/index/quadtree/Node;
    :cond_0
    return-object p0
.end method

.method insertNode(Lcom/vividsolutions/jts/index/quadtree/Node;)V
    .locals 8
    .param p1, "node"    # Lcom/vividsolutions/jts/index/quadtree/Node;

    .prologue
    .line 129
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v3, p1, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 132
    iget-object v2, p1, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    iget-wide v4, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrex:D

    iget-wide v6, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->centrey:D

    invoke-static {v2, v4, v5, v6, v7}, Lcom/vividsolutions/jts/index/quadtree/Node;->getSubnodeIndex(Lcom/vividsolutions/jts/geom/Envelope;DD)I

    move-result v1

    .line 134
    .local v1, "index":I
    iget v2, p1, Lcom/vividsolutions/jts/index/quadtree/Node;->level:I

    iget v3, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->level:I

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_2

    .line 135
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aput-object p1, v2, v1

    .line 145
    :goto_1
    return-void

    .line 129
    .end local v1    # "index":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 141
    .restart local v1    # "index":I
    :cond_2
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/index/quadtree/Node;->createSubnode(I)Lcom/vividsolutions/jts/index/quadtree/Node;

    move-result-object v0

    .line 142
    .local v0, "childNode":Lcom/vividsolutions/jts/index/quadtree/Node;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/index/quadtree/Node;->insertNode(Lcom/vividsolutions/jts/index/quadtree/Node;)V

    .line 143
    iget-object v2, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->subnode:[Lcom/vividsolutions/jts/index/quadtree/Node;

    aput-object v0, v2, v1

    goto :goto_1
.end method

.method protected isSearchMatch(Lcom/vividsolutions/jts/geom/Envelope;)Z
    .locals 1
    .param p1, "searchEnv"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/vividsolutions/jts/index/quadtree/Node;->env:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v0

    return v0
.end method
