.class public Lcom/vividsolutions/jts/index/bintree/Root;
.super Lcom/vividsolutions/jts/index/bintree/NodeBase;
.source "Root.java"


# static fields
.field private static final origin:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/bintree/NodeBase;-><init>()V

    .line 55
    return-void
.end method

.method private insertContained(Lcom/vividsolutions/jts/index/bintree/Node;Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/lang/Object;)V
    .locals 6
    .param p1, "tree"    # Lcom/vividsolutions/jts/index/bintree/Node;
    .param p2, "itemInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;
    .param p3, "item"    # Ljava/lang/Object;

    .prologue
    .line 97
    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/bintree/Node;->getInterval()Lcom/vividsolutions/jts/index/bintree/Interval;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/vividsolutions/jts/index/bintree/Interval;->contains(Lcom/vividsolutions/jts/index/bintree/Interval;)Z

    move-result v2

    invoke-static {v2}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 103
    invoke-virtual {p2}, Lcom/vividsolutions/jts/index/bintree/Interval;->getMin()D

    move-result-wide v2

    invoke-virtual {p2}, Lcom/vividsolutions/jts/index/bintree/Interval;->getMax()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/vividsolutions/jts/index/quadtree/IntervalSize;->isZeroWidth(DD)Z

    move-result v0

    .line 105
    .local v0, "isZeroArea":Z
    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/index/bintree/Node;->find(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/NodeBase;

    move-result-object v1

    .line 109
    .local v1, "node":Lcom/vividsolutions/jts/index/bintree/NodeBase;
    :goto_0
    invoke-virtual {v1, p3}, Lcom/vividsolutions/jts/index/bintree/NodeBase;->add(Ljava/lang/Object;)V

    .line 110
    return-void

    .line 108
    .end local v1    # "node":Lcom/vividsolutions/jts/index/bintree/NodeBase;
    :cond_0
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/index/bintree/Node;->getNode(Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/Node;

    move-result-object v1

    .restart local v1    # "node":Lcom/vividsolutions/jts/index/bintree/NodeBase;
    goto :goto_0
.end method


# virtual methods
.method public insert(Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/lang/Object;)V
    .locals 6
    .param p1, "itemInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 62
    const-wide/16 v4, 0x0

    invoke-static {p1, v4, v5}, Lcom/vividsolutions/jts/index/bintree/Root;->getSubnodeIndex(Lcom/vividsolutions/jts/index/bintree/Interval;D)I

    move-result v0

    .line 64
    .local v0, "index":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 65
    invoke-virtual {p0, p2}, Lcom/vividsolutions/jts/index/bintree/Root;->add(Ljava/lang/Object;)V

    .line 88
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v3, p0, Lcom/vividsolutions/jts/index/bintree/Root;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v3, v0

    .line 78
    .local v2, "node":Lcom/vividsolutions/jts/index/bintree/Node;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/bintree/Node;->getInterval()Lcom/vividsolutions/jts/index/bintree/Interval;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/index/bintree/Interval;->contains(Lcom/vividsolutions/jts/index/bintree/Interval;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 79
    :cond_1
    invoke-static {v2, p1}, Lcom/vividsolutions/jts/index/bintree/Node;->createExpanded(Lcom/vividsolutions/jts/index/bintree/Node;Lcom/vividsolutions/jts/index/bintree/Interval;)Lcom/vividsolutions/jts/index/bintree/Node;

    move-result-object v1

    .line 80
    .local v1, "largerNode":Lcom/vividsolutions/jts/index/bintree/Node;
    iget-object v3, p0, Lcom/vividsolutions/jts/index/bintree/Root;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aput-object v1, v3, v0

    .line 86
    .end local v1    # "largerNode":Lcom/vividsolutions/jts/index/bintree/Node;
    :cond_2
    iget-object v3, p0, Lcom/vividsolutions/jts/index/bintree/Root;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v3, v3, v0

    invoke-direct {p0, v3, p1, p2}, Lcom/vividsolutions/jts/index/bintree/Root;->insertContained(Lcom/vividsolutions/jts/index/bintree/Node;Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected isSearchMatch(Lcom/vividsolutions/jts/index/bintree/Interval;)Z
    .locals 1
    .param p1, "interval"    # Lcom/vividsolutions/jts/index/bintree/Interval;

    .prologue
    .line 117
    const/4 v0, 0x1

    return v0
.end method
