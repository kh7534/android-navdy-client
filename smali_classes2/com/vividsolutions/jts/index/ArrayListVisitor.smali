.class public Lcom/vividsolutions/jts/index/ArrayListVisitor;
.super Ljava/lang/Object;
.source "ArrayListVisitor.java"

# interfaces
.implements Lcom/vividsolutions/jts/index/ItemVisitor;


# instance fields
.field private items:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/ArrayListVisitor;->items:Ljava/util/ArrayList;

    .line 48
    return-void
.end method


# virtual methods
.method public getItems()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/vividsolutions/jts/index/ArrayListVisitor;->items:Ljava/util/ArrayList;

    return-object v0
.end method

.method public visitItem(Ljava/lang/Object;)V
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/vividsolutions/jts/index/ArrayListVisitor;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method
