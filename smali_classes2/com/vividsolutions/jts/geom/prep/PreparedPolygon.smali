.class public Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;
.super Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;
.source "PreparedPolygon.java"


# instance fields
.field private isRectangle:Z

.field private pia:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

.field private segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Polygonal;)V
    .locals 2
    .param p1, "poly"    # Lcom/vividsolutions/jts/geom/Polygonal;

    .prologue
    const/4 v1, 0x0

    .line 62
    check-cast p1, Lcom/vividsolutions/jts/geom/Geometry;

    .end local p1    # "poly":Lcom/vividsolutions/jts/geom/Polygonal;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 56
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->isRectangle:Z

    .line 58
    iput-object v1, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    .line 59
    iput-object v1, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->pia:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    .line 63
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isRectangle()Z

    move-result v0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->isRectangle:Z

    .line 64
    return-void
.end method


# virtual methods
.method public contains(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 108
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->envelopeCovers(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 116
    :goto_0
    return v0

    .line 112
    :cond_0
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->isRectangle:Z

    if-eqz v0, :cond_1

    .line 113
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Polygon;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->contains(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 116
    :cond_1
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContains;->contains(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method

.method public containsProperly(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->envelopeCovers(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const/4 v0, 0x0

    .line 124
    :goto_0
    return v0

    :cond_0
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;->containsProperly(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method

.method public covers(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->envelopeCovers(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 136
    :goto_0
    return v0

    .line 133
    :cond_0
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->isRectangle:Z

    if-eqz v0, :cond_1

    .line 134
    const/4 v0, 0x1

    goto :goto_0

    .line 136
    :cond_1
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonCovers;->covers(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized getIntersectionFinder()Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;
    .locals 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-static {v1}, Lcom/vividsolutions/jts/noding/SegmentStringUtil;->extractSegmentStrings(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->segIntFinder:Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPointLocator()Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;
    .locals 2

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->pia:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;

    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/algorithm/locate/IndexedPointInAreaLocator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->pia:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->pia:Lcom/vividsolutions/jts/algorithm/locate/PointOnGeometryLocator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public intersects(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->envelopesIntersect(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    .line 98
    :cond_0
    iget-boolean v0, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->isRectangle:Z

    if-eqz v0, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Polygon;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleIntersects;->intersects(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0

    .line 102
    :cond_1
    invoke-static {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonIntersects;->intersects(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    goto :goto_0
.end method
