.class public abstract Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;
.super Ljava/lang/Object;
.source "GeometryEditor.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/util/GeometryEditor$GeometryEditorOperation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/geom/util/GeometryEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CoordinateOperation"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final edit(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "factory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 259
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/LinearRing;

    if-eqz v1, :cond_1

    .line 260
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;->edit([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object p1

    .line 277
    .end local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return-object p1

    .line 264
    .restart local p1    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v1, :cond_2

    .line 265
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;->edit([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object p1

    goto :goto_0

    .line 269
    :cond_2
    instance-of v1, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v1, :cond_0

    .line 270
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/vividsolutions/jts/geom/util/GeometryEditor$CoordinateOperation;->edit([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 273
    .local v0, "newCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v1, v0

    if-lez v1, :cond_3

    const/4 v1, 0x0

    aget-object v1, v0, v1

    :goto_1
    invoke-virtual {p2, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object p1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public abstract edit([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
.end method
