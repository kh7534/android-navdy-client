.class public Lcom/vividsolutions/jts/geom/util/LineStringExtracter;
.super Ljava/lang/Object;
.source "LineStringExtracter.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryFilter;


# instance fields
.field private comps:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .param p1, "comps"    # Ljava/util/List;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/LineStringExtracter;->comps:Ljava/util/List;

    .line 87
    return-void
.end method

.method public static getLines(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, v0}, Lcom/vividsolutions/jts/geom/util/LineStringExtracter;->getLines(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getLines(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "lines"    # Ljava/util/List;

    .prologue
    .line 57
    instance-of v0, p0, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_1

    .line 58
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_0
    :goto_0
    return-object p1

    .line 60
    :cond_1
    instance-of v0, p0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Lcom/vividsolutions/jts/geom/util/LineStringExtracter;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/util/LineStringExtracter;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryFilter;)V

    goto :goto_0
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 91
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/geom/util/LineStringExtracter;->comps:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_0
    return-void
.end method
