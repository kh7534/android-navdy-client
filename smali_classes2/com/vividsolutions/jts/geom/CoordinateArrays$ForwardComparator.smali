.class public Lcom/vividsolutions/jts/geom/CoordinateArrays$ForwardComparator;
.super Ljava/lang/Object;
.source "CoordinateArrays.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/geom/CoordinateArrays;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ForwardComparator"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3
    .param p1, "o1"    # Ljava/lang/Object;
    .param p2, "o2"    # Ljava/lang/Object;

    .prologue
    .line 115
    check-cast p1, [Lcom/vividsolutions/jts/geom/Coordinate;

    .end local p1    # "o1":Ljava/lang/Object;
    move-object v0, p1

    check-cast v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 116
    .local v0, "pts1":[Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast p2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .end local p2    # "o2":Ljava/lang/Object;
    move-object v1, p2

    check-cast v1, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 118
    .local v1, "pts2":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-static {v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->compare([Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    return v2
.end method
