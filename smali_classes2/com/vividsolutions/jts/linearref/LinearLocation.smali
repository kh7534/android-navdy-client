.class public Lcom/vividsolutions/jts/linearref/LinearLocation;
.super Ljava/lang/Object;
.source "LinearLocation.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private componentIndex:I

.field private segmentFraction:D

.field private segmentIndex:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    .line 89
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 97
    return-void
.end method

.method public constructor <init>(ID)V
    .locals 2
    .param p1, "segmentIndex"    # I
    .param p2, "segmentFraction"    # D

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>(IID)V

    .line 101
    return-void
.end method

.method public constructor <init>(IID)V
    .locals 3
    .param p1, "componentIndex"    # I
    .param p2, "segmentIndex"    # I
    .param p3, "segmentFraction"    # D

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    .line 89
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 105
    iput p1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    .line 106
    iput p2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 107
    iput-wide p3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 108
    invoke-direct {p0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->normalize()V

    .line 109
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/linearref/LinearLocation;)V
    .locals 2
    .param p1, "loc"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    const/4 v0, 0x0

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    .line 89
    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 118
    iget v0, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    .line 119
    iget v0, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 120
    iget-wide v0, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    iput-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 121
    return-void
.end method

.method public static compareLocationValues(IIDIID)I
    .locals 4
    .param p0, "componentIndex0"    # I
    .param p1, "segmentIndex0"    # I
    .param p2, "segmentFraction0"    # D
    .param p4, "componentIndex1"    # I
    .param p5, "segmentIndex1"    # I
    .param p6, "segmentFraction1"    # D

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 386
    if-ge p0, p4, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v0

    .line 387
    :cond_1
    if-le p0, p4, :cond_2

    move v0, v1

    goto :goto_0

    .line 389
    :cond_2
    if-lt p1, p5, :cond_0

    .line 390
    if-le p1, p5, :cond_3

    move v0, v1

    goto :goto_0

    .line 392
    :cond_3
    cmpg-double v2, p2, p6

    if-ltz v2, :cond_0

    .line 393
    cmpl-double v0, p2, p6

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_0

    .line 395
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getEndLocation(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p0, "linear"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 56
    new-instance v0, Lcom/vividsolutions/jts/linearref/LinearLocation;

    invoke-direct {v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>()V

    .line 57
    .local v0, "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/linearref/LinearLocation;->setToEnd(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 58
    return-object v0
.end method

.method private normalize()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 132
    iget-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 133
    iput-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 135
    :cond_0
    iget-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v0, v0, v4

    if-lez v0, :cond_1

    .line 136
    iput-wide v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 139
    :cond_1
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    if-gez v0, :cond_2

    .line 140
    iput v6, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    .line 141
    iput v6, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 142
    iput-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 144
    :cond_2
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-gez v0, :cond_3

    .line 145
    iput v6, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 146
    iput-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 148
    :cond_3
    iget-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_4

    .line 149
    iput-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 150
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 152
    :cond_4
    return-void
.end method

.method public static pointAlongSegmentByFraction(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 10
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "frac"    # D

    .prologue
    .line 78
    const-wide/16 v0, 0x0

    cmpg-double v0, p2, v0

    if-gtz v0, :cond_0

    .line 85
    .end local p0    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object p0

    .line 79
    .restart local p0    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p2, v0

    if-ltz v0, :cond_1

    move-object p0, p1

    goto :goto_0

    .line 81
    :cond_1
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v0, v8

    mul-double/2addr v0, p2

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double v2, v0, v8

    .line 82
    .local v2, "x":D
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v0, v8

    mul-double/2addr v0, p2

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double v4, v0, v8

    .line 84
    .local v4, "y":D
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    sub-double/2addr v0, v8

    mul-double/2addr v0, p2

    iget-wide v8, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    add-double v6, v0, v8

    .line 85
    .local v6, "z":D
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    move-object p0, v1

    goto :goto_0
.end method


# virtual methods
.method public clamp(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "linear"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 162
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 163
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->setToEnd(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumPoints()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 167
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 168
    .local v0, "line":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 169
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    goto :goto_0
.end method

.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 441
    new-instance v0, Lcom/vividsolutions/jts/linearref/LinearLocation;

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    iget-wide v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>(IID)V

    return-object v0
.end method

.method public compareLocationValues(IID)I
    .locals 5
    .param p1, "componentIndex1"    # I
    .param p2, "segmentIndex1"    # I
    .param p3, "segmentFraction1"    # D

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 356
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    if-ge v2, p1, :cond_1

    .line 365
    :cond_0
    :goto_0
    return v0

    .line 357
    :cond_1
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    if-le v2, p1, :cond_2

    move v0, v1

    goto :goto_0

    .line 359
    :cond_2
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-lt v2, p2, :cond_0

    .line 360
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-le v2, p2, :cond_3

    move v0, v1

    goto :goto_0

    .line 362
    :cond_3
    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpg-double v2, v2, p3

    if-ltz v2, :cond_0

    .line 363
    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v0, v2, p3

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_0

    .line 365
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 331
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/linearref/LinearLocation;

    .line 333
    .local v0, "other":Lcom/vividsolutions/jts/linearref/LinearLocation;
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    iget v4, v0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    if-ge v3, v4, :cond_1

    .line 342
    :cond_0
    :goto_0
    return v1

    .line 334
    :cond_1
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    iget v4, v0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    if-le v3, v4, :cond_2

    move v1, v2

    goto :goto_0

    .line 336
    :cond_2
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    iget v4, v0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-lt v3, v4, :cond_0

    .line 337
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    iget v4, v0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-le v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    .line 339
    :cond_3
    iget-wide v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    iget-wide v6, v0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpg-double v3, v4, v6

    if-ltz v3, :cond_0

    .line 340
    iget-wide v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    iget-wide v6, v0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v1, v4, v6

    if-lez v1, :cond_4

    move v1, v2

    goto :goto_0

    .line 342
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getComponentIndex()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    return v0
.end method

.method public getCoordinate(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 271
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1, v3}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 272
    .local v0, "lineComp":Lcom/vividsolutions/jts/geom/LineString;
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 273
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt v3, v4, :cond_0

    .line 276
    .end local v1    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object v1

    .line 275
    .restart local v1    # "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 276
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    invoke-static {v1, v2, v4, v5}, Lcom/vividsolutions/jts/linearref/LinearLocation;->pointAlongSegmentByFraction(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;D)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    goto :goto_0
.end method

.method public getSegment(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/LineSegment;
    .locals 6
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 288
    iget v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1, v4}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 289
    .local v0, "lineComp":Lcom/vividsolutions/jts/geom/LineString;
    iget v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 291
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    iget v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_0

    .line 292
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 293
    .local v3, "prev":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v4, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v4, v3, v1}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 296
    .end local v3    # "prev":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object v4

    .line 295
    :cond_0
    iget v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 296
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v4, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v4, v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    goto :goto_0
.end method

.method public getSegmentFraction()D
    .locals 2

    .prologue
    .line 249
    iget-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    return-wide v0
.end method

.method public getSegmentIndex()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    return v0
.end method

.method public getSegmentLength(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 6
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 204
    iget v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1, v4}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 207
    .local v0, "lineComp":Lcom/vividsolutions/jts/geom/LineString;
    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 208
    .local v3, "segIndex":I
    iget v4, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-lt v4, v5, :cond_0

    .line 209
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v4

    add-int/lit8 v3, v4, -0x2

    .line 211
    :cond_0
    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 212
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateN(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 213
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    return-wide v4
.end method

.method public isEndpoint(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 6
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 427
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 429
    .local v0, "lineComp":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 430
    .local v1, "nseg":I
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-ge v2, v1, :cond_0

    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-ne v2, v1, :cond_1

    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isOnSameSegment(Lcom/vividsolutions/jts/linearref/LinearLocation;)Z
    .locals 6
    .param p1, "loc"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    .line 407
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    iget v3, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    if-eq v2, v3, :cond_1

    .line 415
    :cond_0
    :goto_0
    return v0

    .line 408
    :cond_1
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    iget v3, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-ne v2, v3, :cond_2

    move v0, v1

    goto :goto_0

    .line 409
    :cond_2
    iget v2, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    iget v3, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    sub-int/2addr v2, v3

    if-ne v2, v1, :cond_3

    iget-wide v2, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3

    move v0, v1

    .line 411
    goto :goto_0

    .line 412
    :cond_3
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    iget v3, p1, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    sub-int/2addr v2, v3

    if-ne v2, v1, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    .line 414
    goto :goto_0
.end method

.method public isValid(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 6
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 308
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v3

    if-lt v2, v3, :cond_1

    .line 319
    :cond_0
    :goto_0
    return v1

    .line 311
    :cond_1
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1, v2}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 312
    .local v0, "lineComp":Lcom/vividsolutions/jts/geom/LineString;
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v3

    if-gt v2, v3, :cond_0

    .line 314
    iget v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    .line 317
    :cond_2
    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v2, v4

    if-gtz v2, :cond_0

    .line 319
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isVertex()Z
    .locals 4

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setToEnd(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "linear"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 224
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    .line 225
    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/LineString;

    .line 226
    .local v0, "lastLine":Lcom/vividsolutions/jts/geom/LineString;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineString;->getNumPoints()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    .line 227
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    .line 228
    return-void
.end method

.method public snapToVertex(Lcom/vividsolutions/jts/geom/Geometry;D)V
    .locals 12
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p2, "minDistance"    # D

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/16 v8, 0x0

    .line 182
    iget-wide v6, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpg-double v6, v6, v8

    if-lez v6, :cond_0

    iget-wide v6, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    cmpl-double v6, v6, v10

    if-ltz v6, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getSegmentLength(Lcom/vividsolutions/jts/geom/Geometry;)D

    move-result-wide v4

    .line 185
    .local v4, "segLen":D
    iget-wide v6, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    mul-double v2, v6, v4

    .line 186
    .local v2, "lenToStart":D
    sub-double v0, v4, v2

    .line 187
    .local v0, "lenToEnd":D
    cmpg-double v6, v2, v0

    if-gtz v6, :cond_2

    cmpg-double v6, v2, p2

    if-gez v6, :cond_2

    .line 188
    iput-wide v8, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    goto :goto_0

    .line 190
    :cond_2
    cmpg-double v6, v0, v2

    if-gtz v6, :cond_0

    cmpg-double v6, v0, p2

    if-gez v6, :cond_0

    .line 191
    iput-wide v10, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 446
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LinearLoc["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->componentIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/vividsolutions/jts/linearref/LinearLocation;->segmentFraction:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
