.class Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;
.super Ljava/lang/Object;
.source "LocationIndexOfPoint.java"


# instance fields
.field private linearGeom:Lcom/vividsolutions/jts/geom/Geometry;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 64
    return-void
.end method

.method public static indexOf(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 2
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 50
    new-instance v0, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 51
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    return-object v1
.end method

.method public static indexOfAfter(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 2
    .param p0, "linearGeom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 56
    new-instance v0, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 57
    .local v0, "locater":Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;
    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOfAfter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    return-object v1
.end method

.method private indexOfFromStart(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 22
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 113
    const-wide v10, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 114
    .local v10, "minDistance":D
    const/4 v8, 0x0

    .line 115
    .local v8, "minComponentIndex":I
    const/4 v9, 0x0

    .line 116
    .local v9, "minSegmentIndex":I
    const-wide/high16 v12, -0x4010000000000000L    # -1.0

    .line 118
    .local v12, "minFrac":D
    new-instance v14, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v14}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    .line 119
    .local v14, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    new-instance v6, Lcom/vividsolutions/jts/linearref/LinearIterator;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-direct {v6, v15}, Lcom/vividsolutions/jts/linearref/LinearIterator;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 120
    .local v6, "it":Lcom/vividsolutions/jts/linearref/LinearIterator;
    :goto_0
    invoke-virtual {v6}, Lcom/vividsolutions/jts/linearref/LinearIterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_2

    .line 121
    invoke-virtual {v6}, Lcom/vividsolutions/jts/linearref/LinearIterator;->isEndOfLine()Z

    move-result v15

    if-nez v15, :cond_1

    .line 122
    invoke-virtual {v6}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentStart()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v15

    iput-object v15, v14, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 123
    invoke-virtual {v6}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getSegmentEnd()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v15

    iput-object v15, v14, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 124
    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/vividsolutions/jts/geom/LineSegment;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v16

    .line 125
    .local v16, "segDistance":D
    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/vividsolutions/jts/geom/LineSegment;->segmentFraction(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v18

    .line 127
    .local v18, "segFrac":D
    invoke-virtual {v6}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getComponentIndex()I

    move-result v4

    .line 128
    .local v4, "candidateComponentIndex":I
    invoke-virtual {v6}, Lcom/vividsolutions/jts/linearref/LinearIterator;->getVertexIndex()I

    move-result v5

    .line 129
    .local v5, "candidateSegmentIndex":I
    cmpg-double v15, v16, v10

    if-gez v15, :cond_1

    .line 131
    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    move-wide/from16 v1, v18

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->compareLocationValues(IID)I

    move-result v15

    if-gez v15, :cond_1

    .line 137
    :cond_0
    move v8, v4

    .line 138
    move v9, v5

    .line 139
    move-wide/from16 v12, v18

    .line 140
    move-wide/from16 v10, v16

    .line 120
    .end local v4    # "candidateComponentIndex":I
    .end local v5    # "candidateSegmentIndex":I
    .end local v16    # "segDistance":D
    .end local v18    # "segFrac":D
    :cond_1
    invoke-virtual {v6}, Lcom/vividsolutions/jts/linearref/LinearIterator;->next()V

    goto :goto_0

    .line 145
    :cond_2
    const-wide v20, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v15, v10, v20

    if-nez v15, :cond_3

    .line 147
    new-instance v7, Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-object/from16 v0, p2

    invoke-direct {v7, v0}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>(Lcom/vividsolutions/jts/linearref/LinearLocation;)V

    .line 151
    :goto_1
    return-object v7

    .line 150
    :cond_3
    new-instance v7, Lcom/vividsolutions/jts/linearref/LinearLocation;

    invoke-direct {v7, v8, v9, v12, v13}, Lcom/vividsolutions/jts/linearref/LinearLocation;-><init>(IID)V

    .line 151
    .local v7, "loc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    goto :goto_1
.end method


# virtual methods
.method public indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 1
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOfFromStart(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    return-object v0
.end method

.method public indexOfAfter(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;
    .locals 4
    .param p1, "inputPt"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "minIndex"    # Lcom/vividsolutions/jts/linearref/LinearLocation;

    .prologue
    .line 94
    if-nez p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOf(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    .line 108
    :cond_0
    :goto_0
    return-object v1

    .line 97
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->linearGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->getEndLocation(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v1

    .line 98
    .local v1, "endLoc":Lcom/vividsolutions/jts/linearref/LinearLocation;
    invoke-virtual {v1, p2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-lez v2, :cond_0

    .line 101
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/linearref/LocationIndexOfPoint;->indexOfFromStart(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/linearref/LinearLocation;)Lcom/vividsolutions/jts/linearref/LinearLocation;

    move-result-object v0

    .line 106
    .local v0, "closestAfter":Lcom/vividsolutions/jts/linearref/LinearLocation;
    invoke-virtual {v0, p2}, Lcom/vividsolutions/jts/linearref/LinearLocation;->compareTo(Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v3, "computed location is before specified minimum location"

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    move-object v1, v0

    .line 108
    goto :goto_0

    .line 106
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
