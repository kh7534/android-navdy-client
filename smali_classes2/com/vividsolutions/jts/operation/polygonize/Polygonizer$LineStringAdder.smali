.class Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;
.super Ljava/lang/Object;
.source "Polygonizer.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/GeometryComponentFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LineStringAdder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;


# direct methods
.method private constructor <init>(Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;->this$0:Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;
    .param p2, "x1"    # Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$1;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;-><init>(Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;)V

    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 69
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer$LineStringAdder;->this$0:Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;

    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-static {v0, p1}, Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;->access$000(Lcom/vividsolutions/jts/operation/polygonize/Polygonizer;Lcom/vividsolutions/jts/geom/LineString;)V

    .line 71
    :cond_0
    return-void
.end method
