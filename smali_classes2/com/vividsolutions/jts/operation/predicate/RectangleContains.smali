.class public Lcom/vividsolutions/jts/operation/predicate/RectangleContains;
.super Ljava/lang/Object;
.source "RectangleContains.java"


# instance fields
.field private rectEnv:Lcom/vividsolutions/jts/geom/Envelope;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Polygon;)V
    .locals 1
    .param p1, "rectangle"    # Lcom/vividsolutions/jts/geom/Polygon;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Polygon;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    .line 75
    return-void
.end method

.method public static contains(Lcom/vividsolutions/jts/geom/Polygon;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "rectangle"    # Lcom/vividsolutions/jts/geom/Polygon;
    .param p1, "b"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 62
    new-instance v0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;-><init>(Lcom/vividsolutions/jts/geom/Polygon;)V

    .line 63
    .local v0, "rc":Lcom/vividsolutions/jts/operation/predicate/RectangleContains;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->contains(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    return v1
.end method

.method private isContainedInBoundary(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 4
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v2, 0x0

    .line 96
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/Polygon;

    if-eqz v3, :cond_1

    .line 105
    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    :goto_0
    return v2

    .line 97
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v3, :cond_2

    check-cast p1, Lcom/vividsolutions/jts/geom/Point;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->isPointContainedInBoundary(Lcom/vividsolutions/jts/geom/Point;)Z

    move-result v2

    goto :goto_0

    .line 98
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    instance-of v3, p1, Lcom/vividsolutions/jts/geom/LineString;

    if-eqz v3, :cond_3

    check-cast p1, Lcom/vividsolutions/jts/geom/LineString;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->isLineStringContainedInBoundary(Lcom/vividsolutions/jts/geom/LineString;)Z

    move-result v2

    goto :goto_0

    .line 100
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 101
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 102
    .local v0, "comp":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->isContainedInBoundary(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 100
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 105
    .end local v0    # "comp":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_4
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isLineSegmentContainedInBoundary(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 6
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x1

    .line 160
    invoke-virtual {p1, p2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->isPointContainedInBoundary(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    .line 182
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpl-double v1, v2, v4

    if-nez v1, :cond_3

    .line 165
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 182
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 169
    :cond_3
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v1, v2, v4

    if-nez v1, :cond_2

    .line 170
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v4

    cmpl-double v1, v2, v4

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method private isLineStringContainedInBoundary(Lcom/vividsolutions/jts/geom/LineString;)Z
    .locals 5
    .param p1, "line"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 139
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinateSequence()Lcom/vividsolutions/jts/geom/CoordinateSequence;

    move-result-object v3

    .line 140
    .local v3, "seq":Lcom/vividsolutions/jts/geom/CoordinateSequence;
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v1}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 141
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v2}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 142
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v3}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    .line 143
    invoke-interface {v3, v0, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 144
    add-int/lit8 v4, v0, 0x1

    invoke-interface {v3, v4, v2}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V

    .line 146
    invoke-direct {p0, v1, v2}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->isLineSegmentContainedInBoundary(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 147
    const/4 v4, 0x0

    .line 149
    :goto_1
    return v4

    .line 142
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private isPointContainedInBoundary(Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 4
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 126
    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinX()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxX()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMinY()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getMaxY()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPointContainedInBoundary(Lcom/vividsolutions/jts/geom/Point;)Z
    .locals 1
    .param p1, "point"    # Lcom/vividsolutions/jts/geom/Point;

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Point;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->isPointContainedInBoundary(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public contains(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v0, 0x0

    .line 80
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->rectEnv:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/Envelope;->contains(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 88
    :cond_1
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/predicate/RectangleContains;->isContainedInBoundary(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 90
    const/4 v0, 0x1

    goto :goto_0
.end method
