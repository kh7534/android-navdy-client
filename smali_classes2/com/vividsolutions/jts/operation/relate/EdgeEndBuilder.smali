.class public Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;
.super Ljava/lang/Object;
.source "EdgeEndBuilder.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method


# virtual methods
.method public computeEdgeEnds(Ljava/util/Iterator;)Ljava/util/List;
    .locals 4
    .param p1, "edges"    # Ljava/util/Iterator;

    .prologue
    .line 62
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v2, "l":Ljava/util/List;
    move-object v1, p1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 65
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {p0, v0, v2}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;->computeEdgeEnds(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/util/List;)V

    goto :goto_0

    .line 67
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    return-object v2
.end method

.method public computeEdgeEnds(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/util/List;)V
    .locals 6
    .param p1, "edge"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "l"    # Ljava/util/List;

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    move-result-object v1

    .line 79
    .local v1, "eiList":Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->addEndpoints()V

    .line 81
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 82
    .local v4, "it":Ljava/util/Iterator;
    const/4 v3, 0x0

    .line 83
    .local v3, "eiPrev":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    const/4 v0, 0x0

    .line 85
    .local v0, "eiCurr":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 100
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 88
    .local v2, "eiNext":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :cond_1
    move-object v3, v0

    .line 89
    move-object v0, v2

    .line 90
    const/4 v2, 0x0

    .line 91
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "eiNext":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    check-cast v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 93
    .restart local v2    # "eiNext":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    :cond_2
    if-eqz v0, :cond_3

    .line 94
    invoke-virtual {p0, p1, p2, v0, v3}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;->createEdgeEndForPrev(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;)V

    .line 95
    invoke-virtual {p0, p1, p2, v0, v2}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;->createEdgeEndForNext(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;)V

    .line 98
    :cond_3
    if-nez v0, :cond_1

    goto :goto_0
.end method

.method createEdgeEndForNext(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;)V
    .locals 6
    .param p1, "edge"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "l"    # Ljava/util/List;
    .param p3, "eiCurr"    # Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .param p4, "eiNext"    # Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .prologue
    .line 150
    iget v3, p3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    add-int/lit8 v1, v3, 0x1

    .line 152
    .local v1, "iNext":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getNumPoints()I

    move-result v3

    if-lt v1, v3, :cond_0

    if-nez p4, :cond_0

    .line 163
    :goto_0
    return-void

    .line 154
    :cond_0
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 157
    .local v2, "pNext":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz p4, :cond_1

    iget v3, p4, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    iget v4, p3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    if-ne v3, v4, :cond_1

    .line 158
    iget-object v2, p4, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 160
    :cond_1
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    iget-object v3, p3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    new-instance v4, Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(Lcom/vividsolutions/jts/geomgraph/Label;)V

    invoke-direct {v0, p1, v3, v2, v4}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 162
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method createEdgeEndForPrev(Lcom/vividsolutions/jts/geomgraph/Edge;Ljava/util/List;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;)V
    .locals 8
    .param p1, "edge"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "l"    # Ljava/util/List;
    .param p3, "eiCurr"    # Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .param p4, "eiPrev"    # Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .prologue
    .line 117
    iget v1, p3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    .line 118
    .local v1, "iPrev":I
    iget-wide v4, p3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->dist:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_1

    .line 120
    if-nez v1, :cond_0

    .line 134
    :goto_0
    return-void

    .line 121
    :cond_0
    add-int/lit8 v1, v1, -0x1

    .line 123
    :cond_1
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 125
    .local v3, "pPrev":Lcom/vividsolutions/jts/geom/Coordinate;
    if-eqz p4, :cond_2

    iget v4, p4, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->segmentIndex:I

    if-lt v4, v1, :cond_2

    .line 126
    iget-object v3, p4, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 128
    :cond_2
    new-instance v2, Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 130
    .local v2, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/Label;->flip()V

    .line 131
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    iget-object v4, p3, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1, v4, v3, v2}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 133
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
