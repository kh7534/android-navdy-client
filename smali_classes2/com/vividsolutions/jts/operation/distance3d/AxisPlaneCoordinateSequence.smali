.class public Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;
.super Ljava/lang/Object;
.source "AxisPlaneCoordinateSequence.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateSequence;


# static fields
.field private static XY_INDEX:[I

.field private static XZ_INDEX:[I

.field private static YZ_INDEX:[I


# instance fields
.field private indexMap:[I

.field private seq:Lcom/vividsolutions/jts/geom/CoordinateSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 90
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->XY_INDEX:[I

    .line 91
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->XZ_INDEX:[I

    .line 92
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->YZ_INDEX:[I

    return-void

    .line 90
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data

    .line 91
    :array_1
    .array-data 4
        0x0
        0x2
    .end array-data

    .line 92
    :array_2
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method private constructor <init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;[I)V
    .locals 0
    .param p1, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .param p2, "indexMap"    # [I

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->seq:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .line 99
    iput-object p2, p0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->indexMap:[I

    .line 100
    return-void
.end method

.method public static projectToXY(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 2
    .param p0, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 65
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;

    sget-object v1, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->XY_INDEX:[I

    invoke-direct {v0, p0, v1}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;-><init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;[I)V

    return-object v0
.end method

.method public static projectToXZ(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 2
    .param p0, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 76
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;

    sget-object v1, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->XZ_INDEX:[I

    invoke-direct {v0, p0, v1}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;-><init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;[I)V

    return-object v0
.end method

.method public static projectToYZ(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 2
    .param p0, "seq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 87
    new-instance v0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;

    sget-object v1, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->YZ_INDEX:[I

    invoke-direct {v0, p0, v1}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;-><init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;[I)V

    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 156
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public expandEnvelope(Lcom/vividsolutions/jts/geom/Envelope;)Lcom/vividsolutions/jts/geom/Envelope;
    .locals 1
    .param p1, "env"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 151
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getCoordinateCopy(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getCoordinate(ILcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "coord"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    iput-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 116
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    iput-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 117
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    iput-wide v0, p2, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    .line 118
    return-void
.end method

.method public getCoordinateCopy(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "i"    # I

    .prologue
    .line 111
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getX(I)D

    move-result-wide v2

    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getY(I)D

    move-result-wide v4

    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getZ(I)D

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    return-object v1
.end method

.method public getDimension()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x2

    return v0
.end method

.method public getOrdinate(II)D
    .locals 2
    .param p1, "index"    # I
    .param p2, "ordinateIndex"    # I

    .prologue
    .line 134
    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    const-wide/16 v0, 0x0

    .line 135
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->seq:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->indexMap:[I

    aget v1, v1, p2

    invoke-interface {v0, p1, v1}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    goto :goto_0
.end method

.method public getX(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    return-wide v0
.end method

.method public getY(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 125
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    return-wide v0
.end method

.method public getZ(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 129
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->getOrdinate(II)D

    move-result-wide v0

    return-wide v0
.end method

.method public setOrdinate(IID)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "ordinateIndex"    # I
    .param p3, "value"    # D

    .prologue
    .line 143
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/distance3d/AxisPlaneCoordinateSequence;->seq:Lcom/vividsolutions/jts/geom/CoordinateSequence;

    invoke-interface {v0}, Lcom/vividsolutions/jts/geom/CoordinateSequence;->size()I

    move-result v0

    return v0
.end method

.method public toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 147
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
