.class public Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;
.super Ljava/lang/Object;
.source "IncrementalDelaunayTriangulator.java"


# instance fields
.field private isUsingTolerance:Z

.field private subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;)V
    .locals 6
    .param p1, "subdiv"    # Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-boolean v0, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->isUsingTolerance:Z

    .line 64
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    .line 65
    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->getTolerance()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->isUsingTolerance:Z

    .line 67
    return-void
.end method


# virtual methods
.method public insertSite(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    .locals 7
    .param p1, "v"    # Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .prologue
    .line 103
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v4, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->locate(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 105
    .local v1, "e":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v4, v1, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isVertexOfEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v0, v1

    .line 136
    :cond_0
    return-object v0

    .line 109
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->isOnEdge(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 112
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 113
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->delete(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 120
    :cond_2
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->makeEdge(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 121
    .local v0, "base":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-static {v0, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->splice(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 122
    move-object v2, v0

    .line 124
    .local v2, "startEdge":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    :cond_3
    iget-object v4, p0, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->subdiv:Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->sym()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdgeSubdivision;->connect(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    .line 126
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v4

    if-ne v4, v2, :cond_3

    .line 131
    :goto_0
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v3

    .line 132
    .local v3, "t":Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;
    invoke-virtual {v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->rightOf(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->orig()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v4

    invoke-virtual {v3}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v5

    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->dest()Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    move-result-object v6

    invoke-virtual {p1, v4, v5, v6}, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;->isInCircle(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 133
    invoke-static {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->swap(Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;)V

    .line 134
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    goto :goto_0

    .line 135
    :cond_4
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v4

    if-eq v4, v2, :cond_0

    .line 138
    invoke-virtual {v1}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->oNext()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;->lPrev()Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    move-result-object v1

    goto :goto_0
.end method

.method public insertSites(Ljava/util/Collection;)V
    .locals 3
    .param p1, "vertices"    # Ljava/util/Collection;

    .prologue
    .line 80
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;

    .line 82
    .local v1, "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/triangulate/IncrementalDelaunayTriangulator;->insertSite(Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;)Lcom/vividsolutions/jts/triangulate/quadedge/QuadEdge;

    goto :goto_0

    .line 84
    .end local v1    # "v":Lcom/vividsolutions/jts/triangulate/quadedge/Vertex;
    :cond_0
    return-void
.end method
