.class public final Lorg/droidparts/inner/ClassSpecRegistry;
.super Ljava/lang/Object;
.source "ClassSpecRegistry.java"


# static fields
.field private static final COLUMN_SPECS:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Entity;",
            ">;[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/sql/ColumnAnn;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final ID_AFFIX:Ljava/lang/String; = "_id"

.field private static final INJECT_SPECS:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/inject/InjectAnn",
            "<*>;>;>;"
        }
    .end annotation
.end field

.field private static final JSON_SPECS:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Model;",
            ">;[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/JSONAnn;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final RECEIVE_EVENTS_SPECS:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;[",
            "Lorg/droidparts/inner/ann/MethodSpec",
            "<",
            "Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final TABLE_NAMES:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Entity;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final XML_SPECS:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Model;",
            ">;[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/XMLAnn;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 191
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ClassSpecRegistry;->INJECT_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    .line 192
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ClassSpecRegistry;->RECEIVE_EVENTS_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    .line 194
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ClassSpecRegistry;->TABLE_NAMES:Ljava/util/concurrent/ConcurrentHashMap;

    .line 195
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ClassSpecRegistry;->COLUMN_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    .line 197
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ClassSpecRegistry;->JSON_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    .line 198
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/inner/ClassSpecRegistry;->XML_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getColumnName(Lorg/droidparts/inner/ann/sql/ColumnAnn;Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 3
    .param p0, "ann"    # Lorg/droidparts/inner/ann/sql/ColumnAnn;
    .param p1, "field"    # Ljava/lang/reflect/Field;

    .prologue
    .line 227
    iget-object v0, p0, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    .line 228
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Lorg/droidparts/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    .line 230
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 231
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 234
    :cond_0
    return-object v0
.end method

.method private static getComponentType(Ljava/lang/reflect/Field;)Ljava/lang/Class;
    .locals 4
    .param p0, "field"    # Ljava/lang/reflect/Field;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    .line 205
    .local v1, "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v1}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 206
    invoke-static {v1}, Lorg/droidparts/inner/ReflectionUtils;->getArrayComponentType(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 212
    :cond_0
    :goto_0
    return-object v0

    .line 207
    :cond_1
    invoke-static {v1}, Lorg/droidparts/inner/TypeHelper;->isCollection(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    invoke-static {p0}, Lorg/droidparts/inner/ReflectionUtils;->getFieldGenericArgs(Ljava/lang/reflect/Field;)[Ljava/lang/Class;

    move-result-object v2

    .line 209
    .local v2, "genericArgs":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    array-length v3, v2

    if-lez v3, :cond_2

    const/4 v3, 0x0

    aget-object v0, v2, v3

    :goto_1
    goto :goto_0

    :cond_2
    const-class v0, Ljava/lang/Object;

    goto :goto_1
.end method

.method public static getInjectSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/inject/InjectAnn",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v9, Lorg/droidparts/inner/ClassSpecRegistry;->INJECT_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 64
    .local v8, "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;"
    if-nez v8, :cond_3

    .line 65
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v7, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;>;"
    invoke-static {p0}, Lorg/droidparts/inner/ReflectionUtils;->buildClassHierarchy(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 67
    .local v2, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/reflect/Field;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v3, v1, v5

    .line 68
    .local v3, "field":Ljava/lang/reflect/Field;
    invoke-static {v3}, Lorg/droidparts/inner/AnnBuilder;->getInjectAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/inject/InjectAnn;

    move-result-object v0

    .line 69
    .local v0, "ann":Lorg/droidparts/inner/ann/inject/InjectAnn;, "Lorg/droidparts/inner/ann/inject/InjectAnn<*>;"
    if-eqz v0, :cond_1

    .line 70
    new-instance v9, Lorg/droidparts/inner/ann/FieldSpec;

    const/4 v10, 0x0

    invoke-direct {v9, v3, v10, v0}, Lorg/droidparts/inner/ann/FieldSpec;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Class;Lorg/droidparts/inner/ann/Ann;)V

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 74
    .end local v0    # "ann":Lorg/droidparts/inner/ann/inject/InjectAnn;, "Lorg/droidparts/inner/ann/inject/InjectAnn<*>;"
    .end local v1    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lorg/droidparts/inner/ann/FieldSpec;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;"
    check-cast v8, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 75
    .restart local v8    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;"
    sget-object v9, Lorg/droidparts/inner/ClassSpecRegistry;->INJECT_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9, p0, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .end local v7    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/inject/InjectAnn<*>;>;>;"
    :cond_3
    return-object v8
.end method

.method public static getJSONSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Model;",
            ">;)[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/JSONAnn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 146
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Model;>;"
    sget-object v10, Lorg/droidparts/inner/ClassSpecRegistry;->JSON_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 147
    .local v9, "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    if-nez v9, :cond_3

    .line 148
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v8, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;>;"
    invoke-static {p0}, Lorg/droidparts/inner/ReflectionUtils;->buildClassHierarchy(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 150
    .local v2, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/reflect/Field;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v4, v1, v6

    .line 151
    .local v4, "field":Ljava/lang/reflect/Field;
    invoke-static {v4}, Lorg/droidparts/inner/AnnBuilder;->getJSONAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/serialize/JSONAnn;

    move-result-object v0

    .line 152
    .local v0, "ann":Lorg/droidparts/inner/ann/serialize/JSONAnn;
    if-eqz v0, :cond_1

    .line 153
    invoke-static {v4}, Lorg/droidparts/inner/ClassSpecRegistry;->getComponentType(Ljava/lang/reflect/Field;)Ljava/lang/Class;

    move-result-object v3

    .line 154
    .local v3, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v10, v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    invoke-static {v10, v4}, Lorg/droidparts/inner/ClassSpecRegistry;->getName(Ljava/lang/String;Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v0, Lorg/droidparts/inner/ann/serialize/JSONAnn;->key:Ljava/lang/String;

    .line 155
    new-instance v10, Lorg/droidparts/inner/ann/FieldSpec;

    invoke-direct {v10, v4, v3, v0}, Lorg/droidparts/inner/ann/FieldSpec;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Class;Lorg/droidparts/inner/ann/Ann;)V

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    .end local v3    # "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 160
    .end local v0    # "ann":Lorg/droidparts/inner/ann/serialize/JSONAnn;
    .end local v1    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Lorg/droidparts/inner/ann/FieldSpec;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    check-cast v9, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 161
    .restart local v9    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;"
    sget-object v10, Lorg/droidparts/inner/ClassSpecRegistry;->JSON_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, p0, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    .end local v8    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/JSONAnn;>;>;"
    :cond_3
    return-object v9
.end method

.method private static getName(Ljava/lang/String;Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "field"    # Ljava/lang/reflect/Field;

    .prologue
    .line 218
    invoke-static {p0}, Lorg/droidparts/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object p0

    .line 221
    :cond_0
    return-object p0
.end method

.method public static getReceiveEventsSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/MethodSpec;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Lorg/droidparts/inner/ann/MethodSpec",
            "<",
            "Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    sget-object v9, Lorg/droidparts/inner/ClassSpecRegistry;->RECEIVE_EVENTS_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lorg/droidparts/inner/ann/MethodSpec;

    .line 86
    .local v8, "specs":[Lorg/droidparts/inner/ann/MethodSpec;, "[Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;"
    if-nez v8, :cond_3

    .line 87
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;>;"
    invoke-static {p0}, Lorg/droidparts/inner/ReflectionUtils;->buildClassHierarchy(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 89
    .local v2, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/reflect/Method;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v7, v1, v4

    .line 90
    .local v7, "method":Ljava/lang/reflect/Method;
    invoke-static {v7}, Lorg/droidparts/inner/AnnBuilder;->getReceiveEventsAnn(Ljava/lang/reflect/Method;)Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;

    move-result-object v0

    .line 91
    .local v0, "ann":Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;
    if-eqz v0, :cond_1

    .line 92
    new-instance v9, Lorg/droidparts/inner/ann/MethodSpec;

    invoke-direct {v9, v7, v0}, Lorg/droidparts/inner/ann/MethodSpec;-><init>(Ljava/lang/reflect/Method;Lorg/droidparts/inner/ann/Ann;)V

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "ann":Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;
    .end local v1    # "arr$":[Ljava/lang/reflect/Method;
    .end local v2    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "method":Ljava/lang/reflect/Method;
    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Lorg/droidparts/inner/ann/MethodSpec;

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "specs":[Lorg/droidparts/inner/ann/MethodSpec;, "[Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;"
    check-cast v8, [Lorg/droidparts/inner/ann/MethodSpec;

    .line 97
    .restart local v8    # "specs":[Lorg/droidparts/inner/ann/MethodSpec;, "[Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;"
    sget-object v9, Lorg/droidparts/inner/ClassSpecRegistry;->RECEIVE_EVENTS_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v9, p0, v8}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;>;"
    :cond_3
    return-object v8
.end method

.method public static getTableColumnSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Entity;",
            ">;)[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/sql/ColumnAnn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    sget-object v10, Lorg/droidparts/inner/ClassSpecRegistry;->COLUMN_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 123
    .local v9, "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    if-nez v9, :cond_3

    .line 124
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v8, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;>;"
    invoke-static {p0}, Lorg/droidparts/inner/ReflectionUtils;->buildClassHierarchy(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 126
    .local v2, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/reflect/Field;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v4, v1, v6

    .line 127
    .local v4, "field":Ljava/lang/reflect/Field;
    invoke-static {v4}, Lorg/droidparts/inner/AnnBuilder;->getColumnAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/sql/ColumnAnn;

    move-result-object v0

    .line 128
    .local v0, "ann":Lorg/droidparts/inner/ann/sql/ColumnAnn;
    if-eqz v0, :cond_1

    .line 129
    invoke-static {v4}, Lorg/droidparts/inner/ClassSpecRegistry;->getComponentType(Ljava/lang/reflect/Field;)Ljava/lang/Class;

    move-result-object v3

    .line 130
    .local v3, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-static {v0, v4}, Lorg/droidparts/inner/ClassSpecRegistry;->getColumnName(Lorg/droidparts/inner/ann/sql/ColumnAnn;Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v0, Lorg/droidparts/inner/ann/sql/ColumnAnn;->name:Ljava/lang/String;

    .line 131
    new-instance v10, Lorg/droidparts/inner/ann/FieldSpec;

    invoke-direct {v10, v4, v3, v0}, Lorg/droidparts/inner/ann/FieldSpec;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Class;Lorg/droidparts/inner/ann/Ann;)V

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    .end local v3    # "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "ann":Lorg/droidparts/inner/ann/sql/ColumnAnn;
    .end local v1    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :cond_2
    invoke-static {v8}, Lorg/droidparts/inner/ClassSpecRegistry;->sanitizeSpecs(Ljava/util/ArrayList;)V

    .line 137
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Lorg/droidparts/inner/ann/FieldSpec;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    check-cast v9, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 138
    .restart local v9    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    sget-object v10, Lorg/droidparts/inner/ClassSpecRegistry;->COLUMN_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, p0, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    .end local v8    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;>;"
    :cond_3
    return-object v9
.end method

.method public static getTableName(Ljava/lang/Class;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Entity;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Entity;>;"
    sget-object v2, Lorg/droidparts/inner/ClassSpecRegistry;->TABLE_NAMES:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 106
    .local v1, "name":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 107
    invoke-static {p0}, Lorg/droidparts/inner/AnnBuilder;->getTableAnn(Ljava/lang/Class;)Lorg/droidparts/inner/ann/sql/TableAnn;

    move-result-object v0

    .line 108
    .local v0, "ann":Lorg/droidparts/inner/ann/sql/TableAnn;
    if-eqz v0, :cond_0

    .line 109
    iget-object v1, v0, Lorg/droidparts/inner/ann/sql/TableAnn;->name:Ljava/lang/String;

    .line 111
    :cond_0
    invoke-static {v1}, Lorg/droidparts/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 114
    :cond_1
    sget-object v2, Lorg/droidparts/inner/ClassSpecRegistry;->TABLE_NAMES:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    .end local v0    # "ann":Lorg/droidparts/inner/ann/sql/TableAnn;
    :cond_2
    return-object v1
.end method

.method public static getXMLSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/FieldSpec;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lorg/droidparts/model/Model;",
            ">;)[",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/serialize/XMLAnn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 169
    .local p0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<+Lorg/droidparts/model/Model;>;"
    sget-object v10, Lorg/droidparts/inner/ClassSpecRegistry;->XML_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 170
    .local v9, "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    if-nez v9, :cond_3

    .line 171
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v8, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;>;"
    invoke-static {p0}, Lorg/droidparts/inner/ReflectionUtils;->buildClassHierarchy(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;

    .line 173
    .local v2, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .local v1, "arr$":[Ljava/lang/reflect/Field;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v4, v1, v6

    .line 174
    .local v4, "field":Ljava/lang/reflect/Field;
    invoke-static {v4}, Lorg/droidparts/inner/AnnBuilder;->getXMLAnn(Ljava/lang/reflect/Field;)Lorg/droidparts/inner/ann/serialize/XMLAnn;

    move-result-object v0

    .line 175
    .local v0, "ann":Lorg/droidparts/inner/ann/serialize/XMLAnn;
    if-eqz v0, :cond_1

    .line 176
    invoke-static {v4}, Lorg/droidparts/inner/ClassSpecRegistry;->getComponentType(Ljava/lang/reflect/Field;)Ljava/lang/Class;

    move-result-object v3

    .line 177
    .local v3, "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v10, v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->tag:Ljava/lang/String;

    invoke-static {v10, v4}, Lorg/droidparts/inner/ClassSpecRegistry;->getName(Ljava/lang/String;Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->tag:Ljava/lang/String;

    .line 178
    new-instance v10, Lorg/droidparts/inner/ann/FieldSpec;

    invoke-direct {v10, v4, v3, v0}, Lorg/droidparts/inner/ann/FieldSpec;-><init>(Ljava/lang/reflect/Field;Ljava/lang/Class;Lorg/droidparts/inner/ann/Ann;)V

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    .end local v3    # "componentType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 183
    .end local v0    # "ann":Lorg/droidparts/inner/ann/serialize/XMLAnn;
    .end local v1    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v4    # "field":Ljava/lang/reflect/Field;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Lorg/droidparts/inner/ann/FieldSpec;

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    check-cast v9, [Lorg/droidparts/inner/ann/FieldSpec;

    .line 184
    .restart local v9    # "specs":[Lorg/droidparts/inner/ann/FieldSpec;, "[Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;"
    sget-object v10, Lorg/droidparts/inner/ClassSpecRegistry;->XML_SPECS:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v10, p0, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    .end local v8    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/serialize/XMLAnn;>;>;"
    :cond_3
    return-object v9
.end method

.method private static sanitizeSpecs(Ljava/util/ArrayList;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/droidparts/inner/ann/FieldSpec",
            "<",
            "Lorg/droidparts/inner/ann/sql/ColumnAnn;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p0, "columnSpecs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 241
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/droidparts/inner/ann/FieldSpec;

    .line 242
    .local v3, "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    iget-object v4, v3, Lorg/droidparts/inner/ann/FieldSpec;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    .line 243
    .local v1, "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v4, v3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-boolean v4, v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;->nullable:Z

    if-eqz v4, :cond_2

    .line 244
    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isBoolean(Ljava/lang/Class;Z)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isInteger(Ljava/lang/Class;Z)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isLong(Ljava/lang/Class;Z)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isFloat(Ljava/lang/Class;Z)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isDouble(Ljava/lang/Class;Z)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isByte(Ljava/lang/Class;Z)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isShort(Ljava/lang/Class;Z)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1, v5}, Lorg/droidparts/inner/TypeHelper;->isCharacter(Ljava/lang/Class;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 251
    :cond_1
    const-string v4, "%s can\'t be null."

    new-array v7, v6, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v4, v7}, Lorg/droidparts/util/L;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 252
    iget-object v4, v3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iput-boolean v5, v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;->nullable:Z

    goto :goto_0

    .line 254
    :cond_2
    iget-object v4, v3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iget-boolean v4, v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;->eager:Z

    if-eqz v4, :cond_0

    .line 255
    invoke-static {v1}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v1}, Lorg/droidparts/inner/TypeHelper;->isArray(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v1}, Lorg/droidparts/inner/TypeHelper;->isCollection(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_3
    iget-object v4, v3, Lorg/droidparts/inner/ann/FieldSpec;->componentType:Ljava/lang/Class;

    invoke-static {v4}, Lorg/droidparts/inner/TypeHelper;->isEntity(Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    move v0, v6

    .line 257
    .local v0, "entity":Z
    :goto_1
    if-nez v0, :cond_0

    .line 258
    const-string v4, "%s can\'t be eager."

    new-array v7, v6, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v4, v7}, Lorg/droidparts/util/L;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    iget-object v4, v3, Lorg/droidparts/inner/ann/FieldSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;

    iput-boolean v5, v4, Lorg/droidparts/inner/ann/sql/ColumnAnn;->eager:Z

    goto/16 :goto_0

    .end local v0    # "entity":Z
    :cond_5
    move v0, v5

    .line 255
    goto :goto_1

    .line 263
    .end local v1    # "fieldType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "spec":Lorg/droidparts/inner/ann/FieldSpec;, "Lorg/droidparts/inner/ann/FieldSpec<Lorg/droidparts/inner/ann/sql/ColumnAnn;>;"
    :cond_6
    return-void
.end method
