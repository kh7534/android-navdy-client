.class public final Lorg/droidparts/inner/ann/serialize/XMLAnn;
.super Lorg/droidparts/inner/ann/Ann;
.source "XMLAnn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/droidparts/inner/ann/Ann",
        "<",
        "Lorg/droidparts/annotation/serialize/XML;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute:Ljava/lang/String;

.field public final optional:Z

.field public tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/droidparts/annotation/serialize/XML;)V
    .locals 1
    .param p1, "annotation"    # Lorg/droidparts/annotation/serialize/XML;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lorg/droidparts/inner/ann/Ann;-><init>(Ljava/lang/annotation/Annotation;)V

    .line 29
    invoke-virtual {p0}, Lorg/droidparts/inner/ann/serialize/XMLAnn;->hackSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    const-string v0, "tag"

    invoke-virtual {p0, v0}, Lorg/droidparts/inner/ann/serialize/XMLAnn;->getElement(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->tag:Ljava/lang/String;

    .line 31
    const-string v0, "attribute"

    invoke-virtual {p0, v0}, Lorg/droidparts/inner/ann/serialize/XMLAnn;->getElement(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->attribute:Ljava/lang/String;

    .line 32
    const-string v0, "optional"

    invoke-virtual {p0, v0}, Lorg/droidparts/inner/ann/serialize/XMLAnn;->getElement(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->optional:Z

    .line 33
    invoke-virtual {p0}, Lorg/droidparts/inner/ann/serialize/XMLAnn;->cleanup()V

    .line 39
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-interface {p1}, Lorg/droidparts/annotation/serialize/XML;->tag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->tag:Ljava/lang/String;

    .line 36
    invoke-interface {p1}, Lorg/droidparts/annotation/serialize/XML;->attribute()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->attribute:Ljava/lang/String;

    .line 37
    invoke-interface {p1}, Lorg/droidparts/annotation/serialize/XML;->optional()Z

    move-result v0

    iput-boolean v0, p0, Lorg/droidparts/inner/ann/serialize/XMLAnn;->optional:Z

    goto :goto_0
.end method
